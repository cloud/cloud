;;; -*- Mode: Lisp; Package: CLOUD-MATHS -*-

;;;  (c) copyright 2004-2007 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :cloud-maths)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   General use functions.


(defun power-of-two (n)
  "Finds the smallest power-of-two greater than n"
  (ash 1 (ceiling (log n 2))))

(defun power-of-two-p (n)
  "Predicate for power of two's."
  (= 1 (logcount n)))

