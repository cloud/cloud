;;; -*- Mode: Lisp; Package: COMMON-LISP-USER -*-

;;;  (c) copyright 2007-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Cloud-maths package definition.

(defpackage :cloud.maths
  (:documentation "This package include several functions for doing
  mathematics, in particular vector and matrix operations.")
  (:nicknames :cloud-maths)
  (:use :iterate :cl :cl-user)
  (:export power-of-two
	   power-of-two-p
	   scalar-product
	   v-make
	   v-abs
	   v-fun
	   save-array
	   sum
	   diff
	   mean
	   variance
	   v-max
	   v-min
	   swap-array
	   pad-array
	   v+
	   v-
	   v*
	   v/
	   make-matrix
	   random-matrix
	   identity-matrix
	   identity-matrix-p
	   symmetric-matrix-p
	   transpose
	   m+
	   opposite
	   m-
	   m.*
	   m*
	   inverse-matrix
	   vflip
	   hflip
	   flip
	   toeplitz
	   hankel
	   squeeze
	   subarray))