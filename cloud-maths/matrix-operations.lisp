;;; -*- Mode: Lisp; Package: CLOUD-MATHS -*-

;;;  (c) copyright 2005-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>
;;;  (c) copyright 2000-2006
;;;           Sam Steingold for cllib/clocc functions

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Library General Public License for more details.
;;;
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA  02111-1307  USA.

(in-package :cloud-maths)

(define-condition matrix ()
  ((msg :initarg :msg :reader msg))
  (:documentation "Condition when the matrix is not correct"))


(defun make-matrix (dims &rest args)
  "Creates a matrix of dimensions specified by dims. If dims is a
single number, a square matrix of size dims."
  (let ((dimensions (if (atom dims) 
			(list dims dims)
			dims)))
			
    (apply #'make-array dimensions args)))

(defun random-matrix (dims &rest args &key (max 10) &allow-other-keys)
  "Create a matrix of given dimensions filled with random numbers."
  (remf args :max)
  (let* ((m (apply #'make-matrix dims args)))
    (dotimes (i (array-total-size m) m)
      (setf (row-major-aref m i) (random max)))))


(defun identity-matrix (n &optional m)
  "Creates an identity matrix"
  (let ((res (make-array (list n (or m n)) :initial-element 0)))
    (iter (for i from 0 below (min n (or m n)))
	  (setf (aref res i i) 1)
	  (finally (return res)))))

(defun identity-matrix-p (A)
  "Checks if the matrix is identity."
  (declare (type array A))
  (let ((dim (array-dimension A 0)))
    (and (< 1 (length (array-dimensions A)))
	 (iter (for i below dim)
	       (unless 
		   (iter (for j below (array-dimension A 1))
			 (if (= i j)
			     (unless (= 1 (aref A i j))
			       (return nil))
			     (unless (zerop (aref A i j))
			       (return nil)))
			 (finally (return t)))
		 (return nil))
	       (finally (return t))))))

(defun symmetric-matrix-p (A)
  "Checks if the matrix is symmetric."
  (declare (type array A))
  (let ((dim (array-dimension A 0)))
    (and (< 1 (length (array-dimensions A)))
	 (= dim (array-dimension A 1))
	 (iter (for i from 1 below dim)
	       (unless (iter (for j below i)
			     (unless (= (aref A i j)
					(aref A j i))
			       (return nil))
			     (finally (return t)))
		 (return nil))
	       (finally (return t))))))
		 

(defun transpose (mx)
  "Transpose the matrix."
  (declare (type array mx))
  (if (= 1 (array-rank mx))
      (let ((mxt (make-array (list (length mx) 1))))
	(loop for i from 0 below (length mx)
	      do (setf (aref mxt i 0) (aref mx i))
	      finally (return mxt)))
      (let ((mxt (make-array (reverse (array-dimensions mx)))))
	(dotimes (ii (array-dimension mx 0) mxt)
	  (dotimes (jj (array-dimension mx 1))
	    (setf (aref mxt jj ii) (aref mx ii jj)))))))


(defun array-check-return (arr dims)
  "Make the value to be returned."
  (if arr
      (if (equal (array-dimensions arr) dims)
          arr
          (error 'dimension :proc 'array-check-return :args
                 (list (array-dimensions arr) dims)))
      (make-array dims :initial-element 0)))



(defmethod two-args-+ ((a number) (b number))
  (+ a b))

(defmethod two-args-+ ((a array) (b number))
  (iter (for (the fixnum i) from 0 below (array-total-size a))
	(with res = (make-array (array-dimensions a)))
	(setf (row-major-aref res i) 
	      (+ (row-major-aref a i)
		 b))
	(finally (return res))))

(defmethod two-args-+ ((b number) (a array))
  (two-args-+ a b))

(defmethod two-args-+ ((a array) (b array))
  (if (equal (array-dimensions a)
	     (array-dimensions b))
      (iter (for (the fixnum i) from 0 below (array-total-size a))
	    (with res = (make-array (array-dimensions a)))
	    (setf (row-major-aref res i)
		  (+ (row-major-aref a i)
		     (row-major-aref b i)))
	    (finally (return res)))
      (error 'dimension :proc 'two-args-+ :args
	     (list (array-dimensions a) (array-dimensions a)))))



(defun m+ (&rest args)
  (reduce #'two-args-+ args :initial-value 0))

(defun opposite (A)
  (typecase A
    (number (- A))
    (array (let ((res (make-array (array-dimensions A))))
	     (iter (for i from 0 below (array-total-size A))
		   (setf (row-major-aref res i)
			 (- (row-major-aref A i)))
		   (finally (return res)))))))

(defun m- (arg1 &rest args)
  (if args
      (two-args-+ arg1 (opposite (reduce #'two-args-+ args :initial-value 0)))
      (opposite arg1)))




(defun m*n (a b)
  (if (and (numberp a) (numberp b))
      (* a b)
      (let* ((matrix (if (numberp a)
			 b
			 a))
	     (number (if (numberp b)
			 b
			 a))
	     (res (make-array (array-dimensions matrix)
			      :element-type (array-element-type matrix))))
	(iter (for i below (array-total-size matrix))
	      (setf (row-major-aref res i)
		    (* (row-major-aref matrix i) number)))
	res)))

(defun two-args-m.* (&optional x y)
  "Multiply two matrices element by element."
  (if (and x y)
      (cond ((or (numberp x) (numberp y)) (m*n x y))
 	    ((equal (array-dimensions x) (array-dimensions y))
	     (iter (with res = (make-array (array-dimensions x) :initial-element 0))
		   (for i from 0 below (array-total-size res))
 		   (setf (row-major-aref res i)
 			 (* (row-major-aref x i)
 			    (row-major-aref y i)))
		   (finally (return res))))
 	    (t (error 'dimension :proc 'two-args-m.* :args
		      (list (array-dimensions x) (array-dimensions y)))))
      (or x y 0)))

(defun m.* (&rest args)
  (reduce #'two-args-m.* args))
			
  

(defun two-args-m* (aa bb &optional re)
  "Multiply two matrices.
The optional third argument is the place where to put the return value."
  (cond ((numberp aa) (m*n aa bb))
	((numberp bb) (m*n aa bb))
	((and (= 2 (array-rank aa)) ; matrix X matrix
	      (= 2 (array-rank bb)))
	 (if (= (array-dimension aa 1)
		(array-dimension bb 0))
	     (loop :with res :of-type array =
		(array-check-return re (list (array-dimension aa 0)
					     (array-dimension bb 1)))
			     :for ii :from 0 :below (array-dimension aa 0) :do
		(loop :for jj :from 0 :below (array-dimension bb 1) :do
                   (setf (aref res ii jj) 0)
                   (loop :for kk :from 0 :below (array-dimension aa 1) :do
		      (incf (aref res ii jj)
			    (* (aref aa ii kk) (aref bb kk jj)))))
									   :finally (return res))
	     (error 'dimension :proc 'two-args-m* :args
		    (list (array-dimensions aa) (array-dimensions bb)))))
	((and (= 1 (array-rank aa)) ; row X matrix
	      (= 2 (array-rank bb)))
	 (if (= (array-dimension aa 0)
		(array-dimension bb 0))
	     (loop :with res :of-type array =
		(array-check-return re (list (array-dimension bb 1)))
			     :for ii :from 0 :below (array-dimension bb 1) :do
		(setf (aref res ii) 0)
		(loop :for jj :from 0 :below (array-dimension aa 0) :do
                   (incf (aref res ii)
                         (* (aref aa jj) (aref bb jj ii))))
									   :finally (return res))
	     (error 'dimension :proc 'two-args-m* :args
		    (list (array-dimensions aa) (array-dimensions bb)))))
	((and (= 2 (array-rank aa)) ; matrix X column
	      (= 1 (array-rank bb)))
	 (if (= (array-dimension aa 1)
		(array-dimension bb 0))
	     (loop :with res :of-type array =
		(array-check-return re (list (array-dimension aa 0)))
			     :for ii :from 0 :below (array-dimension aa 0) :do
		(setf (aref res ii) 0)
		(loop :for jj :from 0 :below (array-dimension bb 0) :do
                   (incf (aref res ii) (* (aref aa ii jj) (aref bb jj))))
									   :finally (return res))
	     (error 'dimension :proc 'm* :args
		    (list (array-dimensions aa) (array-dimensions bb)))))
	((and (= 1 (array-rank aa)) ; column X row (row X col is dot)
	      (= 1 (array-rank bb)))
	 (loop :with res :of-type array =
	    (array-check-return
	     re (list (array-dimension aa 0) (array-dimension bb 0)))
			 :for ii :from 0 :below (array-dimension aa 0) :do
	    (loop :for jj :from 0 :below (array-dimension bb 0) :do
	       (setf (aref res ii jj) (* (aref aa ii) (aref bb jj))))
								       :finally (return res)))
	(t (error 'code :proc 'm* :args (list aa bb) :mesg
		  "cannot multiply matrix <~:/matrix-print/> ~
                     by matrix <~:/matrix-print/>"))))

(defun m* (&rest args)
  (reduce #'two-args-m* args :initial-value 1))



;;;
;;; matrix inversion
;;;

(define-modify-macro mulf (mult) * "Multiply the arg by a number.")

(defun mx-swap-rows (mx ii jj)
  (loop :for kk :from 0 :below (array-dimension mx 1) :do
    (rotatef (aref mx ii kk) (aref mx jj kk))))

(defun mx-swap-cols (mx ii jj)
  (loop :for kk :from 0 :below (array-dimension mx 0) :do
    (rotatef (aref mx kk ii) (aref mx kk jj))))

(defun inverse-matrix (mx)
  "Invert the matrix in-place.
Return the log determinant of the matrix, parity, and the matrix argument."
  (let* ((dim (array-dimension mx 0))
         (logdet 1) (parity 1)  ; log determinant & its sign
         (l (make-array dim))   ; row permutation vector
         (m (make-array dim)))  ; column permutation vector
    (unless (and (= 2 (array-rank mx)) (= dim (array-dimension mx 1)))
      (error 'dimension :proc 'matrix-inverse
             :args (list (array-rank mx) (array-dimensions mx))))
    (loop :with biga = 0 :for k :from 0 :below dim :do
      (setf (aref l k) k
            (aref m k) k
            biga (aref mx k k))
      ;; find the biggest element in the submatrix
      (loop :for i :from k :below dim :do
        (loop :for j :from k :below dim :do
          (when (> (abs (aref mx i j)) (abs biga))
            (setf biga (aref mx i j)
                  (aref l k) i
                  (aref m k) j))))
      ;; interchange rows
      (when (> (aref l k) k)
        (mx-swap-rows mx (aref l k) k))
      ;; interchange columns
      (when (> (aref m k) k)
        (mx-swap-cols mx (aref m k) k))
      ;; divide column by minus pivot (pivot is in biga)
      (when (zerop biga)
        (error 'division-by-zero :operation 'matrix-inverse
               :operands (list mx)))
      (let ((/biga (/ biga)))
        (loop :for i :from 0 :below dim :do
          (when (/= i k)
            (mulf (aref mx i k) (- /biga))))
        ;; reduce matrix
        (loop :for i :from 0 :below dim :do
          (when (/= i k)
            (let ((temp (aref mx i k)))
              (loop :for j :from 0 :below dim :do
                (when (/= j k)
                  (incf (aref mx i j) (* temp (aref mx k j))))))))
        ;; divide row by pivot
        (loop :for j :from 0 :below dim :do
          (when (/= j k)
            (mulf (aref mx k j) /biga)))
        (incf logdet (log (abs biga))) ; DET is the product of pivots
        (when (minusp biga) (setq parity (- parity)))
        (setf (aref mx k k) /biga)))
    ;; final row & column interchanges
    (loop :for k :downfrom (1- dim) :to 0 :do
      (when (> (aref l k) k)
        (mx-swap-cols mx (aref l k) k))
      (when (> (aref m k) k)
        (mx-swap-rows mx (aref m k) k)))
    (values logdet parity mx)))

(defun vflip (mx)
  "Vertically flips a matrix."
  (let* ((dims (array-dimensions mx))
	 (result (make-array dims :element-type (array-element-type mx))))
    (iter (for i below (second dims))
	  (iter (for j below (first dims))
		(setf (aref result j i)
		      (aref mx (- (first dims) j 1) i))))
    result))

(defun hflip (mx)
  "Horizontally flips a matrix."
  (let* ((dims (array-dimensions mx))
	 (result (make-array dims :element-type (array-element-type mx))))
    (iter (for i below (second dims))
	  (iter (for j below (first dims))
		(setf (aref result j i)
		      (aref mx j (- (second dims) i 1)))))
    result))

(defun flip (A &optional (direction :horizontally))
  (if (eql direction :vertically)
      (vflip A)
      (hflip A)))


(defun toeplitz (col &optional row)
  "Creates a toeplitz matrix with col as its first column and row as
   its first row."
  (unless row
    (setf row (make-array (array-dimensions col) :initial-contents col))
    (iter (for i index-of-vector col)
	  (setf (aref col i) (conjugate (aref col i)))))
  (let ((toep (make-array (list (length col) (length row)))))
    (iter (for i from 0 below (length row))
	  (iter (for j from 0 below (length col))
		(if (<= i j)
		    (setf (aref toep j i) (aref col (- j i)))
		    (setf (aref toep j i) (aref row (- i j))))))
    toep))
  

(defun hankel (col &optional row)
  "Creates a hankel matrix with col as its first column and row as its
   first row."
  (unless row
    (setf row (make-array (array-dimensions col))))
  (vflip (toeplitz (reverse col) row)))



;;;; subarrays

(defun subarray-1 (array subscripts)
  (let ((result-array (make-array (mapcar #'(lambda (x)
					      (1+ (- (cadr x) (car x))))
					  subscripts))))
    (iter (for i from (car (nth 0 subscripts)) to (cadr (nth 0 subscripts)))
	  (setf (aref result-array 
		      (- i (car (nth 0 subscripts))))
		(aref array i)))
    result-array))

(defun subarray-2 (array subscripts)
  (let ((result-array (make-array (mapcar #'(lambda (x)
					      (1+ (- (cadr x) (car x))))
					  subscripts))))
    (iter (for i from (car (nth 0 subscripts)) to (cadr (nth 0 subscripts)))
	  (iter (for j from (car (nth 1 subscripts)) to (cadr (nth 1 subscripts)))
		(setf (aref result-array 
			    (- i (car (nth 0 subscripts))) 
			    (- j (car (nth 1 subscripts))))
		      (aref array i j))))
    result-array))

(defun subarray-3 (array subscripts)
  (let ((result-array (make-array (mapcar #'(lambda (x)
					      (1+ (- (cadr x) (car x))))
					  subscripts))))
    (iter (for i from (car (nth 0 subscripts)) to (cadr (nth 0 subscripts)))
	  (iter (for j from (car (nth 1 subscripts)) to (cadr (nth 1 subscripts)))
		(iter (for k from (car (nth 2 subscripts)) to (cadr (nth 2 subscripts)))
		      (setf (aref result-array 
				  (- i (car (nth 0 subscripts))) 
				  (- j (car (nth 1 subscripts)))
				  (- k (car (nth 2 subscripts))))
			    (aref array i j k)))))
    result-array))


(defun squeeze (array)
  (let* ((dims (iter (for i from 0 below (array-rank array))
		     (unless (= (array-dimension array i) 1)
		       (collect (array-dimension array i))))))
    (if dims
	(make-array dims :displaced-to array)
	(make-array 1 :displaced-to array))))


(defun subarray (array &rest subscripts)
  (let ((s (iter (for i in subscripts)
		 (for j from 0)
		 (collect (if (atom i)
			      (if i
				  (list i i)
				  (list 0 (1- (array-dimension array j))))
			      i)))))
    (case (array-rank array)
      (1 (squeeze (subarray-1 array s)))
      (2 (squeeze (subarray-2 array s)))
      (3 (squeeze (subarray-3 array s))))))



(defun sum (g &optional (dim 0))
  (if (= (array-rank g) 1)
      (v-sum g)
      (let ((indices (make-list (array-rank g)))) 
	(iter (for i below (array-dimension g dim))
	      (setf (elt indices dim) i)
	      (reducing (apply #'subarray g indices)
			by #'v+)))))


;;; The following is taken from
;;; http://www.phys.uu.nl/DU/num_recipes/lisp.1ed/senac/readme.htm
;;; which is a translation of the fortran numerical recipes in CL.
;;; Hence, check licence.

;; (defmacro fref (array &rest indicies)
;;   `(aref ,array
;; 	 ,@(mapcar #'(lambda (index) `(1- ,index)) indicies)))

;; ;;Return X with the sign of Y.  Various routines like this exist
;; ;;in the book, but they differ about what to do when Y is 0.
;; ;;This one gives X rather than -X.
;; (defun signp (x y)
;;   (if (minusp y) (- x) x))

;; ;;Convert a number to a double-float
;; (defun dfloat (x)
;;   (coerce x 'double-float))

;; ;;Check that a number returned from a user function is a double-float
;; (defun dfloat-check (x)
;;   (check-type x double-float)
;;   x)


;; (defun rotateal (a i j k l tau s)
;;   (let ((g (aref a i j))
;; 	(h (aref a k l)))
;;     (setf (aref a i j) (- g (* s (+ h (* g tau)))))
;;     (setf (aref a k l) (+ h (* s (- g (* h tau)))))))

;; (defun jacobi (truc)
;;  ;(declare (type (simple-array double-float (* *)) a)) 
  
;;   (let* ((a (make-array (array-dimensions truc) :displaced-to truc))
;; 	 (nrot 0)
;; 	 (n (array-dimension a 0))
;; 	 (d (make-array n :initial-element 0))
;; 	 (v (identity-matrix n))
;; 	 (b (make-array n :initial-element 0))
;; 	 (z (make-array n :initial-element 0))
;; 	 (tresh 0) 
;; 	 (g 0) 
;; 	 (h 0) 
;; 	 (t0 0) 
;; 	 (theta 0) 
;; 	 (c 0) 
;; 	 (s 0) 
;; 	 (tau 0))
    
;;       (iter (for (the fixnum ip) below n)
;; 	    (setf (aref b ip) (aref a ip ip))
;; 	    (setf (aref d ip) (aref b ip)))


;;       (iter (for i from 0)
;; 	    (until (zerop (iter (for ip below (1- n))
;; 				(sum (iter (for iq from (1+ ip) below n)
;; 					   (sum (abs (aref a ip iq))))))))
;; 	    (finally (return-from jacobi (values d v nrot)))


;; 	    (iter (for ip from 0 below (1- n))
;; 		  (iter (for iq from (1+ ip) below n)
;; 			(setf g (* 100 (abs (aref a ip iq))))
;; 			(cond 
;; 			  ((and (= (+ (abs (aref d ip)) g) (abs (aref d ip)))
;; 				(= (+ (abs (aref d iq)) g) (abs (aref d iq))))
;; 			   (setf (aref a ip iq) 0))
;; 			  ((> (abs (aref a ip iq)) tresh)            
;; 			   (setf h (+ (aref d iq) (- (aref d ip))))
			   
;; 			   (cond 
;; 			     ((= (+ (abs h) g) (abs h))
;; 			      (setf t0 (/ (aref a ip iq) h)))
;; 			     (t
;; 			      (setf theta (/ h 2 (aref a ip iq)))
;; 			      (setf t0 (/ 1 (+ (abs theta) (sqrt (1+ (expt theta 2))))))
;; 			      (if (< theta 0) (setf t0 (- t0)))))
			   
;; 			   (setf c (/ 1 (sqrt (+ 1 (expt t0 2))))) 
;; 			   (setf s (* t0 c))
;; 			   (setf tau (/ s (1+ c))) 
;; 			   (setf h (* t0 (aref a ip iq)))
;; 			   (decf (aref z ip) h)
;; 			   (incf (aref z iq) h)
;; 			   (decf (aref d ip) h)
;; 			   (incf (aref d iq) h) 
;; 			   (setf (aref a ip iq) 0)
;; 			   (iter (for j from 0 below ip)
;; 				 (rotateal a j ip j iq tau s))
;; 			   (iter (for j from (1+ ip) below iq)
;; 				 (rotateal a ip j j iq tau s))
;; 			   (iter (for j from (1+ iq) below n)
;; 				 (rotateal a ip j iq j tau s))
;; 			   (iter (for j from 0 below n)
;; 				 (rotateal v j ip j iq tau s))
;; 			   (setf nrot (+ nrot 1))))))
;; 	    (iter (for ip below n)
;; 		  (incf (aref b ip) (aref z ip))
;; 		  (setf (aref d ip) (aref b ip))
;; 		  (setf (aref z ip) 0)))))


;; ; nr11.l
;; ; Eigensystems
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;Routines translated with permission by Kevin A. Broughan from ;;;;;;;;;;;
;; ;;Numerical Recipies in Fortran Copyright (c) Numerical Recipies 1986, 1989;;;;
;; ;;;;;;;;;;;;;;;Modified by Ken Olum for Common Lisp, April 1996;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ; functions:
;; ;	jacobi: eigenvalues and vectors of a symmetric matrix
;; ;	eigsrt: sorts eigenvectors into order by eigenvalue
;; ;	tred2: Householder reduction of a real symmetric matrix
;; ;	tqli: eigenvalues and vectors of a symmetric tridiagonal matrix
;; ;	balanc: balance a non-symmetric matrix
;; ;	elmhes: reduce a general matrix to Hessenberg form
;; ;	hqr: eigenvalues of a Hessenberg matrix
;; ;------------------------------------------------------------------------------

;; (defun optimized-jacobi (a)
;;  ;(declare (type (simple-array double-float (* *)) a)) 

;;  (prog* ((nrot 0)
;;   (n (array-dimension a 0))
;;   (d (make-array n :element-type 'double-float :initial-element 0d0))
;;   (v 
;;     (make-array (list n n) :element-type 'double-float :initial-element 0d0))
;;   (b (make-array n :element-type 'double-float :initial-element 0d0))
;;   (z (make-array n :element-type 'double-float :initial-element 0d0))
;;   (sm 0d0) (tresh 0d0) (g 0d0) (h 0d0) (t0 0d0) 
;;   (theta 0d0) (c 0d0) (s 0d0) (tau 0d0))

;;   (declare (type (simple-array double-float (*)) d)) 
;;   (declare (type (simple-array double-float (* *)) v)) 
;;   (declare (type (simple-array double-float (*)) b)) 
;;   (declare (type (simple-array double-float (*)) z))
;;   (declare (type fixnum n nrot))
;;   (declare (type double-float sm tresh g h t0 theta c s tau))
  


;;   (do ((ip 1 (+ ip 1)))
;;       ((> ip n) t)
;;       (declare (type fixnum ip))
;;     (do ((iq 1 (+ iq 1)))
;;         ((> iq n) t)
;;         (declare (type fixnum iq))
;;       (setf (fref  v ip iq) 0d0))
;;     (setf (fref v ip ip) 1d0)) 
;;   (do ((ip 1 (+ ip 1)))
;;       ((> ip n) t)
;;       (declare (type fixnum ip))
;;     (setf (fref b ip) (fref a ip ip))
;;     (setf (fref d ip) (fref b ip))
;;     (setf (fref z ip) 0d0)) 
;;   (setf nrot 0) 
;;   (do ((i 1 (+ i 1)))
;;       ((> i 50) t)
;;       (declare (type fixnum i))
;;     (setf sm 0d0)
;;     (do ((ip 1 (+ ip 1)))
;;         ((> ip (+ n (- 1))) t)
;;         (declare (type fixnum ip))
;;       (do ((iq (+ ip 1) (+ iq 1)))
;;           ((> iq n) t)
;;           (declare (type fixnum iq))
;;         (setf sm (+ sm (abs (fref a ip iq))))))
    
;;     (if (= sm 0d0) (go end))
;;     (if (< i 4) 
;;         (setf tresh (/ (* 0.2d0 sm) (dfloat (expt n 2))))
;;         (setf tresh 0d0))
;;     (do ((ip 1 (+ ip 1)))
;;         ((> ip (+ n (- 1))) t)
;;         (declare (type fixnum ip))
;;       (do ((iq (+ ip 1) (+ iq 1)))
;;           ((> iq n) t)
;;           (declare (type fixnum iq))
;;         (setf g (* 100d0 (abs (fref a ip iq))))
;;         (cond 
;;          ((and (> i 4)
;;               (= (+ (abs (fref d ip)) g) (abs (fref d ip)))
;;               (= (+ (abs (fref d iq)) g) (abs (fref d iq))))
;;            (setf (fref a ip iq) 0d0))
;;          ((> (abs (fref a ip iq)) tresh)            
;;           (setf h (+ (fref d iq) (- (fref d ip))))

;;           (cond 
;;            ((= (+ (abs h) g) (abs h))
;;             (setf t0 (/ (fref a ip iq) h)))
;;            (t
;;             (setf theta (/ (* 0.5d0 h) (fref a ip iq)))
;;            (setf t0 (/ 1d0 (+ (abs theta) (sqrt (1+ (expt theta 2))))))
;;            (if (< theta 0d0) (setf t0 (- t0)))))

;;           (setf c (/ 1d0 (sqrt (+ 1d0 (expt t0 2))))) (setf s (* t0 c))
;;           (setf tau (/ s (1+ c))) (setf h (* t0 (fref a ip iq)))
;;           (setf (fref z ip) (+ (fref z ip) (- h)))
;;           (setf (fref z iq) (+ (fref z iq) h))
;;           (setf (fref d ip) (+ (fref d ip) (- h)))
;;           (setf (fref d iq) (+ (fref d iq) h)) (setf (fref a ip iq) 0d0)
;;           (do ((j 1 (+ j 1)))
;;               ((> j (+ ip (- 1))) t)
;;               (declare (type fixnum j))
;;             (setf g (fref a j ip))
;;             (setf h (fref a j iq))
;;             (setf (fref a j ip) (+ g (* (- s) (+ h (* g tau)))))
;;             (setf (fref a j iq) (+ h (* s (+ g (* (- h) tau))))))
;;           (do ((j (+ ip 1) (+ j 1)))
;;               ((> j (+ iq (- 1))) t)
;;               (declare (type fixnum j))
;;             (setf g (fref a ip j))
;;             (setf h (fref a j iq))
;;             (setf (fref a ip j) (+ g (* (- s) (+ h (* g tau)))))
;;             (setf (fref a j iq) (+ h (* s (+ g (* (- h) tau))))))
;;           (do ((j (+ iq 1) (+ j 1)))
;;               ((> j n) t)
;;               (declare (type fixnum j))
;;             (setf g (fref a ip j))
;;             (setf h (fref a iq j))
;;             (setf (fref a ip j) (+ g (* (- s) (+ h (* g tau)))))
;;             (setf (fref a iq j) (+ h (* s (+ g (* (- h) tau))))))
;;           (do ((j 1 (+ j 1)))
;;               ((> j n) t)
;;               (declare (type fixnum j))
;;             (setf g (fref v j ip))
;;             (setf h (fref v j iq))
;;             (setf (fref v j ip) (+ g (* (- s) (+ h (* g tau)))))
;;             (setf (fref v j iq) (+ h (* s (+ g (* (- h) tau))))))
;;           (setf nrot (+ nrot 1))))))
;;     (do ((ip 1 (+ ip 1)))
;;         ((> ip n) t)
;;         (declare (type fixnum ip))
;;       (setf (fref b ip) (+ (fref b ip) (fref z ip)))
;;       (setf (fref d ip) (fref b ip))
;;       (setf (fref z ip) 0d0))) 
;;   (error "jacobi should not reach this point") 
;; end
;;   (return (values d v nrot))))

;; ;------------------------------------------------------------------------------

;; (defun eigsrt (d v)
;;  (declare (type (simple-array double-float (*)) d)) 
;;  (declare (type (simple-array double-float (* *)) v))

;;  (prog ((k 0) (n 0) (p 0d0))
;;   (declare (type fixnum k n))
;;   (declare (type double-float p))

;;   (setq n (array-dimension d 0))
   
;;   (do ((i 1 (+ i 1)))
;;       ((> i (1- n)) t)
;;       (declare (type fixnum i))
;;     (setf k i)
;;     (setf p (aref  d (1- i)))
;;     (do ((j (+ i 1) (+ j 1)))
;;         ((> j n) t)
;;         (declare (type fixnum j))
;;       (when (>= (aref d (1- j)) p) 
;;             (setf k j)
;;             (setf p (aref d (1- j)))))

;;     (when (not (= k 1))
;;      (setf (aref d (1- k)) (aref d (1- i))) 
;;      (setf (aref d (1- i)) p)
;;      (do ((j 1 (+ j 1)))
;;          ((> j n) t)
;;          (declare (type fixnum j))
;;        (setf p (aref v (1- j) (1- i)))
;;        (setf (aref v (1- j) (1- i)) (aref v (1- j) (1- k)))
;;        (setf (aref v (1- j) (1- k)) p)))) 
  
;;   (return (values d v))))

;; ;------------------------------------------------------------------------------
;; (defun tred2 (a &key (eigenvectors t))
;;  (declare (type (simple-array double-float (* *)) a)) 
;;  (declare (type symbol eigenvectors))

;;  (prog* (
;;   (n (array-dimension a 0))
;;   (d (make-array n :element-type 'double-float :initial-element 0d0))
;;   (e (make-array n :element-type 'double-float :initial-element 0d0))
;;   (l 0) (h 0d0) 
;;   (scale 0d0) (f 0d0) (g 0d0) (hh 0d0))

;;   (declare (type (simple-array double-float (*)) d)) 
;;   (declare (type (simple-array double-float (*)) e)) 
;;   (declare (type fixnum n l))
;;   (declare (type double-float h scale f g hh))


;;   (do ((i n (1- i)))
;;       ((< i 2) t)
;;       (declare (type fixnum i))
;;     (setf l (1- i))
;;     (setf h 0d0)
;;     (setf scale 0d0)
;;     (cond 
;;      ((> l 1)
;;       (do ((k 1 (+ k 1)))
;;           ((> k l) t)
;;           (declare (type fixnum l))
;;         (setf scale (+ scale (abs (fref a i k)))))
;;       (cond 
;;        ((= scale 0d0)
;;         (setf (fref e i) (fref a i l)))
;;        (t
;;         (do ((k 1 (+ k 1)))
;;             ((> k l) t)
;;             (declare (type fixnum k))
;;           (setf (fref a i k) (/ (fref a i k) scale))
;;           (setf h (+ h (expt (fref a i k) 2))))
;;         (setf f (fref a i l)) (setf g (- (signp (sqrt h) f)))
;;         (setf (fref e i) (* scale g)) (setf h (+ h (* (- f) g)))
;;         (setf (fref a i l) (+ f (- g))) (setf f 0d0)
;;         (do ((j 1 (+ j 1)))
;;             ((> j l) t)
;;             (declare (type fixnum j))
;;           (if eigenvectors (setf (fref a j i) (/ (fref a i j) h)))
;;           (setf g 0d0)
;;           (do ((k 1 (+ k 1)))
;;               ((> k j) t)
;;               (declare (type fixnum k))
;;             (setf g (+ g (* (fref a j k) (fref a i k)))))
;;           (do ((k (+ j 1) (+ k 1)))
;;               ((> k l) t)
;;               (declare (type fixnum k))
;;             (setf g (+ g (* (fref a k j) (fref a i k)))))
;;           (setf (fref e j) (/ g h))
;;           (setf f (+ f (* (fref e j) (fref a i j)))))
;;         (setf hh (/ f (+ h h)))
;;         (do ((j 1 (+ j 1)))
;;             ((> j l) t)
;;             (declare (type fixnum j))
;;           (setf f (fref a i j))
;;           (setf g (+ (fref e j) (* (- hh) f)))
;;           (setf (fref e j) g)
;;           (do ((k 1 (+ k 1)))
;;               ((> k j) t)
;;               (declare (type fixnum k))
;;             (setf (fref a j k) (+ (+ (fref a j k) (* (- f) (fref e k)))
;;                                  (* (- g) (fref a i k)))))))))
;;       (t
;;        (setf (fref e i) (fref a i l))))

;;     (setf (fref d i) h)) 

;;   (if eigenvectors (setf (fref d 1) 0d0))
;;   (setf (fref e 1) 0d0) 
;;   (do ((i 1 (+ i 1)))
;;       ((> i n) t)
;;       (declare (type fixnum i))
;;     (when eigenvectors
;;      (setf l (+ i (- 1)))
;;      (when 
;;       (not (= (fref d i) 0d0))
;;       (do ((j 1 (+ j 1)))
;;           ((> j l) t)
;;           (declare (type fixnum j))
;;         (setf g 0d0)
;;         (do ((k 1 (+ k 1)))
;;             ((> k l) t)
;;             (declare (type fixnum k))
;;           (setf g (+ g (* (fref a i k) (fref a k j)))))
;;         (do ((k 1 (+ k 1)))
;;             ((> k l) t)
;;             (declare (type fixnum k))
;;           (setf (fref a k j) (+ (fref a k j) (* (- g) (fref a k i))))))))
     
;;     (setf (fref d i) (fref a i i))
;;     (when eigenvectors
;;      (setf (fref a i i) 1d0)
;;      (do ((j 1 (+ j 1)))
;;          ((> j l) t)
;;          (declare (type fixnum j))
;;        (setf (fref a i j) 0d0)
;;        (setf (fref a j i) 0d0)))) 
  
;;   (return (values a d e))))

;; ;------------------------------------------------------------------------------

;; (defun tqli (d e z &key (eigenvectors t))
;;  (declare (type (simple-array double-float (*)) d)) 
;;  (declare (type symbol eigenvectors))
;;  (declare (type (simple-array double-float (*)) e)) 
;;  (declare (type (simple-array double-float (* *)) z)) 

;;  (prog ((n 0) (m 0) (iter 0) (f 0d0) (s 0d0) (c 0d0) 
;;         (b 0d0) (r 0d0) (g 0d0) (dd 0d0) (p 0d0))
;;   (declare (type fixnum n m iter))
;;   (declare (type double-float f s c b r g dd p))

;;   (setq n (array-dimension d 0))
;;   (do ((i 2 (+ i 1)))
;;       ((> i n) t)
;;       (declare (type fixnum i))
;;     (setf (fref e (+ i (- 1))) (fref e i))) 
;;   (setf (fref e n) 0d0) 
;;   (do ((l 1 (+ l 1)))
;;       ((> l n) t)
;;       (declare (type fixnum l))
;;     (setf iter 0)
;;     label1
;;     (do ((mm l (+ mm 1)))
;;         ((> mm (+ n (- 1))) t)
;;         (declare (type fixnum mm))
;;       (setf dd (+ (abs (fref d mm)) (abs (fref d (+ mm 1)))))
;;       (setq m mm)
;;       (if (= (+ (abs (fref e mm)) dd) dd) (go label2)))
;;     (setf m n)
;;     label2
;;     (when (not (= m l))
;;      (if (= iter 30) (error "too many iterations in tqli")) 
;;      (setf iter (+ iter 1))
;;      (setf g (/ (+ (fref d (+ l 1)) (- (fref d l))) (* 2d0 (fref e l))))
;;      (setf r (sqrt (+ (expt g 2) 1d0)))
;;      (setf g (+ (+ (fref d m) (- (fref d l)))
;;                 (/ (fref e l) (+ g (signp r g)))))
;;      (setf s 1d0) (setf c 1d0) (setf p 0d0)
;;      (do ((i (+ m (- 1)) (+ i (- 1))))
;;          ((< i l) t)
;;          (declare (type fixnum i))
;;        (setf f (* s (fref e i)))
;;        (setf b (* c (fref e i)))
;;        (cond 
;;         ((>= (abs f) (abs g))
;;          (setf c (/ g f))
;;          (setf r (sqrt (+ (expt c 2) 1d0))) 
;;          (setf (fref e (+ i 1)) (* f r))
;;          (setf s (/ 1 r)) (setf c (* c s)) )
;;         (t 
;;          (setf s (/ f g))
;;          (setf r (sqrt (+ (expt s 2) 1d0))) 
;;          (setf (fref e (+ i 1)) (* g r))
;;          (setf c (/ 1 r)) (setf s (* s c))))

;;        (setf g (+ (fref d (+ i 1)) (- p)))
;;        (setf r (+ (* (+ (fref d i) (- g)) s) (* (* 2 c) b)))
;;        (setf p (* s r))
;;        (setf (fref d (+ i 1)) (+ g p))
;;        (setf g (+ (* c r) (- b)))
;;        (when eigenvectors
;;         (do ((k 1 (+ k 1)))
;;             ((> k n) t)
;;             (declare (type fixnum k))
;;           (setf f (fref z k (+ i 1)))
;;           (setf (fref z k (+ i 1)) (+ (* s (fref z k i)) (* c f)))
;;           (setf (fref z k i) (+ (* c (fref z k i)) (* (- s) f))))))
;;      (setf (fref d l) (+ (fref d l) (- p))) (setf (fref e l) g)
;;      (setf (fref e m) 0d0)
;;      (go label1))) 
  
;;   (return (values d z))))

;; ;------------------------------------------------------------------------------

;; (defun balanc (a &key (radix 2d0))
;;  (declare (type (simple-array double-float (* *)) a))
;;  (declare (type double-float radix))

;;  (prog ((n 0) (sqrdx 0d0) (c 0d0) (r 0d0) (last 0) (f 0d0)
;;         (g 0d0) (s 0d0))
;;   (declare (type fixnum n last))
;;   (declare (type double-float sqrdx c r f g s))

;;   (setq sqrdx (* radix radix))
;;   (setq n (array-dimension a 0))
;; label1
;;   (setf last 1)
;;   (do ((i 1 (+ i 1)))
;;       ((> i n) t)
;;       (declare (type fixnum i))
;;    (setf c 0d0)
;;    (setf r 0d0)
;;    (do ((j 1 (+ j 1)))
;;       ((> j n) t)
;;       (declare (type fixnum j))
;;     (when (not (= j i)) 
;;      (setf c (+ c (abs (fref a j i))))
;;      (setf r (+ r (abs (fref a i j))))))

;;    (when (and (not (= c 0d0)) (not (= r 0d0)))
;;     (setf g (/ r radix))
;;     (setf f 1d0)
;;     (setf s (+ c r))

;; (tagbody label2

;;     (when (< c g)
;;      (setf f (* f radix))
;;      (setf c (* c sqrdx))
;;      (go label2)))

;;     (setf g (* r radix))
;; (tagbody    label3
;;     (when (> c g)
;;      (setf f (/ f radix))
;;      (setf c (/ c sqrdx))
;;      (go label3)))

;;     (when (< (/ (+ c r) f) (* 0.95d0 s))
;;      (setf last 0)
;;      (setf g (/ 1d0 f))
;;      (do ((j 1 (+ j 1)))
;;        ((> j n) t)
;;        (declare (type fixnum j))
;;       (setf (fref a i j) (* (fref a i j) g)))

;;      (do ((j 1 (+ j 1)))
;;        ((> j n) t)
;;        (declare (type fixnum j))
;;      (setf (fref a j i) (* (fref a j i) f))))))
  
;;   (if (= last 0) (go label1))
 
;;   (return a)))
;; ;-----------------------------------------------------------------------------

;; (defun elmhes (a)
;;  ;(declare (type (simple-array double-float (* *)) a))

;;  (prog ((n 0) (y 0d0) (x 0d0) (i 0)) 
;;   (declare (type fixnum n i) (type double-float y x))
;;   (setq n (array-dimension a 0))

;;   (do ((m 2 (+ m 1)))
;;       ((>  m (1- n)) t)
;;       (declare (type fixnum m))
;;     (setf x 0d0)
;;     (setf i m)
;;     (do ((j m (+ j 1)))
;;         ((> j n) t)
;;         (declare (type fixnum j))
;;       (when (> (abs (fref a j (1- m))) (abs x))
;;             (setf x (fref a j (1- m))) 
;;             (setf i j)))

;;     (when (not (= i m))
;;      (do ((j (1- m) (+ j 1)))
;;          ((> j n) t)
;;          (declare (type fixnum j))
;;        (setf y (fref a i j))
;;        (setf (fref a i j) (fref a m j))
;;        (setf (fref a m j) y))

;;      (do ((j 1 (+ j 1)))
;;          ((> j n) t)
;;          (declare (type fixnum j))
;;        (setf y (fref a j i))
;;        (setf (fref a j i) (fref a j m))
;;        (setf (fref a j m) y)))

;;     (when (not (= x 0d0))
;;      (do ((i (1+ m) (+ i 1)))
;;          ((> i n) t)
;;          (declare (type fixnum i))
;;       (setf y (fref a i (1- m)))
;;        (when (not (= y 0d0))
;;         (setf y (/ y x))
;;         (setf (fref a i (1- m)) y)
;;         (do ((j m (+ j 1)))
;;             ((> j n) t)
;;             (declare (type fixnum j))
;;           (setf (fref a i j) (+ (fref a i j) (* (- y) (fref a m j)))))
;;         (do ((j 1 (+ j 1)))
;;             ((> j n) t)
;;             (declare (type fixnum j))
;;           (setf (fref a j m) (+ (fref a j m) (* y (fref a j i))))))))) 
   
;;   (return a)))

;; ;------------------------------------------------------------------------------

;; (defun hqr (a)
;;  ;(declare (type (simple-array double-float (* *)) a)) 

;;  (prog* (
;;   (n (array-dimension a 0))
;;   (wr (make-array n :element-type 'double-float :initial-element 0d0))
;;   (wi (make-array n :element-type 'double-float :initial-element 0d0))
;;   (t0 0d0) (nn 0) (s 0d0) 
;;   (anorm 0d0) (its 0) (l 0) (x 0d0) (y 0d0) (m 0)
;;   (w 0d0) (p 0d0) (q 0d0) (z 0d0) (r 0d0) (u 0d0) (v 0d0))

;;   (declare (type (simple-array double-float (*)) wr)) 
;;   (declare (type (simple-array double-float (*)) wi)) 
;;   (declare (type fixnum n nn its l m))
;;   (declare (type double-float t0 s anorm x y w p q z r u v))


;;   (setf anorm (abs (fref a 1 1))) 
;;   (do ((i 2 (+ i 1)))
;;       ((> i n) t)
;;       (declare (type fixnum i))
;;     (do ((j (1- i) (1+ j)))
;;         ((> j n) t)
;;         (declare (type fixnum j))
;;       (setf anorm (+ anorm (abs (fref a i j)))))) 
;;   (setf nn n) 
;;   (setf t0 0d0) 
;;   label1 
;;   (when 
;;    (>= nn 1)  
;; (tagbody
;;    (setf its 0) 
;;    label2
;;    (do ((ll nn (1- ll)))
;;        ((< ll 2) t)
;;        (declare (type fixnum ll))
;;        (setq l ll)
;;      (setf s (+ (abs (fref a (1- ll) (1- ll)))
;;                 (abs (fref a ll ll))))
;;      (if (= s 0d0) (setf s anorm))
;;      (if (= (+ (abs (fref a ll  (1- ll))) s) s) (go label3)))
;;    (setf l 1) 
;;    label3 
;;    (setf x (fref a nn nn))
;;    (cond 
;;     ((= l nn)  
;;      (setf (fref wr nn) (+ x t0))
;;      (setf (fref wi nn) 0d0) 
;;      (setf nn (1- nn))) 
;;     (t
;;      (setf y (fref a (1- nn) (1- nn)))
;;      (setf w (* (fref a nn (1- nn)) (fref a (1- nn) nn)))
;;      (cond 
;;       ((= l (1- nn))
;;       (setf p (* 0.5d0 (- y x))) (setf q (+ (expt p 2) w))
;;       (setf z (sqrt (abs q))) (setf x (+ x t0))
;;       (cond 
;;        ((>= q 0d0) 
;;         (setf z (+ p (signp z p)))
;;         (setf (fref wr nn) (+ x z)) 
;;         (setf (fref wr (1- nn)) (fref wr nn))
;;         (if (not (= z 0d0)) (setf (fref wr nn) (+ x (/ (- w) z))))
;;         (setf (fref wi nn) 0d0) 
;;         (setf (fref wi (1- nn)) 0d0))
;;        (t 
;;         (setf (fref wr nn) (+ x p))
;;         (setf (fref wr (1- nn)) (fref wr nn)) 
;;         (setf (fref wi nn) z)
;;         (setf (fref wi (1- nn)) (- z))))
;;       (setf nn (- nn 2))) 

;; (t (if (= its 30) (error " too many iterations in hqr "))

;;      (when 
;;       (or (= its 10) (= its 20))
;;       (setf t0 (+ t0 x))

;;       (do ((i 1 (+ i 1)))
;;           ((> i nn) t)
;;           (declare (type fixnum i))
;;         (setf (fref a i i) (+ (fref a i i) (- x))))

;;       (setf s (+ (abs (fref a nn (1- nn)))
;;                  (abs (fref a (1- nn) (- nn 2)))))
;;       (setf x (* 0.75d0 s)) (setf y x) (setf w (* -0.4375d0 (expt s 2))))

;;      (setf its (+ its 1))
;; (tagbody
;;      (do ((mm (- nn 2) (1- mm)))
;;          ((< mm l) t)
;;          (declare (type fixnum mm))
;;        (setq m mm)
;;        (setf z (fref a mm mm))
;;        (setf r (- x z))
;;        (setf s (- y z))
;;        (setf p (+ (/ (- (* r s) w) (fref a (+ mm 1) mm))
;;                   (fref a mm (+ mm 1))))
;;        (setf q (- (- (- (fref a (+ mm 1) (+ mm 1)) z) r) s))
;;        (setf r (fref a (+ mm 2) (+ mm 1)))
;;        (setf s (+ (+ (abs p) (abs q)) (abs r)))
;;        (setf p (/ p s))
;;        (setf q (/ q s))
;;        (setf r (/ r s))
;;        (if (= mm l) (go label4))
;;        (setf u (* (abs (fref a mm (1- mm))) (+ (abs q) (abs r))))
;;        (setf v (* (abs p)
;;                   (+ (+ (abs (fref a (1- mm) (1- mm))) (abs z))
;;                      (abs (fref a (+ mm 1) (+ mm 1))))))
;;        (if (= (+ u  v) v) (go label4)))

;;      label4)
;;      (do ((i (+ m 2) (+ i 1)))
;;          ((> i nn) t)
;;          (declare (type fixnum i))
;;        (setf (fref a i (+ i -2)) 0d0)
;;        (if (not (= i (+ 2 m))) (setf (fref a i (+ i -3)) 0d0)))

;;      (do ((k m (+ k 1)))
;;          ((> k (1- nn)) t)
;;          (declare (type fixnum k))
;;        (when 
;;         (not (= k m))
;;         (setf p (fref a k (1- k)))
;;         (setf q (fref a (+ k 1) (1- k))) (setf r 0d0)
;;         (if (not (= k (1- nn)))
;;           (setf r (fref a (+ k 2) (1- k))))
;;         (setf x (+ (+ (abs p) (abs q)) (abs r)))
;;         (when 
;;          (not (= x 0d0))
;;          (setf p (/ p x))
;;          (setf q (/ q x)) 
;;          (setf r (/ r x))))
;;        (setf s (signp (sqrt (+ (+ (expt p 2) (expt q 2)) (expt r 2))) p))

;;        (when 
;;         (not (= s 0d0))
;;         (if 
;;          (= k m) 
;;          (if 
;;            (not (= l m))
;;            (setf (fref a k (1- k)) (- (fref a k (1- k)))))
;;          (setf (fref a k (1- k)) (* (- s) x)))

;;         (setf p (+ p s)) 
;;         (setf x (/ p s)) 
;;         (setf y (/ q s)) 
;;         (setf z (/ r s))
;;         (setf q (/ q p)) 
;;         (setf r (/ r p))

;;         (do ((j k (+ j 1)))
;;             ((> j nn) t)
;;             (declare (type fixnum j))
;;           (setf p (+ (fref a k j) (* q (fref a (+ k 1) j))))
;;           (when 
;;            (not (= k (1- nn)))
;;            (setf p (+ p (* r (fref a (+ k 2) j))))
;;            (setf (fref a (+ k 2) j) (- (fref a (+ k 2) j) (* p z))))
;;           (setf (fref a (+ k 1) j) (- (fref a (+ k 1) j) (* p y)))
;;           (setf (fref a k j) (- (fref a k j) (* p x))))

;;         (do ((i l (+ i 1)))
;;             ((> i (min nn (+ k 3))) t)
;;             (declare (type fixnum i))
;;           (setf p (+ (* x (fref a i k)) (* y (fref a i (+ k 1)))))
;;           (when 
;;            (not (= k (1- nn)))
;;            (setf p (+ p (* z (fref a i (+ k 2)))))
;;            (setf (fref a i (+ k 2)) (- (fref a i (+ k 2)) (* p r))))
;;           (setf (fref a i (+ k 1)) (- (fref a i (+ k 1)) (* p q)))
;;           (setf (fref a i k) (- (fref a i k) p)))))
;;      (go label2)))
;;    ))
;;    (go label1))) 
   
;;   (return (values wr wi))))
;; ;------------------------------------------------------------------------------
;; ; end of nr11.l
