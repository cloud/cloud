;;; -*- Mode: Lisp; Package: CLOUD-MATHS -*-

;;;  (c) copyright 2004-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :cloud-maths)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  This file provides functions to perform vector
;;;  operations.
;;;  

(defun scalar-product (a b)
  "Also called dot product. Performs the scalar product of two vectors."
  (iterate (for i in-vector a)
	   (for j in-vector b)
	   (sum (* i j))))

(defun v-abs (a)
  "Returns the vector of absolute values of a vector."
  (v-fun #'abs a))

(defun v-make (beg end &key (by 1) (element-type t))
  "Makes a vector from increasing or decreasing values.
;;;  MATHS> (v-make 1 10)
;;; #(1 2 3 4 5 6 7 8 9 10)
;;; MATHS> (v-make 1 10 :by 2)
;;; #(1 3 5 7 9)
;;; MATHS> (v-make 10 1 :by -3)
;;; #(10 7 4 1)
"
  (let (reverse)
    (when (< end beg)
      (rotatef end beg)
      (setf by (abs by))
      (setf reverse t))
    (let ((array (make-array (1+ (floor (/ (- end beg) by)))
		  :element-type element-type)))
      (iterate (for i index-of-vector array)
	       (for j from beg below (+ end by) by by)
	       (setf (aref array i) j))
      (if reverse
	  (nreverse array)
	  array))))


;; This should be handled differently. We should get the return type
;; of fun and give the appropriate type as return type of map.
(defun v-fun (fun a &optional (return-type t))
  "Apply a function to an array. "
  (let ((res (make-array (length a) :element-type return-type)))
    (iter (for i index-of-vector a)
	  (for v in-vector a)
	  (setf (aref res i) (funcall fun v))
	  (finally (return res)))))

(defun save-array (a filename)
  "Saves the values of a vector in an ascii file, vertically."
  (with-open-file (s filename :direction :output :if-exists :supersede)
    (iterate (for i in-vector a)
	     (format s "~a~%" i))))


(defun v-sum (a)
  "Sums the elements of a vector."
  (iterate (for i in-vector a)
	   (sum i)))

(defun diff (s &key (pad nil))
  "Calculates differences between adjacent elements of an vector
  s. Set the pad argument to T to add a zero in the end of the
  result vector."
  (let ((result (make-array (if pad
				(length s)
				(1- (length s)))
			    :element-type (array-element-type s))))
    (iterate (for i in-vector s from 1)
	     (for j in-vector s)
	     (for k index-of-vector result)
	     (setf (aref result k) (- i j))
	     (finally (return result)))))

(defun mean (s)
  "Computes the mean value of the elements of a
  sequence."
  (/ (iterate (for i in-sequence s)
	      (sum i))
     (length s)))

(defun variance (s)
  "Computes the variance of the elements of a sequence."
  (/ (iterate (for i in-sequence s)
	      (sum (* i i)))
     (length s)))

(defun v-max (s)
  "Finds the maximum value of a sequence."
  (reduce #'max s))

(defun v-min (s)
  "Finds the minimum value of a sequence."
  (reduce #'min s))

(defun swap-array (in-array)
  "Rotates a vector by its half-size.
;;; MATHS> (swap-array #(1 2 3 4))
;;; #(3 4 1 2)
"
  (let ((half-size (/ (length in-array) 2)))
    (concatenate (type-of in-array) 
		 (make-array half-size :displaced-to in-array
			     :element-type (array-element-type in-array)
			     :displaced-index-offset half-size)
		 (make-array half-size :displaced-to in-array
			     :element-type (array-element-type in-array)))))
    

(defun pad-array (array &key (before 0) (after 0) (pad-value 0))
  "Adds elements of value pad-value before and/or after the vector."
  (let* ((element-type (array-element-type array))
	 (result-array (make-array (+ (length array)
				      before
				      after)
			:element-type element-type
			:initial-element (coerce pad-value element-type))))
    (iter (for i below (length array))
	  (for j = (+ i before))
	  (setf (aref result-array j) (aref array i)))
    result-array))
    


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Arithmetic operations for vectors
;;;

(defun v+ (&rest args)
  "Adds numbers and/or vectors element-wise."
  (if args
      (if (> (length args) 1)
	  (reduce #'two-args-v+ args)
	  (first args))
      0))
	  
      

;; This should be handled differently. We should get the type
;; of the arrays and give the appropriate type to the result array.
(defun two-args-v+ (arg1 arg2)
  (if (and (numberp arg1) (numberp arg2))
      (+ arg1 arg2)
      (if (arrayp arg1)
	  (if (arrayp arg2)
	      (if (eq (array-element-type arg1)
		      (array-element-type arg2))
		  (map (list 'vector (array-element-type arg1))
		       #'+ arg1 arg2)
		  (iterate (for i in-vector arg1)
			   (for j in-vector arg2)
			   (collect (+ i j) result-type 'vector)))
	      (iterate (for i in-vector arg1)
		       (collect (+ i arg2) result-type 'vector)))
	  (iterate (for j in-vector arg2)
		   (collect (+ arg1 j) result-type 'vector)))))

(defun v- (arg1 &rest args)
  "Substracts numbers and/or vectors element-wise."
  (if args
      (two-args-v- arg1 (apply #'v+ args))
      (if (numberp arg1)
	  (- arg1)
	  (v-fun #'- arg1))))

;; This should be handled differently. We should get the type
;; of the arrays and give the appropriate type to the result array.
(defun two-args-v- (arg1 arg2)
  (if (and (numberp arg1) (numberp arg2))
      (- arg1 arg2)
      (if (arrayp arg1)
	  (if (arrayp arg2)
	      (if (eq (array-element-type arg1)
		      (array-element-type arg2))
		  (map (list 'vector (array-element-type arg1))
		       #'- arg1 arg2)
		  (iterate (for i in-vector arg1)
			   (for j in-vector arg2)
			   (collect (- i j) result-type 'vector)))
	      (iterate (for i in-vector arg1)
		       (collect (- i arg2) result-type 'vector)))
	  (iterate (for j in-vector arg2)
		   (collect (- arg1 j) result-type 'vector)))))

(defun v* (&rest args)
  "Multiplies numbers and/or vectors element-wise."
  (if args
      (reduce #'two-args-v* args)
      1))

;; This should be handled differently. We should get the type
;; of the arrays and give the appropriate type to the result array.
(defun two-args-v* (arg1 arg2)
  (if (and (numberp arg1) (numberp arg2))
      (* arg1 arg2)
      (if (arrayp arg1)
	  (if (arrayp arg2)
	      (let ((res (make-array (length arg1)
				     :element-type (array-element-type arg1) 
				     :adjustable nil)))
		(iterate (for i in-vector arg1)
			 (for j in-vector arg2)
			 (for k from 0)
			 (setf (aref res k) (* i j))
			 (finally (return res))))
	      (let ((res (make-array (length arg1)
				     :element-type (array-element-type arg1) 
				     :adjustable nil)))
		(iterate (for i in-vector arg1)
			 (for k from 0)
			 (setf (aref res k) (* i arg2))
			 (finally (return res)))))
	  (let ((res (make-array (length arg2)
				 :element-type (array-element-type arg2) 
				 :adjustable nil)))
	    (iterate (for i in-vector arg2)
		     (for k from 0)
		     (setf (aref res k) (* i arg1))
		     (finally (return res)))))))

(defun v/ (arg1 &rest args)
  "Divides numbers and/or vectors element-wise."
  (let ((denominator (apply #'v* args)))
    (two-args-v* arg1
		 (if (numberp denominator)
		     (/ denominator)
		     (v-fun #'/ denominator)))))

