;;; -*- Mode: Lisp; Package: COMMON-LISP-USER -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(use-package :asdf)

(defsystem :cloud-sound
    :depends-on (:iterate :cloud-maths :read-bytes)
    :components ((:file "package")
		 (:file "sound")
		 (:file "tools")
		 (:file "wave")
		 (:file "aiff")
		 (:file "soundfile"))
    :serial t)
    

(defun load-cloud-sound  ()
  (asdf:operate 'asdf:load-op 'cloud-sound))
