;;; -*- Mode: Lisp; Package: COMMON-LISP-USER -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(defpackage :cloud.sound
  (:nicknames :cloud-sound)
  (:use :iterate :cloud.maths :read-bytes :cl :cl-user)
  (:export sound
	   get-samples
	   nb-channels
	   sample-rate
	   bits-per-sample
	   frames
	   samples
	   pointer-position
	   point-to
	   channel
	   datafile
	   read-sound
	   write-sound))

(provide :cloud.sound)