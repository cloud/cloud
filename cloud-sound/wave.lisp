;;; -*- Mode: Lisp; Package: CLOUD.SOUND -*-

;;;  (c) copyright 2003-2004 by
;;;           Robert Strandh <strandh@labri.fr>
;;;
;;;  (c) copyright 2003-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.sound)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Wav file support
(eval-when (:compile-toplevel :load-toplevel)
  (define-endianess :little-endian))

(defconstant +wave-header-riff+ (string-code "RIFF"))
(defconstant +wave-header-wave+ (string-code "WAVE"))
(defconstant +wave-header-fmt+ (string-code "fmt "))
(defconstant +wave-header-data+ (string-code "data"))
(defconstant +wave-header-fact+ (string-code "fact"))

;;; WAVE form wFormatTag IDs

(defconstant +wave-format-unknown+                    #x0000) ; Microsoft Corporation
(defconstant +wave-format-pcm+                        #x0001) ; Microsoft Corporation
(defconstant +wave-format-adpcm+                      #x0002) ; Microsoft Corporation 
(defconstant +wave-format-ieee-float+                 #x0003) ; Microsoft Corporation 
(defconstant +wave-format-vselp+                      #x0004) ; Compaq Computer Corp. 
(defconstant +wave-format-ibm-cvsd+                   #x0005) ; IBM Corporation 
(defconstant +wave-format-alaw+                       #x0006) ; Microsoft Corporation 
(defconstant +wave-format-mulaw+                      #x0007) ; Microsoft Corporation 
(defconstant +wave-format-dts+                        #x0008) ; Microsoft Corporation 
(defconstant +wave-format-oki-adpcm+                  #x0010) ; OKI 
(defconstant +wave-format-dvi-adpcm+                  #x0011) ; Intel Corporation 
(defconstant +wave-format-ima-adpcm+                  #x0011) ; Intel Corporation 
(defconstant +wave-format-mediaspace-adpcm+           #x0012) ; Videologic 
(defconstant +wave-format-sierra-adpcm+               #x0013) ; Sierra Semiconductor Corp 
(defconstant +wave-format-g723-adpcm+                 #x0014) ; Antex Electronics Corporation 
(defconstant +wave-format-digistd+                    #x0015) ; DSP Solutions, Inc. 
(defconstant +wave-format-digifix+                    #x0016) ; DSP Solutions, Inc. 
(defconstant +wave-format-dialogic-oki-adpcm+         #x0017) ; Dialogic Corporation 
(defconstant +wave-format-mediavision-adpcm+          #x0018) ; Media Vision, Inc. 
(defconstant +wave-format-cu-codec+                   #x0019) ; Hewlett-Packard Company 
(defconstant +wave-format-yamaha-adpcm+               #x0020) ; Yamaha Corporation of America 
(defconstant +wave-format-sonarc+                     #x0021) ; Speech Compression 
(defconstant +wave-format-dspgroup-truespeech+        #x0022) ; DSP Group, Inc 
(defconstant +wave-format-echosc1+                    #x0023) ; Echo Speech Corporation 
(defconstant +wave-format-audiofile-af36+             #x0024) ; Virtual Music, Inc. 
(defconstant +wave-format-aptx+                       #x0025) ; Audio Processing Technology 
(defconstant +wave-format-audiofile-af10+             #x0026) ; Virtual Music, Inc. 
(defconstant +wave-format-prosody-1612+               #x0027) ; Aculab plc 
(defconstant +wave-format-lrc+                        #x0028) ; Merging Technologies S.A. 
(defconstant +wave-format-dolby-ac2+                  #x0030) ; Dolby Laboratories 
(defconstant +wave-format-gsm610+                     #x0031) ; Microsoft Corporation 
(defconstant +wave-format-msnaudio+                   #x0032) ; Microsoft Corporation 
(defconstant +wave-format-antex-adpcme+               #x0033) ; Antex Electronics Corporation 
(defconstant +wave-format-control-res-vqlpc+          #x0034) ; Control Resources Limited 
(defconstant +wave-format-digireal+                   #x0035) ; DSP Solutions, Inc. 
(defconstant +wave-format-digiadpcm+                  #x0036) ; DSP Solutions, Inc. 
(defconstant +wave-format-control-res-cr10+           #x0037) ; Control Resources Limited 
(defconstant +wave-format-nms-vbxadpcm+               #x0038) ; Natural MicroSystems 
(defconstant +wave-format-cs-imaadpcm+                #x0039) ; Crystal Semiconductor IMA ADPCM 
(defconstant +wave-format-echosc3+                    #x003A) ; Echo Speech Corporation 
(defconstant +wave-format-rockwell-adpcm+             #x003B) ; Rockwell International 
(defconstant +wave-format-rockwell-digitalk+          #x003C) ; Rockwell International 
(defconstant +wave-format-xebec+                      #x003D) ; Xebec Multimedia Solutions Limited 
(defconstant +wave-format-g721-adpcm+                 #x0040) ; Antex Electronics Corporation 
(defconstant +wave-format-g728-celp+                  #x0041) ; Antex Electronics Corporation 
(defconstant +wave-format-msg723+                     #x0042) ; Microsoft Corporation 
(defconstant +wave-format-mpeg+                       #x0050) ; Microsoft Corporation 
(defconstant +wave-format-rt24+                       #x0052) ; InSoft, Inc. 
(defconstant +wave-format-pac+                        #x0053) ; InSoft, Inc. 
(defconstant +wave-format-mpeglayer3+                 #x0055) ; ISO/MPEG Layer3 Format Tag 
(defconstant +wave-format-lucent-g723+                #x0059) ; Lucent Technologies 
(defconstant +wave-format-cirrus+                     #x0060) ; Cirrus Logic 
(defconstant +wave-format-espcm+                      #x0061) ; ESS Technology 
(defconstant +wave-format-voxware+                    #x0062) ; Voxware Inc 
(defconstant +wave-format-canopus-atrac+              #x0063) ; Canopus, co., Ltd. 
(defconstant +wave-format-g726-adpcm+                 #x0064) ; APICOM 
(defconstant +wave-format-g722-adpcm+                 #x0065) ; APICOM 
(defconstant +wave-format-dsat-display+               #x0067) ; Microsoft Corporation 
(defconstant +wave-format-voxware-byte-aligned+       #x0069) ; Voxware Inc 
(defconstant +wave-format-voxware-ac8+                #x0070) ; Voxware Inc 
(defconstant +wave-format-voxware-ac10+               #x0071) ; Voxware Inc 
(defconstant +wave-format-voxware-ac16+               #x0072) ; Voxware Inc 
(defconstant +wave-format-voxware-ac20+               #x0073) ; Voxware Inc 
(defconstant +wave-format-voxware-rt24+               #x0074) ; Voxware Inc 
(defconstant +wave-format-voxware-rt29+               #x0075) ; Voxware Inc 
(defconstant +wave-format-voxware-rt29hw+             #x0076) ; Voxware Inc 
(defconstant +wave-format-voxware-vr12+               #x0077) ; Voxware Inc 
(defconstant +wave-format-voxware-vr18+               #x0078) ; Voxware Inc 
(defconstant +wave-format-voxware-tq40+               #x0079) ; Voxware Inc 
(defconstant +wave-format-softsound+                  #x0080) ; Softsound, Ltd. 
(defconstant +wave-format-voxware-tq60+               #x0081) ; Voxware Inc 
(defconstant +wave-format-msrt24+                     #x0082) ; Microsoft Corporation 
(defconstant +wave-format-g729a+                      #x0083) ; AT&T Labs, Inc. 
(defconstant +wave-format-mvi-mvi2+                   #x0084) ; Motion Pixels 
(defconstant +wave-format-df-g726+                    #x0085) ; DataFusion Systems (Pty) (Ltd) 
(defconstant +wave-format-df-gsm610+                  #x0086) ; DataFusion Systems (Pty) (Ltd) 
(defconstant +wave-format-isiaudio+                   #x0088) ; Iterated Systems, Inc. 
(defconstant +wave-format-onlive+                     #x0089) ; OnLive! Technologies, Inc. 
(defconstant +wave-format-sbc24+                      #x0091) ; Siemens Business Communications Sys 
(defconstant +wave-format-dolby-ac3-spdif+            #x0092) ; Sonic Foundry 
(defconstant +wave-format-mediasonic-g723+            #x0093) ; MediaSonic 
(defconstant +wave-format-prosody-8kbps+              #x0094) ; Aculab plc 
(defconstant +wave-format-zyxel-adpcm+                #x0097) ; ZyXEL Communications, Inc. 
(defconstant +wave-format-philips-lpcbb+              #x0098) ; Philips Speech Processing 
(defconstant +wave-format-packed+                     #x0099) ; Studer Professional Audio AG 
(defconstant +wave-format-malden-phonytalk+           #x00A0) ; Malden Electronics Ltd. 
(defconstant +wave-format-rhetorex-adpcm+             #x0100) ; Rhetorex Inc. 
(defconstant +wave-format-irat+                       #x0101) ; BeCubed Software Inc. 
(defconstant +wave-format-vivo-g723+                  #x0111) ; Vivo Software 
(defconstant +wave-format-vivo-siren+                 #x0112) ; Vivo Software 
(defconstant +wave-format-digital-g723+               #x0123) ; Digital Equipment Corporation 
(defconstant +wave-format-sanyo-ld-adpcm+             #x0125) ; Sanyo Electric Co., Ltd. 
(defconstant +wave-format-siprolab-aceplnet+          #x0130) ; Sipro Lab Telecom Inc. 
(defconstant +wave-format-siprolab-acelp4800+         #x0131) ; Sipro Lab Telecom Inc. 
(defconstant +wave-format-siprolab-acelp8v3+          #x0132) ; Sipro Lab Telecom Inc. 
(defconstant +wave-format-siprolab-g729+              #x0133) ; Sipro Lab Telecom Inc. 
(defconstant +wave-format-siprolab-g729a+             #x0134) ; Sipro Lab Telecom Inc. 
(defconstant +wave-format-siprolab-kelvin+            #x0135) ; Sipro Lab Telecom Inc. 
(defconstant +wave-format-g726adpcm+                  #x0140) ; Dictaphone Corporation 
(defconstant +wave-format-qualcomm-purevoice+         #x0150) ; Qualcomm, Inc. 
(defconstant +wave-format-qualcomm-halfrate+          #x0151) ; Qualcomm, Inc. 
(defconstant +wave-format-tubgsm+                     #x0155) ; Ring Zero Systems, Inc. 
(defconstant +wave-format-msaudio1+                   #x0160) ; Microsoft Corporation 
(defconstant +wave-format-creative-adpcm+             #x0200) ; Creative Labs, Inc 
(defconstant +wave-format-creative-fastspeech8+       #x0202) ; Creative Labs, Inc 
(defconstant +wave-format-creative-fastspeech10+      #x0203) ; Creative Labs, Inc 
(defconstant +wave-format-uher-adpcm+                 #x0210) ; UHER informatic GmbH 
(defconstant +wave-format-quarterdeck+                #x0220) ; Quarterdeck Corporation 
(defconstant +wave-format-ilink-vc+                   #x0230) ; I-link Worldwide 
(defconstant +wave-format-raw-sport+                  #x0240) ; Aureal Semiconductor 
(defconstant +wave-format-ipi-hsx+                    #x0250) ; Interactive Products, Inc. 
(defconstant +wave-format-ipi-rpelp+                  #x0251) ; Interactive Products, Inc. 
(defconstant +wave-format-cs2+                        #x0260) ; Consistent Software 
(defconstant +wave-format-sony-scx+                   #x0270) ; Sony Corp. 
(defconstant +wave-format-fm-towns-snd+               #x0300) ; Fujitsu Corp. 
(defconstant +wave-format-btv-digital+                #x0400) ; Brooktree Corporation 
(defconstant +wave-format-qdesign-music+              #x0450) ; QDesign Corporation 
(defconstant +wave-format-vme-vmpcm+                  #x0680) ; AT&T Labs, Inc. 
(defconstant +wave-format-tpc+                        #x0681) ; AT&T Labs, Inc. 
(defconstant +wave-format-oligsm+                     #x1000) ; Ing C. Olivetti & C., S.p.A. 
(defconstant +wave-format-oliadpcm+                   #x1001) ; Ing C. Olivetti & C., S.p.A. 
(defconstant +wave-format-olicelp+                    #x1002) ; Ing C. Olivetti & C., S.p.A. 
(defconstant +wave-format-olisbc+                     #x1003) ; Ing C. Olivetti & C., S.p.A. 
(defconstant +wave-format-oliopr+                     #x1004) ; Ing C. Olivetti & C., S.p.A. 
(defconstant +wave-format-lh-codec+                   #x1100) ; Lernout & Hauspie 
(defconstant +wave-format-norris+                     #x1400) ; Norris Communications, Inc. 
(defconstant +wave-format-soundspace-musicompress+    #x1500) ; AT&T Labs, Inc. 
(defconstant +wave-format-dvm+                        #x2000) ; FAST Multimedia AG 
(defconstant +wave-format-extensible+                 #xFFFE) ;* Microsoft */
(defconstant +wave-format-development+                #xFFFF)

;;; end of formats

(defparameter *wave-input* nil "stream for reading a Wave file")
(defparameter *input-buffer* '() "used for unreading bytes from *wave-input")
(defparameter *wave-output* nil "stream for writing a Wave file")

(defmacro with-wave-input (filename &body body)
  "execute body with *wave-input* assigned to a stream from filename"
  `(with-open-file (*wave-input* ,filename
		    :direction :input :element-type '(unsigned-byte 8))
    ,@body))

(defmacro with-wave-output (filename &body body)
  "execute body with *wave-output* assigned to a stream from filename"
  `(with-open-file (*wave-output* ,filename
		    :direction :output :if-exists :supersede :element-type '(unsigned-byte 8))
    ,@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Wave file representation in memory

;; (defmethod play ((snd wave))
;;   (with-slots (sample-rate bits-per-sample nb-channels data)
;;       snd
;;     (dsp:with-soundcard sc
;;       (if (zerop (+ (dsp:set-sample-size sc bits-per-sample)
;; 		    (dsp:set-channel-num sc nb-channels)
;; 		    (dsp:set-sample-rate sc sample-rate)))
;; 	  (dsp:write-out-array sc data)
;; 	  (error "Hardware configuration error, or unsupported format!")))))

    
(define-condition header ()
  ((header-type :initarg :header :reader header-type))
  (:documentation "condition when the header is not correct"))

(define-condition chunk-size ()
  ())

(define-condition file-format ()
  ())

(defun read-wave (filename)
  "Create a instance of sound from a wav file."
  (read-wave-file-into-instance! (make-instance 'sound) filename))

(defun read-wave-file-into-instance! (sound filename)
  "Loads a wave-file into a sound instance."
  (with-wave-input filename
    (define-endianess :little-endian)
    (define-input-stream *wave-input*)
    (unless (= (read-unsigned-integer 4) +wave-header-riff+)
      (error (make-condition 'header :header "RIFF")))
    (read-unsigned-integer 4) ; skip chunk size
    (unless (= (read-unsigned-integer 4) +wave-header-wave+)
      (error (make-condition 'header :header "WAVE")))
    (unless (= (read-unsigned-integer 4) +wave-header-fmt+)
      (error (make-condition 'header :header "fmt ")))
    (let* ((format-block-size (read-unsigned-integer 4))
	   (format-type (read-unsigned-integer 2))
	   (extension-size (- format-block-size 16))
	   (nb-channels (read-unsigned-integer 2))
	   (sample-rate (read-unsigned-integer 4))
	   (byte-rate (read-unsigned-integer 4))
	   (block-align (read-unsigned-integer 2)))
      (declare (ignore extension-size byte-rate block-align))
      (unless (= format-type +wave-format-pcm+) ; we only handle PCM for now
	(error (make-condition 'file-format)))
      (let ((bits-per-sample (read-unsigned-integer 2)))
	(unless (= (read-unsigned-integer 4) +wave-header-data+)
	  (error (make-condition 'header :header "data")))
	(let* ((size (read-unsigned-integer 4))
	       (frames (* size (/ 8 (* bits-per-sample nb-channels))))
	       (data (if (> nb-channels 1)
			 (make-array (list frames nb-channels) :element-type 'double-float)
			 (make-array frames :element-type 'double-float))))
	  (read-data-sequence data bits-per-sample size)
	  (with-slots ((wi-nb-channels nb-channels)
		       (wi-sample-rate sample-rate)
		       (wi-data samples))
	      sound
	    (setf wi-nb-channels nb-channels
		  wi-sample-rate sample-rate
		  wi-data data))))))
  sound)


(defun write-wave (sound filename &key (bits-per-sample 16))
  "Write the sound instance into a wav file."
  (with-wave-output filename
    (let* ((nb-channels (nb-channels sound))
	   (sample-rate (round (sample-rate sound)))
	   (frames (array-dimension (samples sound) 0))
	   (data (make-array (array-dimensions (samples sound))
			     :element-type (list 'signed-byte
						 bits-per-sample))))

      (iterate (for i below (* frames nb-channels))
	       (setf (row-major-aref data i)
		     (round (* (row-major-aref (samples sound) i)
			       (expt 2 (1- bits-per-sample))))))
      (define-endianess :little-endian)
      (define-output-stream *wave-output*)
      (write-unsigned-integer +wave-header-riff+ 4)
      (write-unsigned-integer (+ (* frames nb-channels (ash bits-per-sample -3)) 36) 4)
      (write-unsigned-integer +wave-header-wave+ 4)
      (write-unsigned-integer +wave-header-fmt+ 4)
      (write-unsigned-integer 16 4)
      (write-unsigned-integer 1 2) ; Just PCM for now
      (write-unsigned-integer nb-channels 2)
      (write-unsigned-integer sample-rate 4)
      (write-unsigned-integer (* sample-rate nb-channels (ash bits-per-sample -3)) 4)
      (write-unsigned-integer 2 2)
      (write-unsigned-integer bits-per-sample 2)
      (write-unsigned-integer +wave-header-data+ 4)
      (write-unsigned-integer (* frames nb-channels (ash bits-per-sample -3)) 4)
      (iter (for i from 0 below (* frames nb-channels))
	    (write-signed-integer (row-major-aref data i) (ash bits-per-sample -3))))))