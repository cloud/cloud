;;; -*- Mode: Lisp; Package: CLOUD.SOUND -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.sound)

(defclass sound ()
  ((nb-channels :initarg :nb-channels :accessor nb-channels)
   (sample-rate :initarg :sample-rate :accessor sample-rate)
   (samples :initarg :samples :accessor samples)
   (pointer-position :initarg :pointer-position :initform 0 :accessor pointer-position)
   (datafile :initarg :datafile :reader datafile)))


(defmethod get-samples ((s sound) &key start end next)
  (when (and next (not end))
    (if start
	(setf end (+ start next))
	(setf end (+ (pointer-position s) next))))
  (if start
      (if end
	  (read-samples s start end)
	  (read-samples s start (1+ start)))
      (if end
	  (read-samples s (pointer-position s) end)
	  (read-samples s (pointer-position s) 
			(1+ (pointer-position s))))))


(defmethod read-samples ((s sound) start end)
  (if (> (nb-channels s) 1)
      (squeeze (subarray (samples s) (list start (setf (pointer-position s) end)) nil))
      (subseq (samples s) start (setf (pointer-position s) end))))

(defmethod point-to ((s sound) pos)
  (setf (pointer-position s) pos))

(defun channel (s chan)
  (when (> (nb-channels s) chan)
    (if (= (array-rank (samples s)) 1)
	(samples s)
	(squeeze (subarray (samples s) nil chan)))))