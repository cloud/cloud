;;; -*- Mode: Lisp; Package: CLOUD.SOUND -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.sound)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Common tools for sound file handling

(defun read-data-sequence (data bits-per-sample size)
  "read a size sized data sequence of bits-per-sample bits signed
integers from the current input. Interleaved format."
  (let ((divisor (expt 2.0d0 (1- bits-per-sample))))
    (dotimes (i (* (/ size bits-per-sample) 8))
      (setf (row-major-aref data i) (/ (read-signed-integer (/ bits-per-sample 8)) 
				       divisor)))))