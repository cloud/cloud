;;; -*- Mode: Lisp; Package: CLOUD.SOUND -*-

;;;  (c) copyright 2004-2009 by
;;;           Robert Strandh <strandh@labri.fr>
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.sound)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Aiff file support

(eval-when (:compile-toplevel :load-toplevel)
  (define-endianess :big-endian))

(defconstant +aiff-header-form+ (string-code "FORM"))
(defconstant +aiff-header-aiff+ (string-code "AIFF"))
(defconstant +aiff-header-comm+ (string-code "COMM"))
(defconstant +aiff-header-ssnd+ (string-code "SSND"))
(defconstant +aiff-header-comt+ (string-code "COMT"))
(defconstant +aiff-header-copy+ (string-code "(c) "))
(defconstant +aiff-header-name+ (string-code "NAME"))
(defconstant +aiff-header-auth+ (string-code "AUTH"))
(defconstant +aiff-header-anno+ (string-code "ANNO"))

(defparameter *aiff-input* nil "stream for reading an Aiff file")
(defparameter *input-buffer* '() "used for unreading bytes from *aiff-input")
(defparameter *aiff-output* nil "stream for writing an Aiff file")


(defmacro with-aiff-input (filename &body body)
  "execute body with *aiff-input* assigned to a stream from filename"
  `(with-open-file (*aiff-input* ,filename
		    :direction :input :element-type '(unsigned-byte 8))
    ,@body))

(defmacro with-aiff-output (filename &body body)
  "execute body with *aiff-output* assigned to a stream from filename"
  `(with-open-file (*aiff-output* ,filename
		    :direction :output :element-type '(unsigned-byte 8))
    ,@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Aiff file representation in memory

;; (defmethod play ((snd aiff))
;;   (with-slots (sample-rate bits-per-sample nb-channels data)
;;       snd
;;     (dsp:with-soundcard sc
;;       (if (zerop (+ (dsp:set-sample-size sc bits-per-sample)
;; 		    (dsp:set-channel-num sc nb-channels)
;; 		    (dsp:set-sample-rate sc sample-rate)))
;; 	  (dsp:write-out-array sc data)
;; 	(error "Hardware configuration error, or unsupported format!")))))



(define-condition header ()
  ((header-type :initarg :header :reader header-type))
  (:documentation "condition when the header is not correct"))

(define-condition chunk-size ()
  ())

(defun read-aiff (filename)
  "Create a instance of sound from an aiff file."
  (read-aiff-file-into-instance! (make-instance 'sound) filename))

(defun read-aiff-file-into-instance! (sound filename)
  (with-aiff-input filename
    (define-endianess :big-endian)
    (define-input-stream *aiff-input*)
    (unless (= (read-unsigned-integer 4) +aiff-header-form+)
      (error (make-condition 'header :header "FORM")))
    (read-unsigned-integer 4) ; skip chunk size
    (unless (= (read-unsigned-integer 4) +aiff-header-aiff+)
      (error (make-condition 'header :header "AIFF")))
    (loop until (= (read-unsigned-integer 4) +aiff-header-comm+)  ; skip author, (c), comments, etc.
	  do (read-unsigned-integer (read-unsigned-integer 4)))
    (unless (= (read-unsigned-integer 4) 18)
      (error (make-condition 'chunk-size)))
    (let* ((nb-channels (read-unsigned-integer 2))
	   (frames (read-unsigned-integer 4))
	   (bits-per-sample (read-unsigned-integer 2))
	   (sample-rate (read-IEEE-SANE-extended-float)))
      (unless (= (read-unsigned-integer 4) +aiff-header-ssnd+)
	(error (make-condition 'header :header "SSND")))
      (let* ((size (read-unsigned-integer 4))
	     (offset (read-unsigned-integer 4))
	     (block-size (read-unsigned-integer 4))
	     (data (if (> nb-channels 1)
		       (make-array (list frames nb-channels) :element-type 'double-float)
		       (make-array frames :element-type 'double-float))))
	(declare (ignore offset block-size))
	(read-data-sequence data bits-per-sample (- size 8))
	(with-slots ((ai-nb-channels nb-channels)
		     (ai-sample-rate sample-rate)
		     (ai-data samples))
	    sound
	  (setf ai-nb-channels nb-channels
		ai-sample-rate (round sample-rate)
		ai-data data)))))
 sound)		