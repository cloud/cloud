;;; -*- Mode: Lisp; Package: CLOUD.SOUND -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :cloud.sound)

(defun read-sound (filename)
  (let ((ext (subseq filename (position #\. filename :from-end t))))
    (cond
      ((equal ext ".wav")
       (read-wave filename))
      ((equal ext ".aiff")
       (read-aiff filename))
      (t (print "File format not supported.")
	 (print ext)))))

(defun write-sound (s filename)
  (let ((ext (subseq filename (position #\. filename :from-end t))))
    (cond
      ((equal ext ".wav")
       (write-wave s filename))
      (t (print "File format not supported.")
	 (print ext)))))


