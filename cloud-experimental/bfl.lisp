;;; -*- Mode: Lisp; Package: CLOUD.BLISS -*-

;;;  (c) copyright 2008 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Library General Public License for more details.
;;;
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA  02111-1307  USA.


(in-package :cloud-bliss)

(defun distribute (az amps &optional (l 181))
  (let ((res (make-array l)))
    (iter (for i index-of-vector az)
	  (when (and (aref az i)
		     (> i 0))
	    (setf (aref res (round (+ (* (aref az i) l (/ 1 2 pi)) (1+ (/ (1- l) 2)))))
		  (aref amps i))))
    res))

(defun bfl (filename)
  (let* ((sound (read-sound filename))
	 (frame-length 512)
	 (hopsize 512)
	 (data (samples sound)))
    
    (with-stereo-sliding-window (data frame 
				      :frame-length frame-length
				      :hopsize hopsize)
      
				      
	  
	  ;;; Analyse

      (multiple-value-bind (az amps)
	    (azimuth-estimation (left frame) (right frame))
	(plot (distribute az amps 181)))
      (read-line)))
    ;;; Stockage

    ;;; Feedback

    ;;; Classification
    
    ;;; Feedback

  (print "done"))