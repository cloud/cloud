;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2007-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Library General Public License for more details.
;;;
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA  02111-1307  USA.

(in-package :cloud-clicks)

(use-package :sb-thread)

(defparameter buffer-count 0)

(defmacro make-lockable-buffer (name)
  
  (let* ((sname (symbol-name name))
	 (queue-name (intern (format nil "~A-QUEUE" sname)))
	 (mutex-name (intern (format nil "~A-LOCK" sname)))
	 (writer-thread-name (intern (format nil "~A-WRITER-THREAD" sname))))

    `(progn

      (defparameter ,name (make-instance 'buffer))
      (defvar ,queue-name (make-waitqueue))
      (defvar ,mutex-name
	(make-mutex :name ,(format nil "~A lock" (string-capitalize sname))))
      
      
      (let ((,writer-thread-name nil))
	(defun ,(intern (format nil "~A-WRITER-START" sname))
	    ()
	  (setf ,writer-thread-name sb-thread:*current-thread*))
	
	(defun ,(intern (format nil "~A-WRITER-ALIVE-P" sname))
	    ()
	  (and ,writer-thread-name (sb-thread:thread-alive-p ,writer-thread-name)))

      
	(defun ,(intern (format nil "~A-PUSH" sname))
	    (object)
	  (unless ,writer-thread-name
	    (,(intern (format nil "~A-WRITER-START" sname))))
	  (with-mutex (,mutex-name )
	    (setf (buffer ,name) (nconc (buffer ,name) (list object)))
	    (condition-broadcast ,queue-name)))
      
	(defun ,(intern (format nil "END-WRITING-IN-~A" sname)) ()
	  (setf ,writer-thread-name nil)
	  (condition-notify ,queue-name)))

      (defmacro ,(intern (format nil "~A-~A-~A" 
				 (string-upcase "as")
				 sname
				 (string-upcase "writer"))) (&body body)
	  `(progn
	    (,',(intern (format nil "~A-WRITER-START" sname)))
	    ,@body
	    (,',(intern (format nil "END-WRITING-IN-~A" sname)))))
				 
      
      (defmacro ,(intern (format nil "WITH-ELEMENTS-FROM-~A" sname))
	  (single-element &body body)
	`(let ((,single-element nil))
	  (iterate (while (or (,',(intern (format nil "~A-WRITER-ALIVE-P" sname)))
			      ,',name))
		   (with-mutex (,',mutex-name :wait-p t)
		     (iterate (while ,',name)
			      (setf ,single-element (pop (buffer ,',name)))
			      (release-mutex ,',mutex-name)
			      ,@body
			      (get-mutex ,',mutex-name sb-thread:*current-thread* t))
		     (when (,',(intern (format nil "~A-WRITER-ALIVE-P" sname)))
		       (condition-wait ,',queue-name
				       ,',mutex-name)))))))))

(defmacro make-file-buffer (name)

  (let* ((sname (symbol-name name))
	 (queue-name (intern (format nil "~A-QUEUE" sname)))
	 (mutex-name (intern (format nil "~A-LOCK" sname)))
	 (stream-name (intern (format nil "~A-STREAM" sname)))
	 (writer-thread-name (intern (format nil
					     "~A-WRITER-THREAD"
					     sname))))
    (close (open (format nil "~A" name) :direction :output :if-exists :supersede :if-does-not-exist :create))
    `(progn

      (defparameter ,name nil)
      (defparameter ,stream-name nil)
      (defvar ,queue-name (make-waitqueue))
      (defvar ,mutex-name
	(make-mutex :name
		    ,(format nil "~A lock" (string-capitalize sname))))
      
      
      (let ((,writer-thread-name nil))
	(defun ,(intern (format nil "~A-WRITER-START" sname))
	    ()
	  (setf ,writer-thread-name sb-thread:*current-thread*))
	
	(defun ,(intern (format nil "~A-WRITER-ALIVE-P" sname))
	    ()
	  (and ,writer-thread-name
	       (sb-thread:thread-alive-p ,writer-thread-name)))

      
	(defun ,(intern (format nil "~A-PUSH" sname))
	    (object)
	  (unless ,writer-thread-name
	    (,(intern (format nil "~A-WRITER-START" sname))))
	  (with-mutex (,mutex-name )
	    (format ,stream-name "~S~%" object)
	    (force-output ,stream-name)
	    (condition-broadcast ,queue-name)))
      
	(defun ,(intern (format nil "END-WRITING-IN-~A" sname)) ()
	  (setf ,writer-thread-name nil)
	  (condition-broadcast ,queue-name)))

      (defmacro ,(intern (format nil "~A-~A-~A" 
				 (string-upcase "as")
				 sname
				 (string-upcase "writer"))) (&body body)
	  `(progn
	    (,',(intern (format nil "~A-WRITER-START" sname)))
	    (with-open-file (,',stream-name ,',(format nil "~A" name)
					  :direction :output
					  :if-exists :append
					  :if-does-not-exist :create)
	      ,@body)
	    (,',(intern (format nil "END-WRITING-IN-~A" sname)))))
				 
      
      (defmacro ,(intern (format nil "WITH-ELEMENTS-FROM-~A" sname))
	  (single-element &body body)
	`(let ((,single-element nil)
	       (quit nil)
	       (alive-p nil))
	   (with-open-file (,',stream-name ,',(format nil "~A" name)
					   :direction :input
					   :if-does-not-exist :create)
	     (iterate (while (not quit))
		      (with-mutex (,',mutex-name :wait-p t)
			(setf alive-p (,',(intern (format nil "~A-WRITER-ALIVE-P" sname)))
			      ,single-element (read ,',stream-name nil 'EOF))
			(when (and alive-p (eq ,single-element 'EOF))
			  (condition-wait ,',queue-name
					  ,',mutex-name)))
		      (unless (or alive-p (not (eq ,single-element 'EOF)))
			(setf quit t))
		      (unless (eq ,single-element 'EOF)
			,@body))))))))


(defclass buffer ()
  ((buffer :initarg buffer :initform nil :accessor buffer)
   (buffer-name :initform (format nil "buffer-~a" (incf buffer-count))
		:reader buffer-name)
   (buffer-stream :initarg buffer-stream :initform nil 
		  :accessor buffer-stream)
   (buffer-queue :initarg buffer-queue :initform (make-waitqueue) 
		 :accessor buffer-queue)
   (buffer-lock :initarg buffer-lock 
		:initform (make-mutex :name "buffer lock") 
		:accessor buffer-lock)
  (buffer-writer-thread :initarg buffer-writer-thread :initform nil 
			:accessor buffer-writer-thread)))


(defun buffer-writer-start (buffer)
  (setf (buffer-writer-thread buffer) sb-thread:*current-thread*))

(defun buffer-writer-alive-p (buffer)
    (and (buffer-writer-thread buffer) 
	 (sb-thread:thread-alive-p (buffer-writer-thread buffer))))

(defun buffer-push (object buffer)
  (unless (buffer-writer-thread buffer) 
    (buffer-writer-start buffer))
  (with-mutex ((buffer-lock buffer)) 
    (format (buffer-stream buffer) "~s~%" object)
    (force-output (buffer-stream buffer)) 
    (condition-notify (buffer-queue buffer))))

(defun end-writing-in-buffer (buffer)
  (setf (buffer-writer-thread buffer) nil)
  (condition-notify (buffer-queue buffer)))




(defmacro as-buffer-writer (buffer &body body)
  (let ((s (gensym)))
    `(progn
       (buffer-writer-start ,buffer)
       (with-open-file
	   (,s (buffer-name ,buffer) :direction :output 
		 :if-exists :supersede)
	 (setf (buffer-stream ,buffer) ,s)
	 ,@body)
       (end-writing-in-buffer ,buffer))))

(defmacro with-elements-from-buffer (buffer single-element &body body)
  `(let ((,single-element nil))
     (with-open-file
	 ((buffer-stream ,buffer) (buffer-name ,buffer) :direction :input 
	  :if-does-not-exist :create)
       (iterate
	(while
	    (or (buffer-writer-alive-p ,buffer)
		(not (eq (peek-char nil (buffer-stream ,buffer) 
				    nil 'eof nil) 
			 'eof))))
	(with-mutex ((buffer-lock ,buffer) :wait-p t)
	  (iterate (while (setf ,single-element 
				(read (buffer-stream ,buffer) 
				      nil nil)))
		   (release-mutex (buffer-lock ,buffer)) 
		   ,@body
		   (get-mutex (buffer-lock ,buffer) nil t))
	  (when (buffer-writer-alive-p ,buffer)
	    (condition-wait (buffer-queue ,buffer) 
			    (buffer-lock ,buffer))))))))

(defun start-thread (fun &rest args)
  (sb-thread:make-thread #'(lambda ()
			     (apply fun args))
			 :name (symbol-name fun)))
