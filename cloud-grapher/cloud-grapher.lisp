;;; -*- Mode: Lisp; Package: CLOUD-GRAPHER -*-

;;;  (c) copyright 2005-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; The Grapher, a mcclim plotting facility.
;;;

(defpackage :cloud.grapher
  (:use :clim-lisp :iterate)
  (:nicknames :cloud-grapher)
  (:export plot
	   new-plot
	   new-figure
	   add-to-plot
	   set-xlabel
	   set-ylabel
	   set-title
	   grayscale
	   matlab
	   make-plottable))

(provide :cloud.grapher)


(in-package :cloud.grapher)


(defparameter *current-frame* nil)
(defparameter *figures* nil)
(defparameter *gold* (/ (+ 1 (sqrt 5)) 2))
(defparameter *default-color* clim:+blue+)
(defparameter *default-width* 800)

(clim:define-application-frame grapher ()
  ((plot-list :accessor plot-list
	      :initarg :plot-list
	      :initform nil)
   (y-min :initarg :y-min
	  :initform nil
	  :accessor y-min)
   (y-max :initarg :y-max
	  :initform nil
	  :accessor y-max)
   (x-max :initarg :x-max
	  :initform 0
	  :accessor x-max)
   (x-min :initarg :x-min
	  :initform nil
	  :accessor x-min)
   (y-min-view :initarg :y-min-view
	       :initform nil
	       :accessor y-min-view)
   (y-max-view :initarg :y-max-view
	       :initform nil
	       :accessor y-max-view)
   (x-max-view :initarg :x-max-view
	       :initform 0
	       :accessor x-max-view)
   (x-min-view :initarg :x-min-view
	       :initform nil
	       :accessor x-min-view)
   (plot-title :initarg :plot-title
	       :initform ""
	       :accessor plot-title)
   (xlabel :initarg :xlabel
	   :initform ""
	   :accessor xlabel)
   (ylabel :initarg :ylabel
	   :initform ""
	   :accessor ylabel)
   (zoomed :accessor zoomed
	   :initarg :zoomed
	   :initform nil)
   (modified :accessor modified
	     :initarg :modified
	     :initform nil)
   (id :accessor id
       :initarg :id
       :initform nil)
   (caller-output :initarg :caller-output
		  :accessor caller-output
		  :initform nil))
  (:panes (plotter-display :application
			   :min-height (/ *default-width* *gold*)
			   :min-width *default-width*
			   :display-function '(display-plots))
	  (interactor :interactor
		      :max-height 10))
  (:layouts (default-layout
		(clim:vertically ()
		  plotter-display
		  interactor)))
  (:menu-bar t))


(define-grapher-command (com-quit :name "Quit" :menu t) ()
  (setf *figures* (delete clim:*application-frame* *figures*))
  (clim:frame-exit clim:*application-frame*))

(clim:define-presentation-type table ())

(defparameter *x-margin* (/ *default-width* (expt *gold* 5)))
(defparameter *y-margin* (/ *default-width* (expt *gold* 5)))
(defparameter *tick-length* 5)
(defparameter *text-padding* 2)




(defun ticks-number (x)
	     (/ x (expt 10 (1- (round (log x 10))))))




(defun ticks-spacings (x)
	     (list (abs (- 75/10 (ticks-number x)))
		   (abs (- 75/10 (ticks-number (/ x 2))))
		   (abs (- 75/10 (ticks-number (/ x 5))))))



(defun ticks-spacing (x)
	     (let* ((ti (ticks-spacings x))
		    (min (apply #'min ti)))
	       (cond ((= min (first ti)) 
		      (* 1 (expt 10 (1- (round (log x 10))))))
		     ((= min (second ti)) 
		      (* 2 (expt 10 (1- (round (log (/ x 2) 10))))))
		     ((= min (third ti)) 
		      (* 5 (expt 10 (1- (round (log (/ x 5) 10)))))))))


(defun draw-title (frame pane)
  (clim:draw-text* pane
		   (plot-title frame)
		   (/ (clim:bounding-rectangle-width pane) 2)
		   (- (clim:bounding-rectangle-height pane) 
		      (/ *y-margin* (* 2 *gold*)))
		   :align-x :center
		   :align-y :center
		   :text-size :small))

(defun draw-xlabel (frame pane)
  (clim:draw-text* pane
		   (xlabel frame)
		   (/ (clim:bounding-rectangle-width pane) 2)
		   (/ *y-margin* (* 2 *gold*))
		   :align-x :center
		   :align-y :center
		   :text-size :small))

(defun draw-ylabel (frame pane)
  (clim:with-translation 
      (pane
       (/ *x-margin* (* 2 *gold*))
       (/ (clim:bounding-rectangle-height pane) 2))
    
    (clim:with-rotation (pane 90)
      (clim:draw-text* pane
		       (ylabel frame)
		       0 0
		       :transform-glyphs t
		       :toward-x -200
		       :align-x :center
		       :align-y :center
		       :transformation (clim:make-rotation-transformation 90)
		       :text-size :small))))
		   

(defun draw-v-tick (frame pane x y val)
  (declare (ignore frame))
  (clim:draw-line* pane
		   x y
		   (- x *tick-length*) y)
  (clim:draw-text* pane
		   (format nil "~f" val)
		   (- x *tick-length* *text-padding*) y
                   :align-x :right
	           :align-y :center
		   :text-size :very-small))

(defun draw-h-tick (frame pane x y val)
  (declare (ignore frame))
  (clim:draw-line* pane
		   x y
		   x (- y *tick-length*))
  (clim:draw-text* pane
		   (format nil "~f" val)
		   x (- y *tick-length* *text-padding*)
                   :align-x :center
	           :align-y :top
		   :text-size :very-small))
  
(defun draw-axis (frame pane)
  (let* ((width (clim:bounding-rectangle-width pane))
	 (height (clim:bounding-rectangle-height pane))
	 (fig-height (- height *y-margin* *y-margin*))
	 (fig-width (- width *x-margin* *x-margin*))
	 (vts (ticks-spacing (- (y-max-view frame) (y-min-view frame))))
	 (hts (ticks-spacing (- (x-max-view frame) (x-min-view frame) 1))))
    (clim:draw-arrow* pane
		      *x-margin* *y-margin*
		      *x-margin* (- height (/ *y-margin* *gold*)))
    (clim:draw-arrow* pane
		      *x-margin* *y-margin*
		      (- width (/ *x-margin* *gold*)) *y-margin*)
    
    (iter (for i from (* vts (ceiling (y-min-view frame) vts)) to (y-max-view frame) by vts)
	  (draw-v-tick frame pane *x-margin* 
		       (+ *y-margin* 
			  (* fig-height 
			     (/ (- i (y-min-view frame)) 
				(-  (y-max-view frame) 
				    (y-min-view frame)))))
		       i))
    
    (iter (for i from (* hts (ceiling (x-min-view frame) hts)) to (1- (x-max-view frame)) by hts)
	  (draw-h-tick frame pane 
		       (+ *x-margin* (* fig-width 
					(/ (- i 
					      (x-min-view frame)) 
					   (-  (x-max-view frame) 
					       (x-min-view frame)
					       1))))
		       *y-margin*
		       i))))

(defun gray (x)
  (clim:make-rgb-color x x x))

(defun zerone (x)
  (if (> x 0)
      (if (< x 1)
	  x
	  1)
      0))

(defun matlab (x)
  (clim:make-rgb-color
   (zerone (+ (- (* 4 (abs (- x 0.75))))
	      1.5))
   (zerone (+ (- (* 4 (abs (- x 0.5))))
	      1.5))
   (zerone (+ (- (* 4 (abs (- x 0.25))))
	      1.5))))

(defun strange (x)
  (clim:make-ihs-color .5 x 1))

(defun rgb2ub32 (color)
  (multiple-value-bind (red green blue)
      (clim:color-rgb color)
    (dpb (round (* red 255)) (byte 8 0)
	 (dpb (round (* green 255)) (byte 8 8)
	      (dpb (round (* blue 255)) (byte 8 16)
		   (dpb (- 255 0) (byte 8 24) 0))))))

(defun convert-image-to-ub32 (image)
  (let ((res (make-array (array-dimensions (slot-value image 'climi::data)) 
			 :element-type '(unsigned-byte 32))))
    (iter (for (the fixnum i) below (* (slot-value image 'climi::width)
			  (slot-value image 'climi::height)))
	  (setf (row-major-aref res i)
		(rgb2ub32 (row-major-aref (slot-value image 'climi::data) i))))
    (make-instance 'climi::rgb-image
		   :data res
		   :width (array-dimension res 1)
		   :height (array-dimension res 0))))

(defun make-image-from-array (array &key (colormap #'gray))
  (let* ((length (apply #'* (array-dimensions array)))
	 (image (make-array (array-dimensions array))))
    (iter (for (the fixnum i) below length)
	    (setf (row-major-aref image i) 
		  (funcall colormap (row-major-aref array i))))

    
    (make-instance 'climi::rgb-image
		   :data image
		   :width (array-dimension array 0)
		   :height (array-dimension array 1))))


(defun nearest-neighbour (array i j)
  (aref array (floor i) (floor j)))

(defun linear (array i j)
  (multiple-value-bind (x rest-x) 
      (floor i)
    (multiple-value-bind (y rest-y)
	(floor j)
      (let* ((bx x)
	     (ax (if (zerop rest-x)
		     (1+ x)
		     (1+ x)))
	     (by y)
	     (ay (if (zerop rest-y)
		     (1+ y)
		     (1+ y))))
	(multiple-value-bind (tlr tlg tlb)
	    (clim:color-ihs (aref array bx by))
	  (multiple-value-bind (trr trg trb)
	      (clim:color-ihs (aref array ax by))
	    (multiple-value-bind (blr blg blb)
		(clim:color-ihs (aref array bx ay))
	      (multiple-value-bind (brr brg brb)
		  (clim:color-ihs (aref array ax ay))
		(clim:make-ihs-color 
		 (+ (* rest-x
		       (+ (* rest-y 
			     tlr)
			  (* (- 1 rest-y)
			     blr)))
		    (* (- 1 rest-x)
		       (+ (* rest-y 
			     trr)
			  (* (- 1 rest-y)
			     brr))))
		 (+ (* rest-x
		       (+ (* rest-y 
			     tlg)
			  (* (- 1 rest-y)
			     blg)))
		    (* (- 1 rest-x)
		       (+ (* rest-y 
			     trg)
			  (* (- 1 rest-y)
			     brg))))
		 (+ (* rest-x
		       (+ (* rest-y 
			     tlb)
			  (* (- 1 rest-y)
			     blb)))
		    (* (- 1 rest-x)
		       (+ (* rest-y 
			     trb)
			  (* (- 1 rest-y)
			     brb)))))))))))))
	     

	     
	


(defun resize-image (image new-width new-height &key (interpolation #'nearest-neighbour))
  (let ((x-scale (/ (slot-value image 'climi::width)
		    new-width))
	(y-scale (/ (slot-value image 'climi::height)
		    new-height))
	(res (make-array (list new-width new-height))))
    (iter (for (the fixnum i) below new-width)
	  (iter (for  (the fixnum j) below new-height)
		(setf (aref res i j)
		      (funcall interpolation 
			       (slot-value image 'climi::data) 
			       (* i x-scale)
			       (* j y-scale)))))
    (make-instance 'climi::rgb-image
		   :data res
		   :height new-width
		   :width new-height)))

	  

(defun draw-image (array frame pane &key (colormap #'gray))
  (clim:draw-design pane 
		    (climi::make-rgb-image-design 
		     (convert-image-to-ub32 (resize-image 
					     (make-image-from-array array :colormap colormap)
					     (round (- (clim:bounding-rectangle-height pane) 
						       (* 2 *y-margin*)))
					     (round (- (clim:bounding-rectangle-width pane)
						       (* 2 *x-margin*))))))
		    :transformation (clim:make-translation-transformation 
				     0 (array-dimension array 0))))
    
(defun draw-signal (values frame pane &key (beginning 0) (label nil))
  (let* ((width (clim:bounding-rectangle-width pane))
	 (x-zoom-factor (/ width
			   (- (x-max-view frame) (x-min-view frame) 1)))
	 (x-shrink-factor (/ x-zoom-factor)))
    (if (<= 1 x-zoom-factor)
	(progn
	  (when label
	    (clim:draw-text* pane label
			     (* (- beginning (x-min-view frame)) x-zoom-factor)
			     (aref values 0)
	                     :align-x :right
		             :align-y :center
			     :text-size :tiny 
			     :ink clim:+black+))
	  (iter (for i from (floor (max beginning (x-min-view frame)))
		       below (min (+ (length values) beginning -1) (1- (x-max-view frame))))
		(clim:draw-line* pane 
				 (* (- i (x-min-view frame)) x-zoom-factor) 
				 (aref values (- i beginning))
				 (* (- (1+ i) (x-min-view frame)) x-zoom-factor) 
				 (aref values (1+ (- i
						     beginning))))))
	(iter (for i from (* beginning x-shrink-factor)
		   below (min width (* (length values)
					 x-shrink-factor)))
	      (iter (for j 
			 from (max (round (* i x-shrink-factor))
				   beginning)
			 below (min (1+ (round (* (1+ i) x-shrink-factor)))
				    (length values)))
		    (maximize (aref values (- j beginning)) into max)
		    (minimize (aref values (- j beginning)) into min)
		    (finally (clim:draw-line* pane
					      i max
					      i min)))))))



(clim:define-presentation-type partial-presentation ())



(defun recompute-vals (frame obj &optional (beginning 0))
  (typecase obj  
    (t 
     

  (let ((values
	 (typecase obj
	   (array obj)
	   (t nil))))
	
    (when values
      (if (= (array-rank values) 1)
	  (progn
	    (unless (and (y-max frame)
			 (y-min frame))
	      (setf (y-max frame) (aref values 0)
		    (y-min frame) (aref values 0)))
	    (setf (x-max frame)
		  (max (x-max frame) 
		       (+ beginning (length values))))
	    (setf (x-min frame)
		  (if (x-min frame)
		      (min (x-min frame)
			   beginning)
		      beginning))
	    (iterate (for i in-vector values)
		     (maximize i into max)
		     (minimize i into min)
		     (finally (setf (y-max frame) 
				    (max max (y-max frame)))
			      (setf (y-min frame)
				    (min min (y-min frame)))))
	    (when (= (y-min frame) (y-max frame))
	      (decf (y-min frame) 1/2)
	      (incf (y-max frame) 1/2))
	    (unless (zoomed frame)
	      (setf (y-min-view frame) (y-min frame)
		    (y-max-view frame) (y-max frame)
		    (x-min-view frame) (x-min frame)
		    (x-max-view frame) (x-max frame))))
	  (progn
	    (setf (x-min frame) 0
		  (x-max frame) (1+ (array-dimension values 1))
		  (y-min frame) 0
		  (y-max frame) (array-dimension values 0))
	    (unless (zoomed frame)
	      (setf (y-min-view frame) (y-min frame)
		    (y-max-view frame) (y-max frame)
		    (x-min-view frame) (x-min frame)
		    (x-max-view frame) (x-max frame))))))))))

(defmethod display-plots ((frame grapher) pane)
  (clim:clear-output-record (clim:stream-output-history pane))
  (when (plot-list frame)
    (when (modified frame)
      (setf (modified frame) nil)
      (iter (for plot in (plot-list frame))
	    (if (listp plot)
		(recompute-vals frame 
				(first plot) 
				(getf (rest plot) :beginning 0))
		(recompute-vals frame plot))))
    (let* ((width (clim:bounding-rectangle-width 
		   pane))
	   (height (clim:bounding-rectangle-height
		    pane))
	   (flip (clim:make-reflection-transformation* 		      
		  0 (/ height 2)
		  width (/ height 2)))
	   (resizing (clim:make-scaling-transformation*
		      (/ (- width (* *x-margin* 2)) 
			 width)
		      (/ (- height (* *y-margin* 2)) 
			 (- (y-max-view frame)
			    (y-min-view frame)))
		      0 0))
	   (moving-to-origin (clim:make-translation-transformation
			      0
			      (- (y-min-view frame))))
	   (centering (clim:make-translation-transformation
		       *x-margin*
		       *y-margin*)))
      (clim:with-drawing-options (pane
				  :transformation flip)
	(draw-axis frame pane)
	(draw-title frame pane)
	(draw-xlabel frame pane)
	(draw-ylabel frame pane)
	(clim:with-drawing-options (pane
				    :transformation
				    (clim:compose-transformations
				     centering
				     (clim:compose-transformations
				      resizing
				      moving-to-origin))
				    :clipping-region (clim:make-rectangle*
					0 (y-min-view frame)
					width
					(y-max-view frame)))
	  (dolist (current-plot (plot-list frame))
	    (if (listp current-plot)
		(let ((plot (first current-plot))
		      (label (getf (rest current-plot) :label ""))
		      (color (getf (rest current-plot) :color clim:+blue+))
		      (colormap (getf (rest current-plot) :colormap #'gray))
		      (beg (getf (rest current-plot) :beginning 0)))
		  (clim:with-drawing-options (pane
					      :ink color)
		    
		    (typecase plot
		      (array (if (= (array-rank plot) 2)
				 (draw-image plot frame pane :colormap colormap)
				 (draw-signal plot frame pane :beginning beg :label label)))
		      (t nil))))
		(clim:with-drawing-options (pane
					    :ink *default-color*)
		  (typecase current-plot
		    (array (if (= (array-rank current-plot) 2)
				 (draw-image current-plot frame pane)
				 (draw-signal current-plot frame pane
		      :beginning 0))))))))
	(draw-axis frame pane)))
    (terpri pane)
    (terpri pane)))
  
(defun update-plotter-display ()
  (let* ((fig clim:*application-frame*)
	 (pane (clim:find-pane-named fig 'plotter-display)))
    (clim:repaint-sheet pane clim:+everywhere+)))

(define-grapher-command (com-clear :name "Clear" :menu t) ()
  (setf (plot-list clim:*application-frame*) nil
	(x-max clim:*application-frame*) 0
	(x-min clim:*application-frame*) nil
	(y-max clim:*application-frame*) nil
	(y-min clim:*application-frame*) nil)
  (update-plotter-display))

(define-grapher-command (com-plot :name "Plot")
    ((thing 't))
  (setf (plot-list clim:*application-frame*) nil
	(x-max clim:*application-frame*) 0
	(x-min clim:*application-frame*) nil
	(y-max clim:*application-frame*) nil
	(y-min clim:*application-frame*) nil)
  (setf (plot-list clim:*application-frame*) (list thing))
  (update-plotter-display))

(define-grapher-command (com-get-info :name "Get Info")
    ((thing 't))
  (format (caller-output clim:*application-frame*)
	  "~a~%" thing))

(clim:define-gesture-name :info :pointer-button (:left :control))

(define-grapher-command (com-add-plot :name "Add Plot")
    ((thing 't))
  (push thing (plot-list clim:*application-frame*))
  (setf (modified clim:*application-frame*) t))

(define-grapher-command (com-replot :name "Replot" :menu t) ()
  (update-plotter-display))

(define-grapher-command (com-zoom :name "Zoom") 
    ((x-min 't) (y-min 't) (x-max 't) (y-max 't))
  (setf (y-min-view clim:*application-frame*) (min y-min y-max)
	(y-max-view clim:*application-frame*) (max y-min y-max)
	(x-min-view clim:*application-frame*) (min x-min (1+ x-max))
	(x-max-view clim:*application-frame*) (max x-min (1+ x-max))
	(zoomed clim:*application-frame*) t)
  (update-plotter-display))

(define-grapher-command (com-zoom-select :name "Zoom Area") ((x 'integer) (y 'integer))
  (let* ((frame *application-frame*)
	 (pane (get-frame-pane frame 'plotter-display))
	 (width (clim:bounding-rectangle-width 
		 pane))
	 (height (clim:bounding-rectangle-height
		  pane))
	 (flip (clim:make-reflection-transformation* 		      
		0 (/ height 2)
		width (/ height 2)))
	 (resizing (clim:make-scaling-transformation*
		    (/ (- width (* *x-margin* 2)) 
		       (- (x-max-view frame)
			  (x-min-view
			   frame) 1))
		    (/ (- height (* *y-margin* 2)) 
		       (- (y-max-view frame)
			  (y-min-view frame)))
		    0 0))
	 (moving-to-origin (clim:make-translation-transformation
			    (- (x-min-view frame))
			    (- (y-min-view frame))))
	 (centering (clim:make-translation-transformation
		     *x-margin*
		     *y-margin*))
	 (trans (clim:compose-transformations
		 flip
		 (clim:compose-transformations
		  centering
		  (clim:compose-transformations
		   resizing
		   moving-to-origin)))))
    (multiple-value-bind (ll tt rr bb)
                  (pointer-input-rectangle* :stream pane
                                            :left x 
					    :top y)
      (multiple-value-bind (a b c d)
	  (untransform-rectangle* trans ll tt rr bb)
	(com-zoom a b c d)))))

(clim:define-presentation-to-command-translator zoom-on
    (blank-area com-zoom-select grapher :gesture :describe)
    (x y)
  `(,x ,y))

(define-grapher-command (com-reset-zoom :name "Reset Zoom" :menu t) 
    ()
  (setf (y-min-view clim:*application-frame*) (y-min clim:*application-frame*)
	(y-max-view clim:*application-frame*) (y-max clim:*application-frame*)
	(x-min-view clim:*application-frame*) (x-min clim:*application-frame*)
	(x-max-view clim:*application-frame*) (x-max clim:*application-frame*)
	(zoomed clim:*application-frame*) nil)
  (update-plotter-display))

(define-grapher-command (com-xlabel :name "Set X Label" :menu nil)
    ((label 'string))
  (setf (xlabel clim:*application-frame*) label)
  (update-plotter-display))

(define-grapher-command (com-ylabel :name "Set Y Label" :menu nil)
    ((label 'string))
  (setf (ylabel clim:*application-frame*) label)
  (update-plotter-display))

(define-grapher-command (com-title :name "Set Title" :menu nil)
    ((label 'string))
  (setf (plot-title clim:*application-frame*) label)
  (update-plotter-display))

(define-grapher-command (com-export :name "Export" :menu t)
    ((filename 'string))
  (with-open-file (fs filename 
		   :direction :output 
		   :if-exists :supersede)
    (clim:with-output-to-postscript-stream (s fs :device-type :eps)
      (display-plots clim:*application-frame* s))))
		   
(defun initialize-figure (&optional title)
  (if *figures*
      (first *figures*)
      (new-figure title)))

(defun new-figure (&optional title)
  (let* ((new-id (if *figures*
		     (let ((i 1))
		       (iter (while (find i *figures* :key #'id))
			     (incf i)
			     (finally (return i))))
		     1))
	 (new-figure 
	  (clim:make-application-frame 'grapher
	   :caller-output *standard-output*
	   :id new-id
	   :pretty-name (or title (format nil "Grapher ~a" new-id))
	   :plot-list nil)))
    (push new-figure *figures*)
	 (sb-thread:make-thread #'(lambda ()
				    (unwind-protect
					 (clim:run-frame-top-level new-figure)
				      (setf *figures* (delete new-figure *figures*))))
	  :name (or title (format nil "Grapher ~a" new-id)))
    new-figure))

(defun new-plot (&rest things)
  (new-figure)
  (apply #'plot things))

(defun add-attributes (thing attributes)
  (let ((rest (rest thing)))
    (iter (for attribute in (list :color :colormap :label :beginning))
	  (when (getf attributes attribute)
	    (setf (getf rest attribute)
		  (getf attributes attribute))))
    (nconc (list (first thing)) rest)))
	  

(defun apply-attributes (things attributes)
  (iter (for thing in things)
	(if (atom thing)
	    (collect (add-attributes (list thing) attributes))
	    (collect (add-attributes thing attributes)))))

(defun get-things (things)
  (iter (for thing in things)
	(if (atom thing)
	    (if (arrayp thing)
		(nconcing (list thing))
		(nconcing (get-things (make-plottable thing))))
	    (if (> (length thing) 1)
		(if (keywordp (second thing))
		    (nconcing (apply-attributes (get-things (list (first thing)))
						(rest thing)))
		    (nconcing (get-things thing)))
		(nconcing (get-things thing))))))

(defgeneric make-plottable (thing)
  (:documentation "Describes how thing has to be plotted. Available
  for external classes. For example if thing is of class bidule, and
  has a slot called data that is to be used for plotting, the
  corresponding method should return something like (list (data bidule
  instance) :label \"bidule-instance\")."))

(defmethod make-plottable ((thing array))
  (list thing))

;;; Example for extension

;; (defclass truc ()
;;   ((data :accessor data)
;;    (start :accessor start)))

;; (defmethod make-plottable ((thing truc))
;;   (list (data thing) :beginning (start thing)))

;; (defclass troc ()
;;   ((trucs :accessor trucs)))

;; (defmethod make-plottable ((thing troc))
;;   (iter (for i in (trucs thing))
;; 	(for j from 1)
;; 	(collect (list i :label (format nil "~a" j)))))

(defun plot (&rest things)
  (let ((fig (initialize-figure)))
    (sleep 0.1) ; for racy calls
    (clim:execute-frame-command fig (list 'com-clear))
    (iter (for thing in (get-things things))
	  (clim:execute-frame-command 
	   fig
	   (list 'com-add-plot
		 thing)))
    (clim:execute-frame-command fig (list 'com-replot))
    
    (sleep 0.01) ; ugly hack
    (clim:redisplay-frame-panes fig)
    (force-output (clim:find-pane-named fig 'plotter-display))
    (clim:repaint-sheet (clim:find-pane-named fig 'plotter-display)
			clim:+everywhere+)))

	

(defun set-xlabel (label &optional (fig nil))
  (unless fig
    (setf fig (first *figures*)))
  (clim:execute-frame-command fig (list 'com-xlabel label))
  (sleep 0.01) ; ugly hack
  (clim:redisplay-frame-panes fig)
  (force-output (clim:find-pane-named fig 'plotter-display))
  (clim:repaint-sheet (clim:find-pane-named fig 'plotter-display)
		      clim:+everywhere+))

(defun set-ylabel (label &optional (fig nil))
  (unless fig
    (setf fig (first *figures*)))
  (clim:execute-frame-command fig (list 'com-ylabel label))
  (sleep 0.01) ; ugly hack
  (clim:redisplay-frame-panes fig)
  (force-output (clim:find-pane-named fig 'plotter-display))
  (clim:repaint-sheet (clim:find-pane-named fig 'plotter-display)
		      clim:+everywhere+))

(defun set-title (label &optional (fig nil))
  (unless fig
    (setf fig (first *figures*)))
  (clim:execute-frame-command fig (list 'com-title label))
  (sleep 0.01) ; ugly hack
  (clim:redisplay-frame-panes fig)
  (force-output (clim:find-pane-named fig 'plotter-display))
  (clim:repaint-sheet (clim:find-pane-named fig 'plotter-display)
		      clim:+everywhere+))

(defun add-to-plot (thing &rest other-things)
  (clim:execute-frame-command (first *figures*) (list 'com-add-plot thing))
  (iter (for other-thing in other-things)
	(clim:execute-frame-command (first *figures*)
				    (list 'com-add-plot
  other-thing))))

(defun zoom (x-min y-min x-max y-max)
  (clim:execute-frame-command (first *figures*) (list 'com-zoom x-min y-min x-max y-max)))

(defun export-image (image file)
  (with-open-file (s file :direction :output :if-exists :supersede)
    (iter (for i below (array-dimension image 0))
	  (iter (for j below (array-dimension image 1))
		(format s "~a ~a ~a~%" i j (aref image i j))))))

(defun make-testa ()
  (setf testa (make-array '(300 300)))
  
  (iter (for i below 300)
	(iter (for j below 300)
	      (setf (aref testa i j) (/ (random 255) 255))))
  
  (iter (for i below 300)
	(setf (aref testa 150 i) 1
	      (aref testa i 150) 1)))