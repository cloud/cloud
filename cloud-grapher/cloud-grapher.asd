;;; -*- Mode: Lisp; Package: COMMON-LISP-USER -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(use-package :asdf)

(defsystem :cloud-grapher
    :depends-on (:mcclim :iterate)	
    :components ((:file "cloud-grapher")))
    
(defun load-grapher  ()
  (asdf:operate 'asdf:load-op 'grapher))
