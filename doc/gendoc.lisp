(require :cloud-clicks)
(require :cloud-bliss)
(require :cldoc)

(cldoc:extract-documentation 'cldoc:html "."
			     (list
			      (asdf:find-system :cloud-maths)
			      (asdf:find-system :cloud-sp)
			      (asdf:find-system :read-bytes)
			      (asdf:find-system :cloud-sound)
			      (asdf:find-system :cloud-clicks)
			      (asdf:find-system :cloud-bliss)
			      (asdf:find-system :cloud-grapher))
			     :table-of-contents-title "Cloud"
			     :filter #'cldoc::default-filter)

(quit)

