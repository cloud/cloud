;;; -*- Mode: Lisp; Package: CLOUD.BLISS -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.bliss)

(deftype side ()
  '(member :left :right))

(defconstant +cipic-azimuths+ #(-80 -65 -55 -45 -40 -35 -30 -25 -20 -15 -10 -5 0 5 10 15 20 25 30 35 40 45 55 65 80))
(defconstant +cipic-elevations+ (v-make -45 (+ 225 360/64) :by 360/64))
(defconstant +cipic-subjects+ #("003" "008" "009" "010" "011" "012" "015" "017" "018" "019" "020" "021" "027" "028" "033" "040" "044" "048" "050" "051" "058" "059" "060" "061" "065" "119" "124" "126" "127" "131" "133" "134" "135" "137" "147" "148" "152" "153" "154" "155" "156" "158" "162" "163" "165"))
(defconstant +cipic-head-diameters+ '(0.0809 nil nil 0.0644 nil nil nil nil 0.0664 nil 0.0810 0.0682 0.0716 0.0804 0.0726 0.0632 0.0751 0.0699 0.0747 0.0680 0.0717 0.0721 0.0707 0.0722 0.0726 0.0726 0.0745 0.0635 0.0727 0.0726 0.0714 0.0708 0.0670 0.0744 0.0726 0.0695 0.0770 0.0688 0.0802 0.0726 0.0678 nil 0.0770 0.0784 0.0701))
(defconstant +default-head-diameter+ 0.0721)
(defconstant +c+ 344) ;speed of sound in the air
(defconstant +cipic-sampling-rate+ 44100)

(defun closest-cipic-azimuth-position (angle)
  (iter (for i index-of-vector +cipic-azimuths+)
	(finding i minimizing (abs (- (aref +cipic-azimuths+ i) angle)))))

(defun closest-cipic-azimuth (angle)
  (aref +cipic-azimuths+ (closest-cipic-azimuth-position angle)))

(defun closest-cipic-elevation-position (angle)
  (iter (for i index-of-vector +cipic-elevations+)
	(finding i minimizing (abs (- (aref +cipic-elevations+ i) angle)))))

(defun closest-cipic-elevation (angle)
  (aref +cipic-elevations+ (closest-cipic-elevation-position angle)))

(let ((subject-memo nil)
      (hrir-l nil)
      (hrir-r nil))
  (defun cipic-hrir (azimuth elevation side &key (subject "021"))
    (if (eql subject-memo subject)
	(if (eql :right side)
	    (subarray hrir-r 
		      (closest-cipic-azimuth-position azimuth)
		      (closest-cipic-elevation-position elevation)
		      nil)
	    (subarray hrir-l 
		      (closest-cipic-azimuth-position azimuth)
		      (closest-cipic-elevation-position elevation)
		      nil))
	(progn
	  (with-open-file (s (concatenate 'string 
					  "cipic/" subject 
					  "_hrir_l")) 
	    (setf hrir-l (read s)))
	  (with-open-file (s (concatenate 'string 
					  "cipic/" subject 
					  "_hrir_r")) 
	    (setf hrir-r (read s)))
	  (setf subject-memo subject)
	  (cipic-hrir azimuth elevation side :subject subject)))))
  
(defun mix (&rest sounds)
  (apply #'m+ sounds))

(defun spatialise (sound 
		   &key (azimuth 0) (elevation 0) 
		        (subject "021") (level 0))
  (make-array (list 2 (+ (length sound) 199))
   :displaced-to (m.* (expt 10 (/ level 20)) 
		      (concatenate 'vector 
				   (v-fun #'realpart 
					  (convolution
					   (cipic-hrir azimuth elevation :right :subject subject) 
					   sound))
				   (v-fun #'realpart 
					  (convolution
					   (cipic-hrir azimuth elevation :left :subject subject) 
					   sound))))))



(defmacro with-persistent (vars &body body)
  `(let (,vars)
     ,@body))

(defmacro with-persistent-vars (vars &body body)
  `(let ,vars
     ,@body))

(defmacro defun-memo-simple (name lambda-list &body body)
  (let* ((tmp (loop for i below (length lambda-list)
		    for j in lambda-list
		    unless (or (eq '&optional j)
			       (eq '&key j)
			       (eq '&rest j))
		    collect (if (atom j)
				j
				(first j))))
	 (syms (loop for i below (length tmp)
		    for j in tmp
		    collect (gensym))))
    `(let ,(append (mapcar #'(lambda (x)
			       `(,x nil))
			   syms)
		   `((res nil)))
       (defun ,name ,lambda-list
	 (unless (and ,@(mapcar #'(lambda (x y)
				   `(equal ,x ,y))
			       syms tmp))
	   (setf ,@(concatenate 'cons (mapcan #'(lambda (x y)
						 `(,x ,y))
					     syms tmp)
			       `(res ,@body))))
	 res))))


(defmacro defun-memo (name lambda-list &body body)
  (let* ((tmp (loop for i below (length lambda-list)
		    for j in lambda-list
		    with restp = nil
		    if (or (eq '&optional j)
			   (eq '&key j)
			   (eq '&allow-other-keys j)
			   (eq '&rest j))
		    do (when (eq '&rest j)
		      (setf restp t))
		    else if restp 
		         do (setf restp nil)
			 else collect (if (atom j)
					  j
					  (first j))))
	 (syms (loop for i below (length tmp)
		    for j in tmp
		    collect (gensym)))
	 (filename (gensym)))
    `(let ,(append (mapcar #'(lambda (x)
			       `(,x nil))
			   syms)
		   `((res nil)))
       (defun ,name ,lambda-list
	 (let ((,filename (format nil "memo/~a-~{~a~^-~}" 
				  ,(format nil "~a" name) 
				  (list ,@tmp))))
	   (unless (and ,@(mapcar #'(lambda (x y)
				      `(equal ,x ,y))
				  syms tmp))
	     (if (open ,filename :direction :probe)
		 (with-open-file (s ,filename :direction :input)
		   (setf ,@(concatenate 'cons (mapcan #'(lambda (x y)
							  `(,x ,y))
						      syms tmp)
					`(res (read s)))))
		 (progn
		   (ensure-directories-exist ,filename)
		   (with-open-file (s ,filename :direction :output)
		     (format s "~s"
			     (setf ,@(concatenate 'cons (mapcan #'(lambda (x y)
								    `(,x ,y))
								syms tmp)
						  `(res (progn ,@body)))))))))
	   res)))))

(defun half (a)
  (make-array (/ (length a) 2) 
   :displaced-to a 
   :element-type (array-element-type a)))

(defun reshape-array (a dimensions)
  (make-array dimensions :displaced-to a :element-type
	      (array-element-type a)))


(defun smooth! (array)
  (let ((prev (aref array 0)))
    (iter (for i from 1 below (1- (length array)))
	  (setf prev (/ (+ prev (* 2 (aref array i)) (aref array (1+ i))) 4))
	  (rotatef prev (aref array i))))
  array)
	  

;;; this is broken, rest must not be included in the filename as
;;; is... see defun-memo macro
;;; should be corrected now
;;; however, it seems that including a function name in the filename
;;; is not a good idea (still to check)
(defun-memo lookup-table (subject fun 
				  &rest rest
				  &key 
				  (windowsize 512) 
				  (smoothing #'identity)
				  &allow-other-keys)
  (remf rest :windowsize)
  (remf rest :smoothing)
  (reshape-array 
   (iter (for az in-vector +cipic-azimuths+)
	 (accumulate
	  (funcall smoothing 
		   (apply fun
			  (append
			   (list (half
				  (sfft
				   (cipic-hrir az 0 :right
						    :subject subject)
				   windowsize))
				 (half
				  (sfft 
				   (cipic-hrir az 0 :left
						    :subject subject)
				   windowsize)))
			   rest)))
	  by #'(lambda (x y)
		 (concatenate 'vector y x))))
   (list (length +cipic-azimuths+)
	 (/ windowsize 2))))

(defun-memo itd-lookup-table (subject &key 
				      (windowsize 512) 
				      (smoothing #'identity))
  (reshape-array 
   (iter (for az in-vector +cipic-azimuths+)
	 (accumulate
	  (funcall smoothing 
		   (itd
		    (half
		     (sfft
		      (cipic-hrir az 0 :right
				  :subject subject)
		      windowsize))
		    (half
		     (sfft 
		      (cipic-hrir az 0 :left
				  :subject subject)
		      windowsize))
		    :sampling-rate +cipic-sampling-rate+
		    :unwrap t))
	  by #'(lambda (x y)
		 (concatenate 'vector y x))))
   (list (length +cipic-azimuths+)
	 (/ windowsize 2))))

(defun-memo alpha (length &optional subject)
  (if subject
      (let* ((lookup (itd-lookup-table subject 
		      :windowsize (* 2 length)
		      :smoothing #'smooth!))
	     (mid-az (/ (1- (length +cipic-azimuths+)) 2))
	     (subject-nb (position subject 
				   +cipic-subjects+ 
			           :test #'string=))
	     (head-radius (if (elt +cipic-head-diameters+ subject-nb)
			      (elt +cipic-head-diameters+ subject-nb)
			      +default-head-diameter+))
	     (fun1 (v-fun #'(lambda (x)
			      (/ (* head-radius (+ (sin x) x))
				 +c+))
			  (v* (subarray +cipic-azimuths+ 
					(list 0 (1- mid-az)))
			      (/ pi 180))))
	     (fun2 (v-fun #'(lambda (x)
			      (/ (* head-radius (+ (sin x) x))
				 +c+))
			  (v* (subarray +cipic-azimuths+ 
					(list (1+ mid-az) 
					      (1- (length +cipic-azimuths+))))
			      (/ pi 180)))))
	(concatenate 'vector
		     #(1.0d0)
		     (iter (for i from 1 below (array-dimension lookup 1))
			   (collect (/ (+ (mean (v/ 
						 (squeeze 
						  (subarray lookup 
							    (list 0 (1- mid-az)) 
							    i))
						 fun1))
					  (mean (v/ 
						 (squeeze 
						  (subarray lookup
							    (list (1+ mid-az)
								  (1- (length +cipic-azimuths+)))
							    i))
						 fun2)))
				       2)
			     result-type 'vector))))
      (v/ (iter (for i in-vector +cipic-subjects+)
	       (reducing (alpha length i) by #'v+))
	 (length +cipic-subjects+))))

(defun-memo ild-lookup-table (subject &key 
				      (windowsize 512) 
				      (smoothing #'identity))
  (reshape-array 
   (iter (for az in-vector +cipic-azimuths+)
	 (accumulate
	  (funcall smoothing 
		   (ild 
		    (half
		     (sfft
		      (cipic-hrir az 0 :right
				  :subject subject)
		      windowsize))
		    (half
		     (sfft 
		      (cipic-hrir az 0 :left
				  :subject subject)
		      windowsize))))
	  by #'(lambda (x y)
		 (concatenate 'vector y x))))
   (list (length +cipic-azimuths+)
	 (/ windowsize 2))))

(defun-memo beta (length &optional subject)
  (if subject
      (let* ((lookup (ild-lookup-table subject 
		      :windowsize (* 2 length)
		      :smoothing #'smooth!))
	     (mid-az (/ (1- (length +cipic-azimuths+)) 2))
	     (fun1 (v-fun #'sin (v* (subarray +cipic-azimuths+ 
					      (list 0 
						    (1- mid-az)))
				    (/ pi 180))))
	     (fun2 (v-fun #'sin (v* (subarray +cipic-azimuths+ 
					      (list (1+ mid-az) 
						    (1- (length +cipic-azimuths+))))
				    (/ pi 180)))))
	(iter (for i below (array-dimension lookup 1))
	      (collect (/ (+ (mean (v/ 
				    (squeeze 
				     (subarray lookup 
					       (list 0 (1- mid-az)) 
					       i))
				    fun1))
			     (mean (v/ 
				    (squeeze 
				     (subarray lookup
					       (list (1+ mid-az)
						     (1- (length +cipic-azimuths+)))
					       i))
				    fun2)))
			  2)
		result-type 'vector)))
      ; else
      (progn 
	(print "Computing beta parameters for all the subjects...")
	(print "Be patient, this can take a while.")
	(v/ (iter (for i in-vector +cipic-subjects+)
		  (for j index-of-vector +cipic-subjects+)
		  (format t "subject ~a of ~a~%" (1+ j) (length +cipic-subjects+))
		  (reducing (beta length i) by #'v+))
	    (length +cipic-subjects+)))))



(defun azimuth-from-lookup (vals &rest rest &key subject cue &allow-other-keys)
  (let ((lfun (cond ; blame defun-memo for this ugly hack
		((eq cue #'itd) #'itd-lookup-table)
		((eq cue #'ild) #'ild-lookup-table)))
	(azimuths (make-array (length vals))))
    (iter (for i index-of-vector vals)
	  (with lookup = (funcall lfun subject :windowsize (length vals) :smoothing #'smooth!))
	  (setf (aref azimuths i)
		(iter (for j below (array-dimension lookup 0))
		      (finding (/ (* (aref +cipic-azimuths+ j) pi) 180)
			       minimizing (- (aref vals i) (aref lookup j i))))))
    azimuths))
  
(defun invg (x)
  (polynomial-value x '(1/1280 0 1/96 0 1/2 0)))

(defun azimuth-from-model (vals &rest rest &key subject cue sample-rate &allow-other-keys)
  (remf rest :subject)
  (let* ((azimuths (make-array (length vals)))
	 (subject-nb (position subject 
			       +cipic-subjects+ 
			       :test #'string=))
	 (head-radius (if (and subject-nb (elt +cipic-head-diameters+ subject-nb))
			  (elt +cipic-head-diameters+ subject-nb)
			  +default-head-diameter+)))
    (if (eq cue #'itd)
	(iter (for i index-of-vector vals)
	      (with alpha = (alpha (length vals) subject))
	      (setf (aref azimuths i)
		    (invg (/ (* +c+ (aref vals i))
			     (aref alpha i)
			     sample-rate
			     head-radius))))
	(iter (for i index-of-vector vals)
	      (with beta = (beta (length vals) subject))
	      (setf (aref azimuths i)
		    (if (and (aref vals i)
			     (< (abs (aref vals i))
				(abs (aref beta i))))
			(asin (/ (aref vals i)
				 (aref beta i)))
			nil)))) ;; ugly hack : in case of zero value in
    ;; either spectrum, the ild function returns nil...
    azimuths))


(defmacro with-stereo-sliding-window ((array frame &key (frame-length 1024) (hopsize 256)) &body body)
  `(iter (for i to (- (array-dimension ,array 0) ,frame-length) by ,hopsize)
	 (setf ,frame (subarray ,array (list i (+ i 
						  (1- ,frame-length))) nil))
	 ,@body))

(defmethod azimuth-estimation ((r number) (l number) &rest rest &key cue model subject omega)
  (let* ((val (funcall cue r l)))
    (case model
      (:lookup (azimuth-from-lookup val subject cue))
      (:individual (azimuth-from-model val subject cue))
      (:average (azimuth-from-model val nil cue)))))


(defun left (array)
  (subarray array nil 0))

(defun right (array)
  (subarray array nil 1))

(defun joint ()
  ())

; this is not it yet. we need the joint estimation case.
(defmethod azimuth-estimation ((right-array array) (left-array array) &rest rest &key (cue #'ild) (model :average) (subject "021") &allow-other-keys)
  (let* ((left-spectrum (half (sfft left-array)))
	 (right-spectrum (half (sfft right-array)))
	 (p-spectrum (v* (v-abs left-spectrum)
			 (v-abs right-spectrum)))
	 (max-p 5))
    (if (eq cue #'joint)
	(let ((vals (azimuth-estimation right-array left-array :cue #'ild :model model :subject subject)))
	  (setf (getf rest :cue) #'itd)
	  (iter (for i below (length vals))
		(iter (for j from (- max-p) to max-p)
		      (setf (getf rest :p) j)
		      (finding (apply #'azimuth-estimation 
				      (aref right-spectrum i)
				      (aref left-spectrum i)
				      rest)
			       minimizing (abs (- (apply #'azimuth-estimation 
							 (aref right-spectrum i)
							 (aref left-spectrum i)
							 rest)
						  (aref vals i)))))))
	
	(let ((vals (apply cue right-spectrum left-spectrum rest)))
	  (remf rest :model)
	  (values
	   (case model
	     (:lookup (apply #'azimuth-from-lookup vals rest))
	     (:individual (apply #'azimuth-from-model vals rest))
	     (:average (progn
			 (setf (getf rest :subject) nil)
			 (apply #'azimuth-from-model vals rest))))
	   p-spectrum)))))
	 
	  
	 

(defun global-azimuth-estimation (array &rest rest &key 
				  (frame-length 1024) 
				  (hopsize 256)
				  (sample-rate 44100)
				  (resolution 181)
				  (p '(0))
				  &allow-other-keys)
  (let ((window nil)
	(cpt 0)
	(res (make-array (list (/ frame-length 2) resolution))))
    (remf rest :frame-length)
    (remf rest :hopsize)
    (remf rest :p)
    (with-stereo-sliding-window (array window :frame-length frame-length :hopsize hopsize)
      (print (incf cpt))
      (iter (for i in p)
	    (multiple-value-bind (az amps) 
		(apply #'azimuth-estimation (right window) (left window) :p i :sample-rate sample-rate rest)
	      (iter (for j below (length az))
		    (when (and (realp (aref az j))
			       (< (abs (aref az j)) (/ pi 2)))
		      (incf (aref res j (round (* (1- resolution) 
						  (/ (+ (aref az j) 
							(/ pi 2)) 
						     pi))))
			    (aref amps j)))))))
			    
    res))

(defun logify (x)
  (let ((res (make-array (array-dimensions x))))
    (iter (for i below (apply #'* (array-dimensions x)))
	  (setf (row-major-aref res i)
		(to-decibels (+ (row-major-aref x i) 1/1))))
    res))

