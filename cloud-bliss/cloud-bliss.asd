;;; -*- Mode: Lisp; Package: COMMON-LISP-USER -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(use-package :asdf)

(defsystem :cloud-bliss
    :depends-on (:iterate :cloud-sp)
    :components ((:file "package")
    		 (:file "hrtf")
		 (:file "cipic")
		 (:file "interaural"))
    :serial t)
