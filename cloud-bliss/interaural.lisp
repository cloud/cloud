;;; -*- Mode: Lisp; Package: CLOUD.BLISS -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.bliss)

(defmethod phase-delay ((right-spectrum array) (left-spectrum array))
  (iter (for r in-vector right-spectrum)
	(for l in-vector left-spectrum)
	(collect (phase (/ r l))
	  result-type 'vector)))

(defmethod itd ((right-spectrum array) (left-spectrum array) &key (p 0) (sample-rate 1) (unwrap nil) &allow-other-keys)
  (v/ (v+ (if unwrap
	      (unwrap (phase-delay right-spectrum left-spectrum))
	      (phase-delay right-spectrum left-spectrum))
	  (* 2 pi p))
      (v-make 1 (length right-spectrum))
      (/ pi (length right-spectrum))
      sample-rate))

(defmethod phase-delay ((r number) (l number))
  (phase (/ r l)))

(defmethod itd ((r number) (l number) &key (p 0) (sample-rate 1) (omega pi) &allow-other-keys)
  (/ (+ (phase-delay r l)
	(* 2 pi p))
     omega
     sample-rate))
     
(defun ild (right-spectrum left-spectrum &key &allow-other-keys)
  (iter (for r in-vector right-spectrum)
	(for l in-vector left-spectrum)
	(collect (unless (or (zerop r)
			 (zerop l))
		   (* 20 (log (abs (/ r l)) 10)))
	  result-type 'vector)))

(defun unwrap (s &optional (tol pi))
  (iter (for el in-vector s)
	(for p-el previous el initially (aref s 0))
	(with k = 0)
	(collect (cond
		   ((>= (- p-el el) tol) (+ el (* 2 pi (incf k))))
		   ((>= (- el p-el) tol) (+ el (* 2 pi (decf k))))
		   (t (+ el (* 2 pi k))))
	  result-type 'vector)))