;;; -*- Mode: Lisp; Package: READ-BYTES -*-

;;;  (c) copyright 2003-2004 by
;;;           Robert Strandh <strandh@labri.fr>
;;;
;;;  (c) copyright 2003-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  This is a package for reading bytes from a stream and getting
;;;  the appropriate number associated to a certain amount of bytes.
;;;  

(defpackage :read-bytes
  (:documentation "Binary bytes reader.

This package provides a binary bytes reader that makes Lisp capable of
reading binary encoded numbers, for example with C. It supports both
big and little endian numbers.

There are two predefined types that are 'single-float and
'double-float that correspond to the C types. In order to read or
write such numbers, you can use either the read-IEEE-single-float,
write-IEEE-single-float, read-IEEE-double-float, and
write-IEEE-double-float, or get the reader or write corresponding to a
given type (previously defined). Let us illustrate this for further clarity.

Example of usage for predefined types, fetching the appropriate
functions given a type:

;;; CL-USER> (with-open-file
;;;              (s \"test.bin\"
;;;                 :direction :output
;;;                 :element-type '(unsigned-byte 8)
;;;                 :if-exists :supersede)
;;; 	       (define-endianess :little-endian)
;;; 	       (define-output-stream s)
;;; 	       (funcall (IEEE-float-writer 'double-float) 56.3332563322d0))
;;; NIL
;;; CL-USER> (with-open-file 
;;;              (s \"test.bin\"
;;;                 :direction :input
;;;                 :element-type '(unsigned-byte 8))
;;; 	       (define-endianess :little-endian)
;;; 	       (define-input-stream s)
;;; 	       (funcall (IEEE-float-reader 'double-float)))
;;; 56.3332563322d0

Example of new number encoding definition:

;;; (define-IEEE-float IEEE-SANE-extended-float
;;;     :sign-bits 1
;;;     :exponent-bits 15
;;;     :mantissa-bits 64
;;;     :bias 16383
;;;     :associated-type 'long-float)

Then in order to use the newly defined encoding, you can use the first
example provided above to read and write (with type 'long-float), or
use the newly created functions write-IEEE-SANE-extended-float and
read-IEEE-SANE-extended-float:

;;; CL-USER> (with-open-file
;;;              (s \"test.bin\"
;;;                 :direction :output
;;;                 :element-type '(unsigned-byte 8)
;;;                 :if-exists :supersede)
;;; 	       (define-endianess :little-endian)
;;; 	       (define-output-stream s)
;;; 	       (write-IEEE-SANE-extended-float 56.3332563322d0))
;;; NIL
;;; CL-USER> (with-open-file 
;;;              (s \"test.bin\"
;;;                 :direction :input
;;;                 :element-type '(unsigned-byte 8))
;;; 	       (define-endianess :little-endian)
;;; 	       (define-input-stream s)
;;; 	       (read-IEEE-SANE-extended-float))
;;; 56.3332563322d0
")
  (:use :cl :cl-user)
  (:export *endianess*
	   define-endianess
	   string-code
	   define-input-stream
	   define-output-stream
	   read-unsigned-integer
	   read-signed-integer
	   write-unsigned-integer
	   write-signed-integer
	   define-IEEE-float
	   IEEE-float-reader
	   IEEE-float-writer
	   read-IEEE-double-float
	   write-IEEE-double-float
	   read-IEEE-single-float
	   write-IEEE-single-float
	   read-IEEE-SANE-extended-float
	   write-IEEE-SANE-extended-float
	   with-gensyms))

(provide :read-bytes)

(in-package :read-bytes)

;;; From Paul Graham's "On Lisp"

(defmacro with-gensyms (syms &body body)
  `(let ,(mapcar #'(lambda (s)
                     `(,s (gensym)))
                 syms)
    ,@body))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Some definitions

(deftype endianess ()
  "Defines the legal values for *endianess*."
  '(member :big-endian :little-endian))

(defvar *endianess* nil
  "*endianess* has to be either :big-endian or :little-endian")

(defparameter *binary-input-stream* nil
  "stream for reading binary coded numbers.")
(defparameter *input-buffer* '()
  "used for unreading bytes from *binary-input-stream*.")
(defparameter *binary-output-stream* nil
  "stream for writing binary coded numbers.")

(defun define-endianess (endian)
  (check-type endian endianess)
  (setf *endianess* endian))

(defun define-input-stream (input)
  (setf *binary-input-stream* input))

(defun define-output-stream (output)
  (setf *binary-output-stream* output))

(defun output-stream ()
  *binary-output-stream*)

(defun input-stream ()
  *binary-input-stream*)

(defun string-code (s)
  "compute the ASCII-based numerical value of the string [warning:
works only if the chars are coded in ASCII]"
  (loop with result = 0
	for c across (case *endianess*
		       (:little-endian (reverse s))
		       (:big-endian s))
	do (setf result (+ (* result 256) (char-code c)))
	finally (return result)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Reading

(defun read-next-byte ()
  "read an unsigned 8-bit byte from *binary-input-stream*
   checking for unread bytes"
  (if *input-buffer*
      (pop *input-buffer*)
      (read-byte *binary-input-stream*)))

(defun unread-byte (byte)
  "unread a byte from *binary-input-stream*"
  (push byte *input-buffer*))

(defun read-unsigned-integer (nb-bytes)
  "read an unsigned integer of nb-bytes bytes
   from *binary-input-stream*"
  (if (zerop nb-bytes)
      0
      (case *endianess*
	(:little-endian (+ (read-next-byte)
			   (ash (read-unsigned-integer (1- nb-bytes))
				8)))
	(:big-endian (+ (ash (read-unsigned-integer (1- nb-bytes))
			     8)
			(read-next-byte))))))

(defun read-signed-integer (nb-bytes)
  "read a signed integer of nb-bytes bytes from an array"
  (let ((unsigned-value 0))
    (setf unsigned-value (read-unsigned-integer nb-bytes))
    (if (>= unsigned-value (ash 1 (1- (* 8 nb-bytes))))
	(- unsigned-value (ash 1 (* 8 nb-bytes)))
        unsigned-value)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Writing

(defun write-bytes (&rest bytes)
  "write an arbitrary number of bytes to *binary-output-stream*"
  (mapc #'(lambda (byte)
	    (write-byte byte *binary-output-stream*))
	bytes))

(defun write-unsigned-integer (quantity nb-bytes)
  "write an unsigned integer of nb-bytes bytes 
   to *binary-output-stream*"
  (case *endianess*
    (:little-endian (unless (zerop nb-bytes)
		      (write-bytes (logand quantity #xff))
		      (write-unsigned-integer (ash quantity -8)
					      (1- nb-bytes))))
    (:big-endian (unless (zerop nb-bytes)
		      (write-unsigned-integer (ash quantity -8)
					      (1- nb-bytes))
		      (write-bytes (logand quantity #xff))))))

(defun write-signed-integer (number nb-bytes)
  "write a signed integer number on nb-bytes
   to *binary-output-stream*"
  (if (< number 0)
      (write-unsigned-integer (+ number
				 (ash 1 (* 8 nb-bytes)))
			      nb-bytes)
      (write-unsigned-integer number nb-bytes)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Floating point numbers support


(defparameter *IEEE-floats* (make-hash-table :test #'eq))

(defmacro define-IEEE-float (name
			     &key (sign-bits 1)
			     (exponent-bits 8)
			     (mantissa-bits 23)
			     (bias 127)
			     (associated-type 'float))
  "Creates a IEEE float type and the associated writer and reader.
   These are placed in a hash-table."
  (with-gensyms (significant 
		 exponent 
		 sign 
		 s-bits 
		 e-bits 
		 m-bits
		 b 
		 value)
    `(progn
      (setf (gethash ,associated-type *IEEE-floats*)
       ',(list (intern (string-upcase (format nil "write-~s" name)))
	       (intern (string-upcase (format nil "read-~s" name)))))

      (defun ,(intern (string-upcase (format nil "read-~s" name))) ()
	(let* ((,s-bits ,sign-bits)
	       (,e-bits ,exponent-bits)
	       (,m-bits ,mantissa-bits)
	       (,b ,bias)
	       (,value (read-unsigned-integer
			(/ (+ ,s-bits ,e-bits ,m-bits) 8))))
	  (if (zerop (ldb (byte (+ ,m-bits ,e-bits) 0) ,value))
	      (coerce 0 ,associated-type)               ; handle zero
	      (coerce (* (dpb 1                         ; mantissa
			      (byte 1 ,m-bits)
			      (ldb (byte ,m-bits 0) ,
				   value))            
			 (expt 2 (- (ldb (byte ,e-bits ,m-bits)
					 ,value)        ; exponent
				    ,b)) 
			 (expt -1 (ldb (byte ,s-bits    ; sign
					     (+ ,e-bits ,m-bits))
				       ,value))) 
		      ,associated-type))))

      (defun ,(intern (string-upcase (format nil "write-~s" name)))
	  (,value)
	(let ((,s-bits ,sign-bits)
	      (,e-bits ,exponent-bits)
	      (,m-bits ,mantissa-bits)
	      (,b ,bias))
	  (multiple-value-bind (,significant ,exponent ,sign)
	      (integer-decode-float ,value)
	    (when (and (zerop ,significant)           ; handle zero
		       (= ,exponent (- (+ ,b ,m-bits))))
	      (setf ,exponent (- ,b)))
	    (write-unsigned-integer 
	     (dpb (/ (1- ,sign) 2)                    ; sign
		  (byte ,s-bits (+ ,e-bits ,m-bits))
		  (dpb (+ ,exponent ,b)               ; exponent
		       (byte ,e-bits ,m-bits)
		       (dpb ,significant              ; mantissa
			    (byte ,m-bits 0)
			    0)))
	     (/ (+ ,s-bits ,e-bits ,m-bits) 8))))))))


(defun IEEE-float-reader (type)
  "Retrieve the reader function for a given type."
  (cadr (gethash type *IEEE-floats*)))

(defun IEEE-float-writer (type)
  "Retrieve the writer function for a given type."
  (car (gethash type *IEEE-floats*)))
    
(define-IEEE-float IEEE-double-float
    :sign-bits 1
    :exponent-bits 11
    :mantissa-bits 52
    :bias 1023
    :associated-type 'double-float)

(define-IEEE-float IEEE-single-float
    :sign-bits 1
    :exponent-bits 8
    :mantissa-bits 23
    :bias 127
    :associated-type 'single-float)

(define-IEEE-float IEEE-SANE-extended-float
    :sign-bits 1
    :exponent-bits 15
    :mantissa-bits 64
    :bias 16383
    :associated-type 'long-float)