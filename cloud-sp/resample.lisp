;;; -*- Mode: Lisp; Package: CLOUD.SP -*-

;;;  (c) copyright 2005-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.sp)


(defun sinc (x)
  "The cardinal sine function."
  (if (zerop x)
      1
      (/ (sin (* pi x)) (* pi x))))


(defun reconstructor-value (x wings &key (window :hann))
  (* (sinc x) 
     (window-value (/ (+ x wings) 
		      (* 2 wings))
		   window)))


(defun classical-analog-value (s x &key (wings 10))
  (let ((left (floor x))
	(right (ceiling x)))
    (if (= left right)
	(aref s left)
	  (iter (for i from (- right wings) to (+ left wings))
		(summing (* (reconstructor-value (- i x) wings) 
			    (if (or (minusp i)
				    (>= i (length s)))
				0
				(aref s i)))
			 into res)
		(finally (return res))))))

(defun classical-resample (s source-rate dest-rate 
		 &key (wings 10))
  (let ((result (make-array (ceiling (* (length s)
					dest-rate
					(/ source-rate)))))
	(factor (/ source-rate
		   dest-rate)))
    (iter (for i below (length result))
	  (setf (aref result i)
		(classical-analog-value s (* factor i) :wings wings))
	  (finally (return result)))))
	       


(defun no-centering (seq x)
  (declare (ignore x))
  (values seq 0))

(defun mean-centering (seq x)
  (declare (ignore x))
  (let ((mean (mean seq)))
    (values (v- seq mean) mean)))


(let ((coeffs nil)
      (rec-seq nil)
      (rec-order nil)
      (poly nil))
  (defun poly-centering (seq x &optional (order 3))
    (let* ((increment (/ (length seq))))
      
      (unless (and (eq rec-order order)
		   (equalp seq rec-seq))
	(setf coeffs (least-squares-estimation seq :order order)
	      rec-seq seq
	      rec-order order
	      poly (polynomial-value (v-make 0
					     (- 1 increment)
					     :by increment)
				     coeffs)))
      (values (v- seq poly)
	      (polynomial-value (/ x (length seq))
				coeffs)))))


(defun analog-value (s x 
		     &key (wings 10) 
		     (centering-fun #'no-centering))
  "Finds the analog value of a signal at arbitrary positions."
  (let ((left (floor x))               
	(right (ceiling x)))           
    (if (= left right)
	(aref s left)
	(let* ((left-boundary (- right wings))
	       (right-boundary (+ left wings)) 
	       (shifted-x (if (> x wings)
			      (- x left-boundary)
			      x)))
	  (multiple-value-bind (centered-sequence add-value)
	      (funcall centering-fun 
		       (subseq s 
			       (max 0 left-boundary)
			       (min (1+ right-boundary) (length s)))
		       shifted-x)
	    (iter (for i from left-boundary to right-boundary)
		  (summing (* (reconstructor-value (- i x) wings) 
			      (if (or (minusp i)
				      (>= i
					  (length s)))
				  0
				  (aref centered-sequence
					(- i (max 0 left-boundary)))))
			   into res)
		  (finally (return (+ res add-value)))))))))


(defun resample (s source-rate dest-rate 
		 &key (wings 10) 
		 (centering-fun #'no-centering)
		 (offset 0))
  "Resamples a signal. In case of downsampling, the signal is NOT
band-limited."
  (let* ((factor (/ source-rate
		    dest-rate))
	 (result (make-array (ceiling (* (- (length s) (mod offset factor))
					dest-rate
					(/ source-rate))))))
    (iter (for i below (length result))
	  (setf (aref result i)
		(analog-value s (+ (* factor i) (mod offset factor)) :wings wings
			      :centering-fun centering-fun))
	  (finally (return result)))))
	       
