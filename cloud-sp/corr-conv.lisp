;;; -*- Mode: Lisp; Package: CLOUD.SP -*-

;;;  (c) copyright 2005-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  This file provides functions to perform correlation and
;;;  convolution.
;;;  

(in-package :cloud.sp)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Convolution.
;;;

(defun old-convolution (a b)
  (let ((result (make-array (1- (+ (length a)
				   (length b))))))
    (iter (for i index-of-vector result)
	  (iter (for j below (length a))
		(for index = (- i j))
		(unless (or (>= index (length b))
			    (< index 0))
		  (incf (aref result i) (* (aref a j)
					   (aref b (- i j)))))))
    result))

(defun convolution (a b)
  (let ((l (+ (length a) (length b) -1)))
    (subseq (sifft (v* (sfft (adjust-array a (power-of-two l))) 
		       (sfft (adjust-array b (power-of-two l)))))
	    0 l)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Correlation.
;;;

(defun correlation (a b)
  (iterate (for i in-vector a)
	   (for j in-vector b)
	   (collect (* i j) result-type 'vector)))
