;;; -*- Mode: Lisp; Package: COMMON-LISP-USER -*-

;;;  (c) copyright 2007-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Cloud-maths package definition.

(defpackage :cloud.sp
  (:nicknames :cloud-sp)
  (:use :cloud-maths :iterate :cl :cl-user)
  (:export help
           convolution
	   correlation
	   make-fourier-instance
	   make-fft-instance
	   fft-common
	   fft
	   fft!
	   sfft
	   sifft
	   with-fft-instance
	   make-ifft-instance
	   ifft-common
	   ifft
	   ifft!
	   least-squares-estimation
	   polynomial-value
	   closest-polynomial
	   arburg
	   burg-extrapolate-1
	   arautocorrelation
	   :rectangular
	   :hann
	   :hanning
	   :hamming
	   :blackman 
	   :barlett
	   :triangular
	   rectangular-value
	   rectangular
	   hann-value
	   hann
	   hamming-value
	   hamming
	   blackman-value
	   blackman
	   barlett-value
	   barlett
	   triangular
	   triangular-value
	   hanning
	   hanning-value
	   make-window
	   window-value
	   n-fill-window
	   n-apply-window
	   apply-window
	   apply-window!
	   sinc
	   resample
	   poly-centering
	   with-sliding-window
	   normalize
	   to-decibels
	   filter))