;;; -*- Mode: Lisp; Package: CLOUD.SP -*-

;;;  (c) copyright 2004-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Least-squares method. Will eventually include ls and wls at
;;;   least. 


(in-package :cloud.sp)

(defmacro-clause (matrix-sum expr &optional into var)
  `(reducing ,expr by #'m+ into ,var))

(defun least-squares-estimation (s &key (order 3))
  "Finds the coefficients of the closest polynomial of order 'order'
by a least squares estimation. Highest order coeffs first. The
polynomial is considered to be defined between 0 and 1."
  (let* ((l (length s)))
    (iterate (for i from 0 below l)
	     (let ((p (make-array (1+ order)
				  :initial-contents 
				  (iter (for j from 0 to order)
					(collect (expt (/ i l) j))))))
	       (matrix-sum (m* p p) into M)
	       (matrix-sum (m* p (aref s i)) into B))
	     (finally (inverse-matrix M)
		      (return (coerce (nreverse (m* M B))
				      'list))))))

(defun polynomial-value (x coeffs &key (element-type t))
  "Computes the polynomial values of the polynomial defined by
   its coefficients. 'x' can be a number or an array."
  (if (arrayp x)
      (let ((result (make-array (length x) :element-type element-type)))
	(iterate (for elt in-vector x)
		 (for i index-of-vector result)
		 (setf (aref result i) (polynomial-value elt coeffs))
		 (finally (return result))))
      (let ((result (car coeffs)))
	(iterate (for c in (cdr coeffs))
		 (setf result (+ (* x result) c))
		 (finally (return result))))))

(defun n-length-polynomial (coeffs n &key (element-type t))
  "Computes the polynomial defined by 'coeffs' for 'n' points between 0 and 1."
  (let ((inc (/ n)))
    (polynomial-value (v-make 0 (- 1 inc)
		       :by inc
		       :element-type element-type)
		      coeffs
                      :element-type element-type)))


(defun closest-polynomial (s &key (order 3))
  "Makes a polynomial approximation of s."
  (let ((inc (/ (length s)))
	(coeffs (least-squares-estimation s :order order)))
    (values (polynomial-value (v-make 0 (- 1 inc)
				      :by inc
				      :element-type
				      (array-element-type s))
			      coeffs
			      :element-type (array-element-type s))
	    coeffs)))

(defun remove-polynomial (s &key (order 3))
  "Removes a polynomial approximation of s from s."
  (multiple-value-bind (approximation coeffs)
      (closest-polynomial s :order order)
    (values (v- s approximation)
	    coeffs)))