;;; -*- Mode: Lisp; Package: CLOUD.SP -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud-sp)

(defun help (fun)
  "Gives documentation on a function."
  (documentation fun 'function))

(defmacro with-sliding-window ((array frame &key (frame-length 1024) (hopsize 256)) &body body)
  `(iter (for i to (- (length ,array) ,frame-length) by ,hopsize)
	 (setf ,frame (make-array ,frame-length 
				  :displaced-to ,array 
				  :displaced-index-offset i
				  :element-type (array-element-type ,array)))
	 ,@body))

(defun normalize (array)
  (let ((max (iter (for i below (apply #'* (array-dimensions array)))
		   (maximize (row-major-aref array i))))
	(min (iter (for i below (apply #'* (array-dimensions array)))
		   (minimize (row-major-aref array i))))
	(res (make-array (array-dimensions array))))
    (iter (for i below (apply #'* (array-dimensions array)))
	  (setf (row-major-aref res i) (/ (- (row-major-aref array i) min) (- max min))))
    res))

(declaim (inline to-decibels))

(defun to-decibels (x)
  (* 20 (log x 10)))