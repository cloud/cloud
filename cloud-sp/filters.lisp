;;; -*- Mode: Lisp; Package: CLOUD.SP -*-

;;;  (c) copyright 2004-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud-sp)

(defun filter (x a b)
  (let ((y (make-array (length x)))
	(n (length b))
	(m (length a)))
    (setf (aref y 0) (/ (* (aref b 0) (aref x 0)) (aref a 0)))
    (iter (for i from 1 below (length x))
	  (setf (aref y i) 
		(/ (- (scalar-product b (reverse (pad-array (subseq x (max 0 (- i n -1)) (1+ i))
							    :before (max 0 (- n i 1)))))
		      (scalar-product (subseq a 1) (reverse (pad-array (subseq y (max 0 (- i m -1)) i)
								       :before (max 0 (- m i 1))))))
		   (aref a 0)))
	  (finally (return y)))))