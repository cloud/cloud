;;; -*- Mode: Lisp; Package: COMMON-LISP-USER -*-

;;;  (c) copyright 2008-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(use-package :asdf)

(defsystem :cloud-sp
    :depends-on (:iterate :cloud-maths)
    :components ((:file "package")
    		 (:file "misc")
		 (:file "fft")
		 (:file "corr-conv")
		 (:file "least-squares")
		 (:file "lpc")
		 (:file "windows")
		 (:file "resample"))
    :serial t)
