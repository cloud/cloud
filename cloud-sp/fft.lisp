;;; -*- Mode: Lisp; Package: CLOUD.SP -*-

;;;  (c) copyright 2004-2009 by
;;;           Robert Strandh <strandh@labri.fr>
;;;           Sylvain Marchand <sm@labri.fr>
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; FFT implementation

(in-package :cloud.sp)

(defclass fft-instance ()
  ((size :initarg :size)
   (inter :initarg :inter)
   (coeffs :initarg :coeffs)))


(deftype real-sample () `double-float)

(deftype complex-sample () `(complex double-float))

(deftype real-sample-array ()  `(simple-array real-sample (*)))

(deftype complex-sample-array ()  `(simple-array complex-sample (*)))


(defun make-fourier-instance (size direction) 
  ;; direction 1 direct, -1 inverse
  ;; check here that size is a power of two
  (let ((inter (make-array size 
			   :element-type 'complex-sample))
        (coeffs (make-array (ash size -1) 
			    :element-type 'complex-sample))
        (coeff #c(1d0 0d0))
        (factor (exp (/ (* direction -2 pi #c(0 1)) size))))
    (loop for i of-type fixnum from 0 below (ash size -1)
          do (setf (aref coeffs i) coeff)
          do (setf coeff (* coeff factor)))
    (make-instance 'fft-instance
                   :size size
                   :inter inter
                   :coeffs coeffs)))

(defun make-fft-instance (size)
  (make-fourier-instance size 1))

(defun fft-common (instance source dest)
  (let ((inter (slot-value instance 'inter))
        (coeffs (slot-value instance 'coeffs))
        (size (slot-value instance 'size)))
    (declare (type complex-sample-array
		   source dest inter coeffs))
    (declare (type fixnum size))
    (assert (= size (length source)))
    (assert (= size (length dest)))
    (labels ((aux (n starts steps startd)
               (declare (optimize (speed 3) (safety 0))
                        (type fixnum starts steps startd))
               (if (= n 2)
                   (let ((a (aref source starts))
                         (b (aref source (+ starts steps))))
                     (declare (type (complex double-float) a b))
                     (setf (aref dest startd) (+ a b)
                           (aref dest (1+ startd)) (- a b))
                     nil)
                   (let ((2*steps (ash steps 1))
                         (n/2 (ash n -1)))
                     (declare (type fixnum 2*steps n/2))
                     (rotatef dest inter)
                     (aux n/2 starts 2*steps startd)
                     (aux n/2 
			  (+ starts steps) 
			  2*steps 
			  (+ startd n/2))
                     (rotatef dest inter)
                     (loop for i of-type fixnum 
			   from (the fixnum (+ startd n/2))
                           for c of-type fixnum from 0 by steps
                           for dummy of-type fixnum from 0 below n/2
                           do (setf (aref inter i) 
				    (* (aref coeffs c) 
				       (aref inter i))))
                     (loop for i of-type fixnum from startd
                           for j of-type fixnum 
			   from (the fixnum (+ startd n/2))
                           for dummy of-type fixnum from 0 below n/2
                           do (let ((a (aref inter i))
                                    (b (aref inter j)))
                                (declare (type (complex double-float)
					       a b))
                                (setf (aref dest i) (+ a b)
                                      (aref dest j) (- a b))))
                     nil))))
      (aux size 0 1 0)
      dest)))

(let* ((array-size 1024)
       (instance (make-fft-instance array-size)))

  (defun fft (source)
    "Returns the Fourier transform of source, allocating a new array
     for the result."
    (when (/= array-size (length source))
      (setf array-size (length source)
	    instance (make-fft-instance array-size)))
    (let ((dest (make-array array-size
			    :element-type '(complex double-float))))
      (fft-common instance source dest)))
  
  (defun fft! (source dest)
    "Destructive version of fft, since it fills dest."
    (when (/= array-size (length source))
      (setf array-size (length source)
	    instance (make-fft-instance array-size)))
    (fft-common instance source dest)))
	       
(defun sfft (source &optional len)
  "This is the generic fft function. Stands for stupid fft. Can take
   any kind of array as input."
  (let* ((l (if len
		len
		(length source)))
	 (lp (power-of-two l))
	 (new-source (make-array lp :element-type 'complex-sample)))
    (iter (for (the fixnum i) below (length source))
	  (setf (aref new-source i) (coerce (aref source i) 'complex-sample)))
    (fft new-source)))

(defmacro with-fft-instance (instance size &body body)
  `(let ((,instance (make-ftt-instance ,size)))
    ,@body))



;; implementation of an approximate generalised fft
(defun myfft (source)
  (let* ((n (length source))
	 (p (power-of-two (* 2 n)))
	 (k (concatenate 'vector (v-make 0 (1- n))
			 (make-array (1+ (- p (* 2 n))) :initial-element 0)
			 (v-make (1- n) 1 :by -1)))
	 (y (v-fun #'exp (v* pi #c(0 1) k k (/ n))))
	 (nx (v/ (concatenate 'vector source
			      (make-array (- p n) :initial-element 0))
		 y)))
    (format t "~a~%" (v/ (sifft (v* (sfft nx) (sfft y))) p))
    (subseq (v/ (sifft (v* (sfft nx) (sfft y)))
		y) 0 n)))
	
	   




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Inverse Fast Fourier Transform


(defun make-ifft-instance (size)
  (make-fourier-instance size -1))

(defun ifft-common (instance source dest)
  (fft-common instance source dest)
  ;; very time consuming !!!
  (map 'complex-sample-array #'(lambda(x) (/ x (length source))) dest))



(let* ((array-size 1024)
       (instance (make-ifft-instance array-size)))

  (defun ifft (source)
    "Returns the inverse Fourier transform of source, allocating a new
     array for the result."
    (when (/= array-size (length source))
      (setf array-size (length source)
	    instance (make-ifft-instance array-size)))
    (let ((dest (make-array array-size
			    :element-type '(complex double-float))))
      (ifft-common instance source dest)))
  
  (defun ifft! (source dest)
    "Destructive version of ifft, since it fills dest."
    (when (/= array-size (length source))
      (setf array-size (length source)
	    instance (make-ifft-instance array-size)))
    (ifft-common instance source dest)))

(defun sifft (source)
  "This is the generic fft function. Stands for stupid fft. Can take
   any kind of array as input."
  (let* ((l (length source))
	 (lp (power-of-two l))
	 (new-source (if (typep (aref source 0) 'complex-sample)
			 source
			 (map 'complex-sample-array 
			      #'(lambda (x)
				  (coerce x 'complex-sample))
			      source)))
	 (modified-source
	  (adjust-array new-source lp :initial-element #c(0.0d0 0.0d0)
			:element-type 'complex-sample)))
    (ifft modified-source)))
