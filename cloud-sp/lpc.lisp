;;; -*- Mode: Lisp; Package: CLOUD.SP -*-

;;;  (c) copyright 2004-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  This file provides functions to perform linear prediction.
;;;  


(in-package :cloud.sp)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Burg prediction functions
;;;

(defun arburg (s order)
  "Computes the linear prediction coefficients using the Burg method."
  (let* ((ef (copy-seq s))
	 (eb (copy-seq s))
	 (a (make-array 1 
			:initial-element 1.0d0 
			:element-type 'double-float 
			:adjustable t))
	 (efp (copy-seq s))
	 (ebp (copy-seq s))
	 dp
	 (ra a)
	 (k 0.0d0))
    (loop for m from 0 below order
       do (setf efp (subseq ef 1)
		ebp (subseq eb 0 (length efp))
		dp (+ (scalar-product ebp ebp) 
		      (scalar-product efp efp))
		k (if (zerop dp)
		      ;; this is a hack. I should be fine, but it has not
		      ;; been mathematically verified.
		      1
		      (/ (* -2 
			    (scalar-product ebp efp)) 
			 dp)))
	 (setf ef (copy-seq efp)
	       eb (copy-seq ebp))
	 
	 (loop for i of-type fixnum from 0 below (length efp) do
	      (incf (aref ef i) (* (aref ebp i) k))
	      (incf (aref eb i) (* (aref efp i) k)))
	 
	 (setf ra (reverse 
		   (adjust-array a (+ m 2) :initial-element 0.0d0)))
	 (loop for i of-type fixnum from 0 below (length a) do
	      (incf (aref a i) (* k (aref ra i)))))
    
    a))

(defun burg-extrapolate-1 (s order)
  "Extrapolates the signal by 1 sample."
  (- (scalar-product (subseq (arburg s order) 1) (reverse s))))

(defun burg-reverse-extrapolate-1 (s order)
  "Extrapolates the reverse signal by 1 sample."
  (- (scalar-product (subseq (arburg s order) 1) s)))

(defun burg-extrapolate-n (s order n)
  "Extrapolates the signal by n samples."
  (if (plusp (length s))
      (let ((filter (subseq (arburg s order) 1))
	    (result (copy-seq s)))
	(iterate (for i from 0 below n)
		 (setf result 
		       (concatenate 'vector 
				    result 
				    (list (- (apply #'+ 
						    (map 'list 
							 #'* 
							 filter 
							 (reverse result))))))))
	result)
      (aref s 0)))

(defun burg-extrapolation-n (s order n)
  "Extrapolates the signal by n samples."
  (if (plusp (length s))
      (let ((filter (subseq (arburg s order) 1))
	    (result (copy-seq s)))
	(iterate (for i from 0 below n)
		 (setf result 
		       (concatenate 'vector 
				    result 
				    (list (- (apply #'+ 
						    (map 'list 
							 #'* 
							 filter 
							 (reverse result))))))))
	(subseq result (length s)))
      (aref s 0)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Autocorrelation prediction functions
;;;

(defun arautocorrelation (s order)
  (let* ((m (length s))
	 (s2 (concatenate 'vector s (make-array m)))
	 (ac (make-array m :initial-contents
			 (iter (for i from 0 below m)
			       (collect (iter (for j from 0 below m)
					      (sum (* (aref s j)
						      (aref s2 (+ i j))))))))))
    (levinson ac order)))
		     
		   

(defun levinson (acf &optional p)
  "Durbin-Levinson algorithm."
  (unless p
    (setf p (1- (length acf))))
  (let* ((ref (make-array p))
	 (g (/ (aref acf 1) (aref acf 0)))
	 (a (make-array 1))
	 (v (* (- 1 (* g g)) (aref acf 0))))
    (setf (aref a 0) g
	  (aref ref 0) g)
    (iter (for i from 1 below p)
	  (print a)
	  (force-output)
	  (setf g (/ (- (aref acf (1+ i))
			(iter (for j from 0 below i)
			      (sum (* (aref a j)
				      (aref acf (1+ j))))))
		     v))
	  (setf a (concatenate 'vector
			       (list g)
			       (v- a (v* g (reverse a)))))
	  (setf v (* v (- 1 (* g g))))
	  (print a)
	  (print g)
	  (force-output)
	  (setf (aref ref i) g))
    (setf a (concatenate 'vector (list 1)
			 (v- (reverse a))))
    (values a v ref)))

(defun extrapolate-array (array &key (before 0) (after 0) (max-order 8))
  (let* ((element-type (array-element-type array))
	 (result-array (make-array (+ (length array)
				      before
				      after)
			:element-type element-type))
	 (after-array (burg-extrapolation-n array 
					    (min max-order 
						 (1- (length array)))
					    after))
	 (before-array (burg-extrapolation-n (reverse array) 
					     (min max-order 
						  (1- (length array)))
					     before)))
    (iter (for i below before)
	  (setf (aref result-array i)
		(aref before-array i)))
    (iter (for i below (length array))
	  (for j = (+ i before))
	  (setf (aref result-array j) (aref array i)))
    (iter (for i below after)
	  (for j = (+ i (length array) before))
	  (setf (aref result-array j)
		(aref after-array i)))
    result-array))