;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2007-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.clicks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Implements functions to handle sounds as made of spectral
;;;   peaks only.

(defmacro with-gensyms (syms &body body)
  `(let ,(mapcar #'(lambda (s)
                     `(,s (gensym)))
                 syms)
    ,@body))

(defconstant +default-sample-rate+ 44100)

(defclass peak-sound (sound)
  ((data-file :initarg :data-file :accessor data-file)
   (envelope-file :initarg :envelope-file :accessor envelope-file)))

(defun touch-file (filename)
  (close (open filename :direction :output :if-does-not-exist :create)))

(defun unique-filename (prefix extension &key (path "."))
  (let* ((time (get-internal-real-time))
	 (filename (format nil "~a/~a~a.~a" path prefix time extension)))
    (loop while (probe-file filename)
       do (incf time)
	 (setf filename (format nil "~a/~a~a.~a" 
				path prefix time extension)))
    filename))

(defun create-peak-sound (&key (nb-channels 1) (sample-rate +default-sample-rate+))
  (let* ((peakname (unique-filename "test" "peaks" :path "tmp"))
	 (envelname (unique-filename "test" "coeffs" :path "tmp")))
    (touch-file peakname)
    (touch-file envelname)
    (make-instance 'peak-sound
                    :data-file peakname
		    :envelope-file envelname
		    :sample-rate sample-rate
		    :nb-channels nb-channels)))


(defmacro with-peak-sound ((peak-output envelope-output peak-sound)
			   &body body)
  (let ((p-sound (gensym)))
    `(let ((,p-sound ,peak-sound))
       (with-open-file (,peak-output (data-file ,p-sound)
			:direction :output
			:if-exists :supersede)
	 (with-open-file (,envelope-output (envelope-file ,p-sound)
			  :direction :output
			  :if-exists :supersede)
	 ,@body)))))
  
(defun store-peaks (peak-output peaks)
  (format peak-output "~s~%" peaks))

(defun store-envelope (envelope-output coeffs)
  (format envelope-output "~s~%" coeffs))


(defmacro with-elements-from-peak-sound (p-sound peak-iterator 
					 &body body)
  (with-gensyms (s)
    `(let ((,peak-iterator nil))
       (with-open-file (,s (data-file ,p-sound) 
			  :direction :input
			  :if-does-not-exist nil)
	 (iterate
	   (while (not (eq (peek-char nil ,s nil 'EOF nil) 'EOF)))
	   (setf ,peak-iterator (read ,s nil nil))
	     ,@body)))))

(defmacro with-elements-from-peak-sound-envelope (p-sound peak-iterator 
						  &body body)
  (with-gensyms (s)
    `(let ((,peak-iterator nil))
       (with-open-file (,s (envelope-file ,p-sound) 
			  :direction :input
			  :if-does-not-exist nil)
	 (iterate
	   (while (not (eq (peek-char nil ,s nil 'EOF nil) 'EOF)))
	   (setf ,peak-iterator (read ,s nil nil))
	   ,@body)))))

(defun add-to-array (in-array out-array &key (offset 0))
  (iter (for i from offset below (min (+ offset (length in-array))
				      (length out-array)))
	(incf (aref out-array i) (aref in-array (- i offset))))
  out-array)

(defmethod envelope ((p-sound peak-sound) 
		     &key (sample-rate +default-sample-rate+)
		     (f-cut 2))
  (when (envelope-file p-sound)
    (let* ((i 0)
	   (hop-size (/ sample-rate (sample-rate p-sound)))
	   (overlap-factor (expt 2
				 (ceiling (log (/ (sample-rate p-sound)
						  f-cut)
					       2))))
	   (frame-length (* overlap-factor hop-size))
	   (result-array (make-array frame-length :adjustable t))
	   (c-array (make-array frame-length :adjustable t))
	   tmp-array)
      (with-elements-from-peak-sound-envelope p-sound coeffs
	(setf tmp-array (n-length-polynomial coeffs
					     frame-length))
	(when (< (length result-array) (+ i frame-length))
	  (adjust-array result-array (+ i frame-length))
	  (adjust-array c-array (+ i frame-length)))
	(add-to-array (apply-window tmp-array :hann) 
		      result-array :offset i)
	(add-to-array (hann frame-length) 
		      c-array :offset i)
	(incf i hop-size))
      (unless (zerop i)
	(iter (for i from (/ frame-length 2) below (1- (length result-array)))
	      (setf (aref result-array i)
		    (/ (aref result-array i)
		       (aref c-array i))))
	(subseq result-array (/ frame-length 2) (- (length result-array)
						   (/ frame-length 2)))))))