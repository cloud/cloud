;;; -*- Mode: Lisp; Package: CLOUD -*-

;;;  (c) copyright 2008 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(defpackage :cloud
  (:use :cl :cl-user :iterate))

(in-package :cloud)

(defun help (fun)
  "Gives documentation on a function."
  (documentation fun 'function))
