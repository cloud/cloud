;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2004-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.clicks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   A sinusoidal sound model (partials actually)
;;;


(defparameter *extrapolation-order* 4)

(defparameter *max-known-peaks* 20)

(defparameter *freq-range* 1) ; percent

(defparameter *min-track-amplitude* 1.0d-3)



(defparameter *min-track-length* 0.2)
(defparameter *max-ghost-length* 0.05)

(defparameter *min-track-samples* 0)
(defparameter *max-ghost-samples* 0)

(let ((sampling-rate 44100))
  (defun adjust-samples (sample-rate)
    (when (/= sample-rate sampling-rate)
      (setf sampling-rate sample-rate
	    *min-track-samples*
	    (round (* *min-track-length*
		      sampling-rate))
	    *max-ghost-samples*
	    (round (* *max-ghost-length*
		      sampling-rate))))))
  



(defun mirror (a b)
  (- (* 2 a) b))


;;; Phase unwrapping, according to MacAulay & Quatieri


(defun M (theta0 theta1 f0 f1 increment-in-s)
  (round (/ (+ theta0 
               (- theta1) 
               (* (+ (* 2 pi f0)
                     (* 2 pi f1)) 
                  1/2 
                  increment-in-s)) 
            (* 2 pi))))


(defun theta (t-in-s theta0 theta1 f0 f1 increment-in-s)
  (let* ((c1 (+ (- theta1 
		   theta0 
		   (* 2 pi f0 increment-in-s)) 
		(* 2 pi (M theta0 
			   theta1 
			   f0
			   f1 
			   increment-in-s))))
	 (c2 (* 2 pi (- f1 f0)))
	 (alpha (- (/ (* 3 c1) 
		      (expt increment-in-s 2)) 
		   (/ c2 increment-in-s)))
	 (beta (+ (/ (* -2 c1) 
		     (expt increment-in-s 3)) 
		  (/ c2 (expt increment-in-s 2))))
	 (n t-in-s))
    (+ theta0 
       (* 2 pi f0 n) 
       (* alpha n n) 
       (* beta n n n))))






;;; Future peak estimations


(defun mirror-estimate (track)
  "Estimate next peak in track using a simple mirroring estimation."
  (if (> (track-length track) 1)
      (make-peak
       :frequency (mirror (peak-frequency (newest-peak track) (main-channel track))
			  (peak-frequency (nth-peak 1
						    track
						    :from-end t)
					  (main-channel track)))
       :amplitude (mirror (peak-amplitude (newest-peak track) (main-channel track))
			  (peak-amplitude (nth-peak 1
						    track
						    :from-end t)
					  (main-channel track))))
      (make-peak
       :frequency (peak-frequency (newest-peak track) (main-channel track))
       :amplitude (peak-amplitude (newest-peak track) (main-channel track)))))


(defun burg-estimate (track order)
  "Estimate next peak in track using the burg lpc method."
  (let ((frequency (burg-extrapolate-1
		    (strip-peaks track
				 (min (track-length track)
				      *max-known-peaks*)
				 :key #'(lambda (x) 
					  (peak-frequency x (main-channel track)))
				 :from-end t)
		    order)))
    (make-peak :frequency
	       frequency
	       :amplitude
	       (burg-extrapolate-1
		(strip-peaks track
				  (min (track-length track)
				       *max-known-peaks*)
				  :key #'(lambda (x)
					   (peak-amplitude x (main-channel track)))
				  :from-end t)
		order)
	       :phase (+ (peak-phase (newest-peak track) (main-channel track))
			 frequency))))

(defun estimate-next-peak (track)
  "Makes a guess of what next peak will be, based on the peak
collection of the track."
  (if (<= (track-length track) *extrapolation-order*)
      (mirror-estimate track)
      (burg-estimate track *extrapolation-order*)))





;;; Peak matching

(defun select-peaks-within-range (peak-frame ref delta
				  &key (key #'identity))
  (remove-if #'(lambda (peak) (> (abs (- (funcall key peak)
					 ref))
				 delta))
	     peak-frame))
  

(defun find-next-peak-for (track peak-frame)
  (let* ((successor (estimate-next-peak track))
	 (ref-freq (peak-frequency successor))
	 (delta-freq (* (/ *freq-range* 100) ref-freq))
	 (peaks-within-range (select-peaks-within-range 
			      peak-frame
			      ref-freq
			      delta-freq
			      :key #'peak-frequency))
	 (existing-peak
	  (iterate 
	    (for p in peaks-within-range)
	    (finding p minimizing 
		     (abs (- (log (peak-amplitude p) 10)
			     (log (peak-amplitude successor) 10)))))))
    (when existing-peak
      (unless (> 2 (/ (peak-amplitude existing-peak) 
		      (peak-amplitude (newest-peak track)))
		 0.5)
	(setf existing-peak nil)))
    (values existing-peak (or existing-peak successor))))

(defun find-next-stereo-peak-for (track peak-frame)
  (let* ((successor (estimate-next-peak track))
	 (ref-freq (peak-frequency successor))
	 (delta-freq (* (/ *freq-range* 100) ref-freq))
	 (peaks-within-range (select-peaks-within-range 
			      peak-frame
			      ref-freq
			      delta-freq
			      :key #'(lambda (x) 
				       (peak-frequency x (main-channel track)))))
	 (existing-peak
	  (iterate 
	    (for p in peaks-within-range)
	    (finding p minimizing 
		     (abs (- (log (peak-amplitude p (main-channel track)) 10)
			     (log (peak-amplitude successor (main-channel track)) 10)))))))
    (when existing-peak
      (unless (> 2 (/ (peak-amplitude existing-peak (main-channel track)) 
		      (peak-amplitude (newest-peak track) (main-channel track)))
		 0.5)
	(setf existing-peak nil)))
    (values existing-peak (or existing-peak successor))))




;;; Partial state manipulations

(defun revive (track successor)
  (append-peak successor track)
  (let* ((new-peaks-nb (1+ (ghost-peak-nb track)))
	 (last-valid-phase 
	  (peak-phase (nth-peak new-peaks-nb
				track
				:from-end t)))
	 (last-valid-frequency
	  (peak-frequency (nth-peak new-peaks-nb
				    track
				    :from-end t))))

    (setf (left-amp track) (* (left-amp track) (- (track-length track) new-peaks-nb))
	  (right-amp track) (* (right-amp track) (- (track-length track) new-peaks-nb)))
    
    (iterate (for i from new-peaks-nb downto 1) ; this is a mistake !
	     (for j from 0) ; we should not recompute all the track every time !
	     (incf (left-amp track) (peak-amplitude (nth-peak j track :from-end t) 0))
	     (incf (right-amp track) (peak-amplitude (nth-peak j track :from-end t) 1))
	     (setf (peak-phase (nth-peak j track :from-end t))
		   (theta (/ i (sample-rate track))
			  last-valid-phase
			  (peak-phase successor)
			  last-valid-frequency
			  (peak-frequency successor)
			  (/ new-peaks-nb (sample-rate track))))))
  (setf (state track) :alive)
  (setf (left-amp track) (/ (left-amp track) (track-length track))
	(right-amp track) (/ (right-amp track) (track-length track)))
  (if (> (right-amp track) (left-amp track))
      (setf (main-channel track) 1)
      (setf (main-channel track) 0))
  (validate-ghost-peaks track))
  
(defun judgement-day (track successor &key (kill-them-all nil))
  (adjust-samples (sample-rate track))
  (setf (state track) :zombie)
  (if (or (> (ghost-peak-nb track) *max-ghost-samples*)
	  kill-them-all)
      (progn ;; it's time to die
	(iterate (for i from 1 to (ghost-peak-nb track))
		 (remove-last-peak track))

	(if (and (> (track-length track) *min-track-samples*)
		 (find-if #'(lambda (p) (> (peak-amplitude p (main-channel track)) *min-track-amplitude*))
			  (peak-list track)))
	    (progn ;; you can go to paradise
	      (setf (date-of-death track)
		    (+ (date-of-birth track)
		       (track-length track)))
	      (setf (state track) :embalmed))
	    ;; you go to hell... mwahahaha
	    (setf (state track) :rotten)))
	(append-peak successor track :ghost t)))

(defun keep-tracks (track-list s-sound &key state)
  "Keeps tracks from track-list. If state is specified, keeps
only the the tracks in that state."
    (iterate (for track in track-list)
	     (when (or (not state)
		       (eq (state track) state))
	       (add-track track s-sound))))



;;; Partial tracking


(defgeneric track-partials (sound 
			    &key hop-size
			    frame-length
			    window-type)
  (:method (sound 
	    &key hop-size
	    frame-length
	    window-type)
    (declare (ignore hop-size frame-length window-type))
    (format nil "~a type objects cannot be tracked." (type-of sound)))
  (:documentation "Track partials to shift to the sinusoidal model"))

(defmethod track-partials ((pcm-instance sound) 
			   &key (hop-size 512)
			   (frame-length 2048)
			   (window-type :hann))
  (track-partials (short-time-spectral-analysis 
		   pcm-instance
		   :hop-size hop-size
		   :frame-length frame-length
		   :window-type window-type)))

(defmethod track-partials ((p-sound peak-sound) 
			   &key hop-size
			   frame-length
			   window-type)
  (declare (ignore hop-size frame-length window-type))
  (let ((track-list nil)
	(current-date 0)
	(s-sound (create-sin-sound)))
    (with-elements-from-peak-sound p-sound peak-frame
	;; Selects next peaks from the list for each partial
	;; and remove the peaks from the peak list.
	(iterate (for track in track-list)
		 (multiple-value-bind (foundp successor)
		     (find-next-peak-for track peak-frame)
		   (if foundp
		       (progn
			 (revive track successor)
			 (setf peak-frame (delete successor peak-frame)))
		       (judgement-day track successor))))
	
	;; Deal with remaining peaks.
	(iterate (for peak in peak-frame)
		 (push (create-track :sample-rate 
				     (sample-rate p-sound)
				     :date-of-birth current-date)
		       track-list)
		 (append-peak peak (first track-list)))
	
	;; Refresh the track-list.
	(keep-tracks track-list s-sound :state :embalmed)
	(setf track-list (delete-if #'deadp track-list))
	
	
	;; Sort the remaining tracks.
	(setf track-list (sort track-list #'>
			       :key #'(lambda (track)
					(peak-amplitude (newest-peak track)))))
	(incf current-date))
      ;; Kill the remaining tracks
      (iterate (for track in track-list)
	       (judgement-day track nil :kill-them-all t))
      (keep-tracks track-list s-sound :state :embalmed)
      (setf track-list (delete-if #'deadp track-list))
      (setf (nb-channels s-sound) (nb-channels p-sound)
	    (sample-rate s-sound) (sample-rate p-sound))
      (let ((env (envelope p-sound :sample-rate (sample-rate s-sound))))
	(setf (envelope s-sound) env))
      s-sound))

(defun differentiator (order)
  (let ((right (v-fun #'(lambda (x) (* (expt -1 x) (/ x))) (v-make 1 (floor (/ order 2))))))
    (v* (concatenate 'vector (v-fun #'- (reverse right)) #(0) right) (subseq (hann (1+ order)) 1))))

(defun differentiate (array order)
  (let ((diffor (differentiator order)))
    (subseq (convolution diffor array) 0 (length array))))



(defun left-channel (stereo-array)
  (subarray stereo-array nil 0))

(defun right-channel (stereo-array)
  (subarray stereo-array nil 1))

(defun complexify (array)
  (v-fun #'complex array '(complex double-float)))

(defun stereo-track-partials (stereo-array &key (frame-length 1024) (hopsize 256) (sample-rate 44100))
  (let* (;(diff-array (differentiate array (1- frame-length)))
	 (left-array (complexify (left-channel stereo-array)))
	 (right-array (complexify (right-channel stereo-array)))
	 (left-diff-array (diff left-array :pad t))
	 (right-diff-array (diff right-array :pad t))
	 peak-frame 
	 (track-list nil)
	 (current-date 0)
	 (s-sound (create-sin-sound))
	 (new-sample-rate (/ sample-rate hopsize))
	 (left-frame (make-array frame-length
				 :element-type '(complex double-float)))
	 (right-frame (make-array frame-length
				  :element-type '(complex double-float)))
	 (left-diff-frame (make-array frame-length
				      :element-type '(complex double-float)))
	 (right-diff-frame (make-array frame-length
				       :element-type '(complex double-float)))
	 (left-spectrum (make-array frame-length 
				    :element-type '(complex double-float)))
	 (right-spectrum (make-array frame-length 
				     :element-type '(complex double-float)))
	 (left-diff-spectrum (make-array frame-length 
					 :element-type '(complex double-float)))
	 (right-diff-spectrum (make-array frame-length 
					  :element-type '(complex double-float))))
    
    (iter (for i from 0 to (- (length left-array) frame-length) by hopsize)
;	  (print current-date)
	  ;; (setf peak-frame (peak-extraction-without-envelope 
;; 			    complex-array 
;; 			    i
;; 			    :frame-length frame-length 
;; 			    :window-type :hann
;; 			    :sample-rate sample-rate))

	  (apply-window! (make-array frame-length
				     :displaced-to left-array
				     :element-type '(complex double-float)
				     :displaced-index-offset i)
			 left-frame
			 :hann)
	  (apply-window! (make-array frame-length
				     :displaced-to right-array
				     :element-type '(complex double-float)
				     :displaced-index-offset i)
			 right-frame
			 :hann)
	  (apply-window! (make-array frame-length
				     :displaced-to left-diff-array
				     :element-type '(complex double-float)
				     :displaced-index-offset i)
			 left-diff-frame
			 :hann)
	  (apply-window! (make-array frame-length
				     :displaced-to right-diff-array
				     :element-type '(complex double-float)
				     :displaced-index-offset i)
			 right-diff-frame
			 :hann)
	  (window-sum :hann frame-length)
	  (setf peak-frame (stereo-mdc-compute-peak-frame 
;			    (adaptive-noise-gate (transform! in-array spectrum))
			    (n-noise-gate (transform! left-frame left-spectrum)
					  *spectrum-noise-threshold*)
			    (transform! left-diff-frame left-diff-spectrum)

			    (n-noise-gate (transform! right-frame right-spectrum)
					  *spectrum-noise-threshold*)
			    (transform! right-diff-frame right-diff-spectrum)
			    
			    :sample-rate sample-rate
			    :frame-length frame-length
			    :window-type :hann))

	  
	  ;; Selects next peaks from the list for each partial
	  ;; and remove the peaks from the peak list.
	  (iterate (for track in track-list)
		   (multiple-value-bind (foundp successor)
		       (find-next-stereo-peak-for track peak-frame)
		     (if foundp
			 (progn
			   (revive track successor)
			   (setf peak-frame (delete successor peak-frame)))
			 (judgement-day track successor))))
	  
	  ;; Deal with remaining peaks.
	  (iterate (for peak in peak-frame)
		   (push (create-track :sample-rate new-sample-rate
				       :date-of-birth current-date)
			 track-list)
		   (append-peak peak (first track-list)))
	  
	  ;; Refresh the track-list.
	  (keep-tracks track-list s-sound :state :embalmed)
	  (setf track-list (delete-if #'deadp track-list))
	  
	  
	  ;; Sort the remaining tracks.
	  (setf track-list (sort track-list #'>
				 :key #'(lambda (track)
					  (peak-amplitude (newest-peak track)))))
	  (incf current-date))
    ;; Kill the remaining tracks
    (iterate (for track in track-list)
	     (judgement-day track nil :kill-them-all t))
    (keep-tracks track-list s-sound :state :embalmed)
    (setf track-list (delete-if #'deadp track-list))
    (setf (nb-channels s-sound) 2)

    (setf (sample-rate s-sound) new-sample-rate)
    ;;(let ((env (envelope p-sound :sample-rate (sample-rate s-sound))))
    ;;  (setf (envelope s-sound) env))
    s-sound))
			      
    