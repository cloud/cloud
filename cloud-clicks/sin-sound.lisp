;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2007-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.clicks)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Sounds made of partials.
;;;

(defclass sin-sound (sound)
  ((data-file :initarg :data-file :accessor data-file)
   (envelope-file :initarg :envelope-file :accessor envelope-file)))

(defun create-sin-sound (&key (channels 1) (sample-rate +default-sample-rate+))
  (let ((sinname (unique-filename "test" "parts" :path "tmp"))
	(envelname (unique-filename "test" "envel" :path "tmp")))
    (touch-file sinname)
    (touch-file envelname)
    (make-instance 'sin-sound
     :nb-channels channels
     :sample-rate sample-rate
     :envelope-file envelname
     :data-file sinname)))

(defun store-partial (partial s-sound)
  (with-open-file (s (data-file s-sound)
		     :if-exists :append
		     :if-does-not-exist :create
		     :direction :output)
    (format s "~s~%" partial)))

(defun add-track (track s-sound)
  (close-track track)
  (store-partial track s-sound))


(defmacro with-elements-from-sin-sound ((s-sound partial-iterator)
					 &body body)
  (with-gensyms (s)
    `(let ((,partial-iterator nil))
       (with-open-file (,s (data-file ,s-sound) 
			  :direction :input
			  :if-does-not-exist nil)
	 (iterate
	   (while (not (eq (peek-char nil ,s nil 'EOF nil) 'EOF)))
	   (setf ,partial-iterator (read ,s nil nil))
	     ,@body)))))

(defun nth-partial (n s-sound)
  (let (p)
    (with-open-file (s (data-file s-sound)
		       :direction :input
		       :if-does-not-exist nil)
      (iterate (for i from 0 to n)
	       (setf p (read s nil nil))))
    p))

(defmethod export-to-disk ((sin-instance sin-sound) filename)
  "Saves the partials on disk in standard format (not in a lisp way)."
  (with-open-file (f (merge-pathnames filename)
		     :direction :output
		     :if-exists :supersede
		     :if-does-not-exist :create)
    (with-elements-from-sin-sound (sin-instance partial)
      (iterate (for peak in (peak-list partial))
	       (for date from (date-of-birth partial))
	       (format f "~A ~,,,,,,'eE ~,,,,,,'eE ~,,,,,,'eE ~,,,,,,'eE~%"
		       date
		       (peak-frequency peak)
		       (peak-amplitude peak)
		       (peak-phase peak)
		       (peak-position peak)))
      (format f "~%"))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    Envelope.
;;;

(defun real-envelope (sound &key (sample-rate +default-sample-rate+))
  (when (envelope-file sound)
    (map 'vector #'realpart (envelope sound :sample-rate
				      sample-rate))))

(defmethod envelope ((s-sound sin-sound)
		     &key (sample-rate +default-sample-rate+))
  (when (envelope-file s-sound)
    (let ((env (with-open-file (s (envelope-file s-sound) :direction :input)
		 (read s  nil nil))))
      (when env
	(if (= (sample-rate s-sound)
	       sample-rate)
	    env
	    (resample env (sample-rate s-sound)
		      sample-rate :centering-fun #'poly-centering))))))

(defun (setf envelope) (envelope s-sound)
  (when envelope
    (with-open-file (s (envelope-file s-sound)
		       :if-exists :supersede
		       :if-does-not-exist :create
		       :direction :output)
      (format s "~s~%" envelope))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    Synthesis.
;;;


(defgeneric synthesize (source-sound
			&optional destination-sample-rate)
  (:method (source-sound
	    &optional destination-sample-rate)
    (declare (ignore destination-sample-rate))
    "Synthesis for these kinds of objects is not implemented.")
  (:documentation "Synthesizes a sound into temporal model."))

(defmethod synthesize ((s-sound sin-sound)
		       &optional (destination-sample-rate
		       +default-sample-rate+))
  (let* ((envelope (real-envelope s-sound :sample-rate destination-sample-rate))
	 (i 0)
	 (samples (make-array (length envelope)
		   :initial-element 0.0d0
		   :adjustable t)))
    (with-elements-from-sin-sound (s-sound p)
      (format t "synthesizing partial ~a~%" (incf i))
      (setf samples (mix-partial-into-array p samples
		                            :destination-sample-rate
					    destination-sample-rate)))
    (if (>= (length envelope)
	    (length samples))
	(v+ samples
	    envelope)
	samples)))
;;   (let ((k 0))
;;     (with-elements-from-sin-sound (s-sound p)
;;       (format t "Synthesizing partial ~a (length: ~a)~%" (incf k) (length (peak-list p)))
;;       (accumulate p by #'mix-partial-into-pcm 
;; 		  initial-value
;; 		  (make-instance 'pcm-sound
;; 				 ;; Arbitrarily set to 1 channel for now.
;; 				 ;; We should check if the positions of the peaks are
;; 				 ;; non null.
;; 				 :channels 1 
;; 				 :sample-rate destination-sample-rate
;; 				 :samples (make-array 0
;; 						      :initial-element 0.0d0
;; 						      :adjustable t))))))

(defun synthesize-to-pcm (sound &optional
			  (destination-sample-rate
			  +default-sample-rate+))
  (make-instance 'pcm-sound
		 :channels 1
		 :sample-rate destination-sample-rate
		 :samples (synthesize sound destination-sample-rate)))


(defmethod cloud-grapher:make-plottable ((thing sin-sound))
  (with-elements-from-sin-sound (thing p)
    (for j from 1)
    (collect (list p :label (format nil "~a" j)))))