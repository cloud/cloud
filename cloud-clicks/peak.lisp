;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2005-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :cloud.clicks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Implements functions to handle sounds as made of spectral
;;;   peaks only.

(deftype peak () '(simple-array double-float (4 2)))

(declaim (inline peak-frequency
		 peak-amplitude
		 peak-phase
		 peak-position
		 (setf peak-frequency)
		 (setf peak-amplitude)
		 (setf peak-phase)
		 (setf peak-position)
		 double))

(defun peak-frequency (peak &optional (channel 0))
  (aref peak 0 channel))
(defun peak-amplitude (peak &optional (channel 0))
  (aref peak 1 channel))
(defun peak-phase (peak &optional (channel 0))
  (aref peak 2 channel))
(defun peak-position (peak)
  (aref peak 3 0))

(defun (setf peak-frequency) (val peak &optional (channel 0))
  (setf (aref peak 0 channel) val))
(defun (setf peak-amplitude) (val peak &optional (channel 0))
  (setf (aref peak 1 channel) val))
(defun (setf peak-phase) (val peak &optional (channel 0))
  (setf (aref peak 2 channel) val))
(defun (setf peak-position) (val peak)
  (setf (aref peak 3 0) val))

(defun double (x) 
  (if (atom x) 
      (list x x) 
      x))

(defun make-peak (&key 
		  (frequency 0.0d0) 
		  (amplitude 0.0d0) 
		  (phase 0.0d0) 
		  (position 0.0d0))
  (make-array '(4 2) 
	      :element-type 'double-float 
	      :initial-contents (list (double frequency)
				      (double amplitude)
				      (double phase)
				      (double position))))
