;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2006-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.clicks)




;;; Order 2

(defun frequencies (peak-list)
  (map 'vector #'peak-frequency peak-list))

(defun amplitudes (peak-list)
  (map 'vector #'peak-amplitude peak-list))

(defun phases (peak-list)
  (map 'vector #'peak-phase peak-list))

(defmethod short-time-spectral-analysis ((p partial) &key
					 (window-type :hann)
					 (frame-length 64)
					 (hop-size 1)
					 (with-envelope t)
					 (iterations 10))
  (let ((f-psound (create-peak-sound))
	(a-psound (create-peak-sound))
	(p-psound (create-peak-sound))
	(f-data (map '(vector (complex double-float)) 
		     #'(lambda (x)
			 (complex x 0.0d0))
		     (extrapolate-array (frequencies (peak-list p))
		      :before (1+ (/ frame-length 2))
		      :after (/ frame-length 2))))
	(a-data (pad-array (map '(vector (complex double-float)) 
				#'(lambda (x)
				    (complex (peak-amplitude x) 0.0d0))
				(peak-list p))
			   :before (1+ (/ frame-length 2))
			   :after (/ frame-length 2)))
	(p-data (map '(vector (complex double-float)) 
		     #'(lambda (x)
			 (complex x 0.0d0))
		     (extrapolate-array (phases (peak-list p))
		      :before (1+ (/ frame-length 2))
		      :after (/ frame-length 2)))))
    
    (setf (channels f-psound) 1
	  (sample-rate f-psound) (/ (sample-rate p) hop-size)
	  (channels a-psound) 1
	  (sample-rate a-psound) (/ (sample-rate p) hop-size)
	  (channels p-psound) 1
	  (sample-rate p-psound) (/ (sample-rate p) hop-size))
    
    
    (with-peak-sound (fp-output fe-output f-psound)
      (iterate (for index index-of-vector f-data from 1
		    to (- (length f-data) frame-length)
		    by hop-size)
	       (if with-envelope
		   (multiple-value-bind (peaks envelope)
		       (peak-extraction-with-envelope 
			f-data 
			index
			:iterations iterations
			:frame-length frame-length
			:window-type window-type
			:sample-rate (sample-rate p))
		     (store-envelope fe-output envelope)
		     (store-peaks fp-output peaks))
		   (store-peaks
		    fp-output
		    (peak-extraction-without-envelope 
		     f-data 
		     index 
		     :frame-length frame-length 
		     :window-type window-type
		     :sample-rate (sample-rate p))))))
    (with-peak-sound (ap-output ae-output a-psound)
      (iterate (for index index-of-vector a-data from 1
		    to (- (length a-data) frame-length)
		    by hop-size)
	       (if with-envelope
		   (multiple-value-bind (peaks envelope)
		       (peak-extraction-with-envelope 
			a-data 
			index
			:iterations iterations
			:frame-length frame-length
			:window-type window-type
			:sample-rate (sample-rate p))
		     (store-envelope ae-output envelope)
		     (store-peaks ap-output peaks))
		   (store-peaks
		    ap-output
		    (peak-extraction-without-envelope 
		     a-data
		     index 
		     :frame-length frame-length 
		     :window-type window-type
		     :sample-rate (sample-rate p))))))
    (with-peak-sound (pp-output pe-output p-psound)
      (iterate (for index index-of-vector p-data from 1
		    to (- (length p-data) frame-length)
		    by hop-size)
	       (if with-envelope
		   (multiple-value-bind (peaks envelope)
		       (peak-extraction-with-envelope 
			p-data 
			index
			:iterations iterations
			:frame-length frame-length
			:window-type window-type
			:sample-rate (sample-rate p))
		     (store-envelope pe-output envelope)
		     (store-peaks pp-output peaks))
		   (store-peaks
		    pp-output
		    (peak-extraction-without-envelope
		     p-data 
		     index 
		     :frame-length frame-length 
		     :window-type window-type
		     :sample-rate (sample-rate p))))))
    (values f-psound
	    a-psound
	    p-psound)))


(defmethod track-partials ((p partial) 
			   &key (hop-size 1)
			   (frame-length 64)
			   (window-type :hann)
			   (iterations 10))
  (let (s-sound)
    (values-list
     (iterate (for i in (multiple-value-list
			 (short-time-spectral-analysis 
			  p
			  :iterations iterations
			  :hop-size hop-size
			  :frame-length frame-length
			  :window-type window-type
			  :with-envelope t)))
	      (setf s-sound (track-partials i
			     :hop-size hop-size
			     :frame-length frame-length
			     :window-type window-type))
	      (setf (envelope s-sound)
		    (envelope i :sample-rate (sample-rate s-sound)))
	      (collect s-sound)))))


(defun order2 (s-sound)
  (let ((k 0))
    (with-elements-from-sin-sound (s-sound p)
      (format t "tracking partials in partial ~a at ~a Hz.~%" (incf k)
	      (peak-frequency (first (peak-list p))))
      (collect (multiple-value-list 
		(track-partials p 
				:hop-size 1
				:frame-length 64))))))


(defun pouf ()
  (setf sax (load-sound-file "saxo.wav")
	saxp (track-partials sax)
	sax30 (nth-partial 31 saxp)
	sax30p (multiple-value-list (track-partials sax30))))


(defun stretch-order-0 (pcm-sound factor)
  (make-instance 'pcm-sound
		 :channels (channels pcm-sound)
		 :sample-rate (sample-rate pcm-sound)
		 :samples (resample (samples pcm-sound)
				    (sample-rate pcm-sound)
				    (* (sample-rate pcm-sound)
				       factor))))

(defun stretch-partial-order-0 (partial factor)
  (multiple-value-bind (dob offset)
      (ceiling (date-of-birth partial) (/ factor))
    (setf offset (- offset))
    (let* ((sr (sample-rate partial))
	   (rfrequencies (resample (frequencies (peak-list partial)) 
				   sr (* sr factor)
			  :centering-fun #'poly-centering
			  :offset offset))
	   (ramplitudes (resample (amplitudes (peak-list partial))
				  sr (* sr factor)
			 :centering-fun #'poly-centering
			 :offset offset))
	   (rphases (resample (phases (peak-list partial))
			      sr (* sr factor)
		     :centering-fun #'poly-centering
		     :offset offset))
	   (res-partial (make-instance 'partial 
			 :sample-rate sr
			 :peak-list nil
			 :date-of-birth dob)))
      (iter (for f in-vector rfrequencies)
	    (for a in-vector ramplitudes)
	    (for p in-vector rphases)
	    (append-peak (make-peak :frequency f
				    :amplitude a
				    :phase (* p factor))
			 res-partial))
      (setf (date-of-death res-partial) (+ dob (track-length res-partial)))
      res-partial)))
		 

(defun stretch-order-1 (sin-sound factor)
  (let ((s (create-sin-sound :channels (channels sin-sound)
			     :sample-rate (sample-rate sin-sound)))
	(env (envelope sin-sound :sample-rate (sample-rate sin-sound))))
    (with-elements-from-sin-sound (sin-sound p)
      (for i from 0)
      (format t "stretching partial \#~a at order 1.~%" i)
      (add-track (stretch-partial-order-0 p factor) s))
    (when env
      (setf (envelope s) (resample env 1 (/ factor)
				  :centering-fun #'poly-centering)))
    s))
    
(defun stretch-partial-order-1 (partial factor)
  (multiple-value-bind (dob offset)
      (ceiling (date-of-birth partial) (/ factor))
    (setf offset (- offset))
    (print "go")
    (multiple-value-bind (frequencies amplitudes phases)
	(track-partials partial)
      (let (rfrequencies ramplitudes rphases res-partial)
	(print "freqs")
	(setf rfrequencies (synthesize (stretch-order-1 frequencies factor)
				       (sample-rate partial)))
	(print "amps")
	(setf ramplitudes (synthesize (stretch-order-1 amplitudes factor)
				      (sample-rate partial)))
	(print "phases")
	(setf rphases (synthesize (stretch-order-1 phases factor)
				  (sample-rate partial)))
	(setf res-partial (make-instance 'partial
			   :sample-rate (sample-rate partial)
			   :peak-list nil
			   :date-of-birth dob))
	(iter (for f in-vector rfrequencies)
	      (for a in-vector ramplitudes)
	      (for p in-vector rphases)
	      (append-peak (make-peak :frequency f
				      :amplitude a
				      :phase (* p factor))
			   res-partial))
	(setf (date-of-death res-partial) (+ dob (track-length res-partial)))
	res-partial))))


(defun stretch-order-2 (sin-sound factor)
  (let ((s (create-sin-sound :channels (channels sin-sound)
			     :sample-rate (sample-rate sin-sound))))
    (with-elements-from-sin-sound (sin-sound p)
      (for i from 0)
      (format t "stretching partial \#~a at order 2.~%" i)
      (add-track (stretch-partial-order-1 p factor) s))
    s))

(defun stretch (pcm-sound factor order)
  (case order
    (0 (stretch-order-0 pcm-sound factor))
    (1 (synthesize-to-pcm
	(stretch-order-1 (track-partials pcm-sound)
			 factor)))
    (2 (synthesize-to-pcm
	(stretch-order-2 (track-partials pcm-sound)
			 factor)))))
