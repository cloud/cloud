;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2005-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :cloud.clicks)

(defun half (in-array)
  (make-array (/ (length in-array) 2)
	      :element-type (array-element-type in-array)
	      :displaced-to in-array))

(defun second-half (in-array)
  (make-array (/ (length in-array) 2)
	      :element-type (array-element-type in-array)
	      :displaced-to in-array
	      :displaced-index-offset (/ (length in-array) 2)))



(defconstant +spectrum-noise-threshold+ 1.0d-4)
(defparameter *spectrum-noise-threshold* 1.0d-4)

;; doesn't cut the array in two !!!
(defun transform! (in-array out-array)
  (half (fft! (swap-array in-array)
	      out-array)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Marchand - Desainte-Catherine method for estimating peak
;;;   frequencies and amplitudes.
;;;

(declaim (inline omega))
(defun omega (f delta-t)
  (* 2 pi f delta-t))

(defun lobe-fun (omega frame-length)
  (if (zerop omega)
      frame-length
      (/ (sin (* omega 
		 0.5d0 
		 frame-length))
	 (sin (/ omega 2.0d0)))))
 
(defun hann-lobe-fun (omega frame-length)
  (let ((factor (/ (* 2.0d0 
		      pi) 
		   frame-length)))
    (/ (abs (+ (* 0.5d0
		  (lobe-fun omega 
			    frame-length))
	       (* 0.25d0 
		  (lobe-fun (- omega 
			       factor) 
			    frame-length))
	       (* 0.25d0  
		  (lobe-fun (+ omega 
			       factor)
			    frame-length))))
       frame-length)))

;;; Not multithread resistant !
(let ((window-sum 0)
      (window-type nil)
      (frame-length 0))

  (defun window-sum (w-type f-length)
    (unless (and (eq w-type window-type)
		 (= f-length frame-length))
      (setf window-type w-type
	    frame-length f-length
	    window-sum (sum (make-window window-type 
					 frame-length))
	    *spectrum-noise-threshold*
	    (* +spectrum-noise-threshold+ window-sum)))
    window-sum))

(defun mdc-compute-peak-frame (spectrum diff-spectrum 
			       &key (sample-rate 44100)
			       (frame-length 2048)
			       (window-type :hann))
  (let ((delta-t (/ sample-rate))
	(frequency 0)
	(val 0))
    (with-funned-local-maxima-positions pos spectrum #'abs
      (setf val (abs (/ (aref diff-spectrum pos)
			(* 2 (aref spectrum pos)))))
      (when (and (<= val 1)
		 (not (zerop (abs (aref diff-spectrum pos)))))
	(collect (make-peak
 		  :frequency
 		  (setf frequency 
 			(/ (asin val)
 			   (* pi delta-t)))
 		  :amplitude
 		  (/ (abs (aref spectrum pos))
 		     (window-sum window-type frame-length)
 		     (hann-lobe-fun
		      (omega (abs (- frequency
				     (/ pos
					delta-t
					frame-length)))
			     delta-t)
		      frame-length))
		  :phase
 		  (phase (aref spectrum pos))  
 		  :position 
 		  0.0d0) into peaks)
	(collect pos into poses))
      (finally (return (values peaks poses))))))
				     
(defun new-mdc-compute-peak-frame (spectrum diff-spectrum 
			       &key (sample-rate 44100)
			       (frame-length 2048)
			       (window-type :hann))
  (let ((delta-t (/ sample-rate))
	(frequency 0)
	(val 0))
    (with-funned-local-maxima-positions pos spectrum #'abs
      (setf val (abs (/ (aref diff-spectrum pos)
			(aref spectrum pos))))
      (setf frequency (/ val
			 (* 2 pi delta-t)))
      ;(print (list (* frequency frame-length delta-t) pos))
      (when (and (< (abs (- (* frequency frame-length delta-t) pos)) 20)
		 (> frequency 0))
	;(print (list frequency pos (* frequency frame-length delta-t)))
	(collect (make-peak
		  :frequency
		  frequency
		  :amplitude
		  (/ (abs (aref spectrum pos))
		     (window-sum window-type frame-length)
		     (hann-lobe-fun
		      (omega (abs (- frequency
				     (/ pos
					delta-t
					frame-length)))
			     delta-t)
		      frame-length))
		  :phase
		  (phase (aref spectrum pos))  
		  :position 
		  0.0d0))))))
  
(defun stereo-mdc-compute-peak-frame (left-spectrum left-diff-spectrum 
				      right-spectrum right-diff-spectrum 
				      &key (sample-rate 44100)
				      (frame-length 2048)
				      (window-type :hann))
  (let ((delta-t (/ sample-rate))
	(left-frequency 0)
	(right-frequency 0)
	(left-val 0)
	(right-val 0))
    (with-stereo-funned-local-maxima-positions pos left-spectrum right-spectrum #'abs
      (when (not (or (zerop (aref left-spectrum pos))
		     (zerop (aref right-spectrum pos))))
	(setf left-val (abs (/ (aref left-diff-spectrum pos)
			       (* 2 (aref left-spectrum pos)))))
	(setf right-val (abs (/ (aref right-diff-spectrum pos)
				(* 2 (aref right-spectrum pos)))))
	(when (and (<= left-val 1)
		   (<= right-val 1)
		   (not (zerop (abs (aref left-diff-spectrum pos))))
		   (not (zerop (abs (aref right-diff-spectrum pos)))))
	  (collect (make-peak
		    :frequency
		    (list
		     (setf left-frequency 
			   (/ (asin left-val)
			      (* pi delta-t)))
		     (setf right-frequency 
			   (/ (asin right-val)
			      (* pi delta-t))))
		    :amplitude
		    (list
		     (/ (abs (aref left-spectrum pos))
			(window-sum window-type frame-length)
			(hann-lobe-fun
			 (omega (abs (- left-frequency
					(/ pos
					   delta-t
					   frame-length)))
				delta-t)
			 frame-length))
		     (/ (abs (aref right-spectrum pos))
			(window-sum window-type frame-length)
			(hann-lobe-fun
			 (omega (abs (- right-frequency
					(/ pos
					   delta-t
					   frame-length)))
				delta-t)
			 frame-length)))
		    :phase
		    (list
		     (phase (aref left-spectrum pos))  
		     (phase (aref right-spectrum pos)))
		    :position 
		    0.0d0)))))))
  
(defun generate-sine (peak length 
		      &key (offset 0) 
		      (sample-rate 44100)
		      (element-type t))
  (map (list 'vector element-type) 
       #'(lambda (x)
	   (* (sin (+ (* 2 pi (peak-frequency peak) x
			 (/ sample-rate))
		      (peak-phase peak)))
		(peak-amplitude peak)))
       (v-make offset (1- length) :element-type element-type)))



(defun remove-sines-from-data (peaks s &key (offset 0) (sample-rate 44100))
  (let* ((l (length s))
	 (tmp-data (make-array l :element-type (array-element-type s))))
    (iter (for peak in peaks)
	  (setf tmp-data 
		(v+ tmp-data 
		    (generate-sine peak l
				   :offset offset
				   :sample-rate sample-rate
				   :element-type (array-element-type s)))))
    (v- s tmp-data)))
    
    
		      

(let ((array-length 0)
      in-array
      diff-in-array
      out-array
      diff-out-array)
  
  (defun initialize-arrays (frame-length)
    "In order to gain some efficiency, the arrays are initialized only
once for each frame length."
    (when (/= frame-length array-length)
      (setf in-array 
	    (make-array frame-length
			:element-type '(complex double-float))
	    diff-in-array 
	    (make-array frame-length
			:element-type '(complex double-float))
	    out-array 
	    (make-array frame-length
			:element-type '(complex double-float))
	    diff-out-array 
	    (make-array frame-length
			:element-type '(complex double-float)))))
	      
  (defun peak-extraction-without-envelope (data index
					   &key
					   (frame-length 2048)
					   (window-type :hann)
					   (sample-rate 44100))
    "Performs a spectral peaks extraction over a piece of signal."
    (initialize-arrays frame-length)
    (apply-window! (make-array frame-length
			       :displaced-to data
			       :element-type '(complex double-float)
			       :displaced-index-offset index)
		   in-array
		   window-type)
    (apply-window! (diff (make-array (1+ frame-length)
				     :displaced-to data
				     :element-type '(complex double-float)
				     :displaced-index-offset (1- index)))
		   diff-in-array
		   window-type)
    (window-sum window-type frame-length)
    (mdc-compute-peak-frame 
     ;(adaptive-noise-gate (transform! in-array out-array))
     (n-noise-gate (transform! in-array out-array)
		   *spectrum-noise-threshold*)
     (transform! diff-in-array diff-out-array)
     ;(n-noise-gate (transform! diff-in-array diff-out-array)
	;	   *spectrum-noise-threshold*)
     :sample-rate sample-rate
     :frame-length frame-length
     :window-type window-type))

  (defun peak-extraction-with-envelope (data index
					&key
					(frame-length 2048)
					(window-type :hann)
					(sample-rate 44100)
					(iterations 10)
					(order 3))
    "Performs a spectral peaks extraction over a piece of signal,
		 computing the envelope of that piece of signal."
    (initialize-arrays frame-length)
    (let ((data-piece (make-array (1+ frame-length)
				  :displaced-to data
				  :element-type '(complex double-float)
				  :displaced-index-offset (1- index)))
	  (sineless-data (make-array (1+ frame-length)
				  :displaced-to data
				  :element-type '(complex double-float)
				  :displaced-index-offset (1- index)))
	  polyless-data
	  peaks
	  poly-coeffs)
      (iter (for i from 1 to iterations)
	    (multiple-value-bind (approximation coeffs)
		(closest-polynomial sineless-data :order order)
	      (setf polyless-data (v- data-piece approximation)
		    poly-coeffs coeffs))
	    (apply-window! (make-array frame-length
				       :displaced-to polyless-data
				       :element-type '(complex double-float)
				       :displaced-index-offset 1)
			   in-array
			   window-type)

	    (apply-window! (diff polyless-data)
			   diff-in-array
			   window-type)

	    (window-sum window-type frame-length)	    
	    (setf peaks (mdc-compute-peak-frame 
			 (n-noise-gate (transform! in-array out-array)
				       *spectrum-noise-threshold*)
			 (transform! diff-in-array diff-out-array)
			 :sample-rate sample-rate
			 :frame-length frame-length
			 :window-type window-type))
	    (unless (= i iterations)
	      ;; remove the computed sines from the data
	      (setf sineless-data 
		    (remove-sines-from-data peaks 
					    data-piece
					    :sample-rate
					    sample-rate
					    :offset -1))))
      (values peaks poly-coeffs))))


(defgeneric short-time-spectral-analysis (snd 
					  &key
					  hop-size
					  frame-length
					  window-type
					  with-envelope)
  (:method (snd  
	    &key
	    hop-size
	    frame-length
	    window-type
	    with-envelope)
    (declare (ignore hop-size frame-length window-type))
    "This type of sound is not supported yet.")
  (:documentation "Performs a short-time Fourier spectral
  analysis of the sound."))
    

(defmethod short-time-spectral-analysis ((snd sound) 
					 &key
					 (hop-size 512)
					 (frame-length 2048)
					 (window-type :hann)
					 (with-envelope nil))
  (let ((p-sound (create-peak-sound
		  :nb-channels (nb-channels snd)
		  :sample-rate (/ (sample-rate snd) hop-size)))
	(data (pad-array (v-fun #'complex (channel snd 0) '(complex double-float))
			 :before (1+ (/ frame-length 2))
			 :after (/ frame-length 2))))
    
    
    (with-peak-sound (p-output e-output p-sound)
      (iterate (for index index-of-vector data from 1
		    to (- (length data) frame-length)
		    by hop-size)
	       (if with-envelope
		   (multiple-value-bind (peaks envelope)
		       (peak-extraction-with-envelope 
			data 
			index
			:frame-length frame-length
			:window-type window-type
			:sample-rate (sample-rate snd))
		     (store-envelope e-output envelope)
		     (store-peaks p-output peaks))
		   (store-peaks
		    p-output
		    (peak-extraction-without-envelope 
		     data 
		     index 
		     :frame-length frame-length 
		     :window-type window-type
		     :sample-rate (sample-rate snd))))))
    p-sound))

