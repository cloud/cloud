;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2007-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.clicks)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Partials internals and handling
;;;

(deftype track-state ()
  '(member :alive :zombie :embalmed :rotten))

(deftype peak-state ()
  '(member :ghost))

(defclass partial ()
  ((sample-rate :initform nil
		:initarg :sample-rate
		:accessor sample-rate)
   (peak-list :initform nil
	      :initarg :peak-list
	      :accessor peak-list)
   (date-of-birth :initform 0
		  :initarg :date-of-birth
		  :accessor date-of-birth)
   (date-of-death :initform 0
		  :initarg :date-of-death
		  :accessor date-of-death)
   (main-channel :initform 0
		 :initarg :main-channel
		 :accessor main-channel)))

(defclass track (partial)
  ((state :initarg :state
	  :initform :alive
	  :accessor state)
   (ghost-peaks :initarg :ghost-peaks
		:initform 0
		:accessor ghost-peaks)
   
   (left-amp :initarg :left-amp
	     :initform 0
	     :accessor left-amp)
   (right-amp :initarg :right-amp
	      :initform 0
	      :accessor right-amp)))


(defmethod print-object ((o partial) stream) 
  (format stream "#T~s" (list 
			 (peak-list o)
			 (sample-rate o)
			 (date-of-birth o)
			 (date-of-death o)
			 (main-channel o))))

(set-dispatch-macro-character 
 #\# #\T
 #'(lambda(stream subchar arg)
     (declare (ignore arg subchar))
     (let ((track (read stream nil (values) t)))  
       (when (and (consp track) (= (length track) 5))
	 (setf track (make-instance 'partial
				    :peak-list (first track)
				    :sample-rate (second track)
				    :date-of-birth (third track)
				    :date-of-death (fourth track)
				    :main-channel (fifth track))))
       track)))

(defun create-track (&key (sample-rate +default-sample-rate+)
		     (date-of-birth 0))
  (make-instance 'track
		 :sample-rate sample-rate
		 :date-of-birth date-of-birth))

(defun close-track (track)
  (setf (peak-list track)
	(nreverse (peak-list track)))
  (change-class track 'partial))


(defun deadp (track)
  (or (eq (type-of track) 'partial)
      (find (state track) '(:embalmed :rotten))))

(defgeneric track-length (track)
  (:method (track)
    (declare (ignore track))
    "This functionality can not be applied to an object other than a
partial or a track.")
  (:documentation "Computes the length of a track or partial"))

(defmethod track-length ((track track))
  (length (peak-list track)))

(defmethod track-length ((track partial))
  (length (peak-list track)))

(defun nth-peak (n track &key (from-end nil))
  (if from-end
      (elt (peak-list track) n)
      (elt (peak-list track)
	   (- (track-length track) 1 n))))

(defun newest-peak (track)
  (first (peak-list track)))

(defun strip-peaks (track n 
		    &key (key #'identity)
		    (from-end nil))
  (if from-end
      (nreverse (map-into (make-sequence 'vector n) 
			  key 
			  (peak-list track)))
      (map-into (make-sequence 'vector n)
		key
		(reverse (peak-list track)))))

(defun ghost-peak-nb (track)
  (ghost-peaks track))

(defun validate-ghost-peaks (track)
  (setf (ghost-peaks track) 0))

(defun append-peak (peak track &key (ghost nil))
  (push peak (peak-list track))
  (if ghost
    (incf (ghost-peaks track))
    (setf (left-amp track) (/ (+ (* (left-amp track) 
				    (max 1 (1- (track-length track))))
				 (peak-amplitude peak 0))
			      (track-length track))
	  (right-amp track) (/ (+ (* (right-amp track) 
				     (max 1 (1- (track-length track))))
				  (peak-amplitude peak 0))
			       (track-length track))))
  (if (> (right-amp track) (left-amp track))
      (setf (main-channel track) 1)
      (setf (main-channel track) 0)))

(defun remove-last-peak (track)
  (pop (peak-list track)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    Synthesis
;;;



(defun synthesize-partial (p destination-sample-rate)
  (let ((hop-size (/ destination-sample-rate (sample-rate p))))
    (iterate (for phi in-vector 
		  (resample (map 'vector #'peak-phase (peak-list p))
			    1 hop-size
			    :centering-fun #'poly-centering))
	     (for a in-vector 
		  (resample (map 'vector #'peak-amplitude (peak-list p))
			    1 hop-size
			    :centering-fun #'poly-centering))
	     (collect (* a (cos phi)) result-type 'vector)))) 
	     
(defmethod play ((p partial))
  (play-samples (synthesize-partial p +default-sample-rate+)))

(defun mix-partial-into-array (p samples &key
			       (destination-sample-rate +default-sample-rate+))
  (let* ((hop-size (/ destination-sample-rate (sample-rate p)))
	 (new-size (ceiling (* hop-size (date-of-death p))))
	 (synth (synthesize-partial p destination-sample-rate)))
    (when (> new-size (length samples))
      (adjust-array samples new-size))
;    (format t "hop-size ~a~%" hop-size)
;    (format t "t length ~a~%" (track-length p))
;    (format t "db ~a~%" (* hop-size (date-of-birth p)))
;    (format t "dd ~a~%" (* hop-size (date-of-death p)))
;    (format t "new size ~a~%" new-size)
;    (format t "length samples ~a~%" (length samples))
;    (format t "length synth ~a~%" (length synth))
    (iterate (for s in-vector synth) 
	     (for i from (* hop-size (date-of-birth p)))
	     (incf (aref samples i) s)))
  samples)

(defun mix-partial-into-pcm (p pcm-instance)
  (with-slots ((destination-sample-rate sample-rate)
	       samples)
      pcm-instance
    (let* ((hop-size (/ destination-sample-rate (sample-rate p)))
	   (new-size (* hop-size (date-of-death p))))
      (when (> new-size (length samples))
	(adjust-array samples new-size))
      (iterate (for s in-vector (synthesize-partial p destination-sample-rate)) 
	       (for i from (* hop-size (date-of-birth p)))
	       (incf (aref samples i) s))))
  pcm-instance)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    Partial exportation
;;;

(defun save-one-partial (partial filename)
  (with-open-file (f (merge-pathnames filename)
		     :direction :output
		     :if-exists :supersede
		     :if-does-not-exist :create)
    (iterate (for peak in (peak-list partial))
	     (for date from (date-of-birth partial))
	     (format f "~A ~,,,,,,'eE ~,,,,,,'eE ~,,,,,,'eE ~,,,,,,'eE~%"
		     date
		     (peak-frequency peak)
		     (peak-amplitude peak)
		     (peak-phase peak)
		     (peak-position peak)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    Partial plotting, with cloud-grapher
;;;

(defmethod cloud-grapher:make-plottable ((thing partial))
  (list (strip-peaks thing (track-length thing)
	 :key #'peak-frequency)
   :beginning (date-of-birth thing)))

(defmethod cloud-grapher:make-plottable ((thing track))
  (list (strip-peaks thing (track-length thing)
	 :key #'peak-frequency)
   :beginning (date-of-birth thing)))