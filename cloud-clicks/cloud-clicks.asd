;;; -*- Mode: Lisp; Package: COMMON-LISP-USER -*-

;;;  (c) copyright 2007-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(use-package :asdf)

(defsystem :cloud-clicks
    :depends-on (:iterate :cloud-sound :cloud-sp :cloud-grapher)
    :components ((:file "package")
    		 (:file "noise-gate")
		 (:file "minmax")
		 (:file "spectrogram")
		 (:file "peak")
		 (:file "peak-sound")
		 (:file "peak-estimation")
		 (:file "partial")
		 (:file "sin-sound")
		 (:file "partial-tracking"))
    :serial t)
