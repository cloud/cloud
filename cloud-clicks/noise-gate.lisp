;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2005-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.clicks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Implements noise gates
;;;

(defun noise-gate (in-array threshold)
  (noise-gate! in-array 
	      (make-array (length in-array)
			  :element-type (array-element-type in-array))
	      threshold))

(defun n-noise-gate (in-array threshold)
  (noise-gate! in-array in-array threshold))

(defun noise-gate! (in-array out-array threshold)
  (let ((z (coerce 0 (array-element-type in-array))))
    (iterate (for i in-vector in-array)
	     (for j index-of-vector out-array)
	     (setf (aref out-array j) (if (> (abs i) threshold)
					  i
					  z))
	     (finally (return out-array)))))

(defun adaptive-noise-gate (in-array)
  (adaptive-noise-gate! in-array in-array))

(let ((array-length 0)
      (filter-len 0)
      (c 0.75)
      (filter nil))

  (defun adaptive-noise-gate! (in-array out-array)

      (when (/= (length in-array) array-length)
	(setf array-length (length in-array)
	      filter-len (/ array-length 64)
	      filter (hann filter-len)))

      
      (let* ((z (coerce 0 (array-element-type in-array)))
	     (e (/ (expt (mean (v-abs in-array)) (- 1 c)) 25))
	     (threshold (v-fun (lambda(x)
				 (* e (expt (abs x) c)))
			       (subseq (convolution (v-abs in-array)
						    filter)
				       (1- (/ filter-len 2))
				     (+ (length in-array)
					(1- (/ filter-len 2)))))))
	(iterate (for i in-vector in-array)
		 (for j index-of-vector out-array)
		 (setf (aref out-array j) (if (> (abs i) (aref threshold j))
					      i
					      z))
		 (finally (return out-array))))))

