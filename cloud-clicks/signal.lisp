;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2007-2009 by
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.clicks)

(defclass general-signal ()
  ())

(defgeneric value-at (s position &rest rest)
  (:documentation "Value of the signal s at a given position (in seconds)."))


;;; Continuous signal

(defclass continuous-signal (general-signal)
  ((math-function :initarg :math-function :accessor math-function))
  (:documentation "A signal defined by a mathematical function."))

(defmethod value-at ((s continuous-signal) position &rest rest)
  (funcall (math-function s) position))


(defclass finite-signal (general-signal)
  ())

(defclass infinite-signal (general-signal)
  ())



;;; Discrete signal

(defclass discrete-signal (general-signal)
  ((sample-rate :initarg :sample-rate :accessor sample-rate))
  (:documentation "An uniformely sampled signal."))

(defclass finite-discrete-signal (discrete-signal finite-signal)
  ((samples :initarg :samples :accessor samples)))

(define-condition out-of-bounds ()
  ()
  (:documentation "Condition when the requested value is out of bounds."))

(define-condition not-a-sample ()
  ()
  (:documentation "Condition when the requested value is not a sample."))



(defmethod value-at ((s finite-discrete-signal) position &rest rest)
  (declare (ignore rest))
  (with-slots (sample-rate samples) s
    (let ((sample-position (* position sample-rate)))
      (cond 
	((not (integerp sample-position))
	 (error (make-condition 'not-a-sample)))
	((or (minusp sample-position)
	     (> sample-position (1- (length samples))))
	 (error (make-condition 'out-of-bounds)))
	(t
	 (aref samples sample-position))))))


(defmethod (setf value-at) (val (s finite-discrete-signal) position)
  (with-slots (sample-rate samples) s
    (let ((sample-position (* position sample-rate)))
      (cond 
	((not (integerp sample-position))
	 (error (make-condition 'not-a-sample)))
	((or (minusp sample-position)
	     (> sample-position (1- (length samples))))
	 (error (make-condition 'out-of-bounds)))
	(t
	 (setf (aref (samples s) (* position (sample-rate s)))
	       val))))))





;;; Reconstructible and Predictable signals

;;; very stupid reconstruction and prediction function
(defun zero (&rest rest)
  (declare (ignore rest))
  0)
;;; that was it... ;)
      

(defclass reconstructible-signal (general-signal)
  ((reconstruction-function :initarg :reconstruction-function
			    :accessor reconstruction-function
			    :initform #'zero)))
					  

(defclass finite-discrete-reconstructible-signal
    (finite-discrete-signal reconstructible-signal)
  ())

(defmethod value-at ((s finite-discrete-reconstructible-signal) 
		     position &rest rest)
  (handler-case (call-next-method s position rest)
    (not-a-sample () (funcall (reconstruction-function s) s position))))


(defclass predictable-signal (general-signal)
  ((prediction-function :initarg :prediction-function
			:accessor prediction-function
			:initform #'zero)))

(defclass finite-discrete-predictable-signal 
    (finite-discrete-signal predictable-signal)
  ())

(defmethod value-at ((s finite-discrete-predictable-signal) position &rest rest)
  (handler-case (call-next-method s position)
    (out-of-bounds () (funcall (prediction-function s) s position))))

(defclass finite-discrete-predictable-reconstructible-signal
    (finite-discrete-signal predictable-signal reconstructible-signal)
  ())

(defmethod value-at ((s finite-discrete-predictable-reconstructible-signal) 
		     position &rest rest)
  (handler-case (call-next-method s position)
    (out-of-bounds () (funcall (prediction-function s) s position))
    (not-a-sample () (funcall (reconstruction-function s) s position))))


;;; More elaborate reconstruction functions

(defun closest (s x)
  (with-slots (sample-rate) s
    (value-at s (/ (round x (/ sample-rate)) sample-rate))))

(defun linear-interpolation (s x)
  (with-slots (sample-rate) s
    (*
     (/
      (-
       (value-at s (/ (ceiling (* sample-rate x)) sample-rate))
       (value-at s (/ (floor (* sample-rate x)) sample-rate)))
      (- 
       (/ (ceiling (* sample-rate x)) sample-rate)
       (/ (floor (* sample-rate x)) sample-rate)))
     x)))

(defun sinc (x)
  "The cardinal sine function."
  (if (zerop x)
      1
      (/ (sin (* pi x)) (* pi x))))


(defun hann-value (x)
  (if (<= 0.0d0 x 1.0d0)
      (* 0.5d0 (- 1d0 (cos (* 2d0 pi x))))
      0.0d0))

(defun window-value (x type)
  "Computes the value of a given window-type at position x."
  (case type
    ((:hann :hanning) (hann-value x))
    (:hamming (hamming-value x))
    (:blackman (blackman-value x))
    ((:barlett :triangular) (barlett-value x))
    ((:rectangular t) (rectangular-value x))))


(defun reconstructor-value (x wings &key (window :hann))
  (* (sinc x) 
     (window-value (/ (+ x wings) 
                      (* 2 wings))
                   window)))

(defun reconstruct (s x &key (wings 10) (window :hann))
  (with-slots (sample-rate) s
    (loop for i from (- wings) to (+ wings)
       with closest = (floor x (/ sample-rate))
       sum (* (value-at s (/ (+ closest i) sample-rate))
	      (reconstructor-value (- (* x sample-rate) 
				      (+ closest i)) 
				   wings 
				   :window window)))))


;;; More elaborate prediction functions

(defun constant-prediction (s x)
  (with-slots (sample-rate samples) s
    (cond 
      ((< x 0) (value-at s 0))
      ((>= x (/ (length samples) sample-rate)) 
       (value-at s (/ (1- (length samples)) sample-rate))))))

(define-condition not-enough-samples ()
  ()
  (:documentation "Condition when there aren't enough samples."))


(defun mirror-prediction (s x)
  (with-slots (sample-rate samples) s
    (cond
      ((< x 0) 
       (if (< (- x) (/ (length samples) sample-rate))
	   (- (* 2 (value-at s 0))
	      (value-at s (- x)))
	   (error (make-condition 'not-enough-samples))))
      ((>= x (/ (length samples) sample-rate))
       (if (<= (/ x 2) (/ (1- (length samples)) sample-rate))
	   (- (* 2 (value-at s (/ (1- (length samples)) sample-rate)))
	      (value-at s (- (* 2 (/ (1- (length samples)) sample-rate))
			     x)))
	   (error (make-condition 'not-enough-samples)))))))


;;; The cherry on top

(defun resample (s new-sample-rate)
  (let* ((length-in-secs (/ (length (samples s))
			    (sample-rate s)))
	 (new-s (make-instance (type-of s)
			       :sample-rate new-sample-rate
			       :samples (make-array (floor (* length-in-secs 
							      new-sample-rate))))))
    (loop for i from 0 below (floor (* length-in-secs
				       new-sample-rate))
	 do (setf (value-at new-s (/ i new-sample-rate))
		  (value-at s (/ i new-sample-rate)))
       finally (return new-s))))



;;; Tests

;; (setf *cosinus*
;;       (make-instance 'continuous-signal
;; 		     :math-function #'(lambda (x)
;; 					(cos x))))

;; (value-at *cosinus* pi)

;; (value-at *cosinus* (* 2/3 pi))

;; (setf *test-signal*
;;       (make-instance 'finite-discrete-predictable-reconstructible-signal
;;  		     :reconstruction-function #'reconstruct
;;  		     :prediction-function #'constant-prediction
;;  		     :sample-rate 44100
;;  		     :samples #(1 2 3 5)))

;; (value-at *test-signal* -1)

;; (samples (resample *test-signal* 44300))
