;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2008-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.clicks)

(defun spectrogram (s &key (frame-length 2048) (hopsize 512))
  (let ((spec (make-array (list (/ frame-length 2) (ceiling (/ (length s) hopsize)))))
	(pos 0)
	frame)
    (with-sliding-window (s frame :frame-length frame-length :hopsize hopsize)
      (iter (for i in-vector (v-fun #'to-decibels 
				    (v-fun #'(lambda (x) (+ x 0.00000001))
					   (v-abs (half (sfft (apply-window frame :hann)))))))
	    (for j from (1- (/ frame-length 2)) downto 0)
	    (setf (aref spec j pos) i))
      (incf pos))
    spec))

