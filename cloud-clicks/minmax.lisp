;;; -*- Mode: Lisp; Package: CLOUD.CLICKS -*-

;;;  (c) copyright 2005-2009
;;;           Martin Raspaud <martin.raspaud@gmail.com>

;;; The authors grant you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.

(in-package :cloud.clicks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Some functions for handling maxima and minima
;;;

(defun local-maxima-positions (in-array)
  "Finds the positions of the local maxima of an array."
  (let ((max-array (make-array 8 :fill-pointer 0)))
    (iterate (with could-be = nil)
	     (for i index-of-vector in-array)
	     (for elt in-vector in-array from 1) 
	     (for pelt previous elt initially (aref in-array 0))
	     (if (< pelt elt)
		 (setf could-be t)
		 (when could-be
		   (vector-push-extend i max-array)
		   (setf could-be nil)))
	     (finally (return max-array)))))

(defmacro with-local-maxima-positions (pos in-array &body body)
  "Uses the positions of the local maxima of an array."
  (with-gensyms (could-be celt pelt)
    `(iterate (with ,could-be = nil)
              (for ,pos index-of-vector ,in-array)
              (for ,celt in-vector ,in-array from 1) 
              (for ,pelt previous ,celt initially (aref ,in-array 0))
              (if (< ,pelt ,celt)
		  (setf ,could-be t)
		  (when ,could-be
		    (setf ,could-be nil)
		    ,@body)))))

(defmacro with-funned-local-maxima-positions (pos in-array fun &body body)
  "Uses the positions of the local maxima of an array which elements
   are processed through fun."
  (let ((could-be (gensym))
	(celt (gensym))
	(pelt (gensym)))
    `(iterate (with ,could-be = nil)
              (for ,pos index-of-vector ,in-array)
              (for ,celt in-vector ,in-array from 1) 
              (for ,pelt previous ,celt initially (funcall ,fun (aref ,in-array 0)))
              (if (< (funcall ,fun ,pelt) (funcall ,fun ,celt))
		  (setf ,could-be t)
		  (when ,could-be
		    (setf ,could-be nil)
		    ,@body)))))

(defmacro with-stereo-funned-local-maxima-positions (pos left-array right-array fun &body body)
  "Uses the positions of the local maxima of an array which elements
   are processed through fun."
  (let ((left-could-be (gensym))
	(right-could-be (gensym))
	(done (gensym))
	(left-celt (gensym))
	(right-celt (gensym))
	(left-pelt (gensym))
	(right-pelt (gensym)))
    `(iterate (with ,left-could-be = nil)
	      (with ,right-could-be = nil)
	      (with ,done = nil)
              (for ,pos index-of-vector ,left-array)
              (for ,left-celt in-vector ,left-array from 1) 
              (for ,right-celt in-vector ,right-array from 1) 
              (for ,left-pelt previous ,left-celt initially (funcall ,fun (aref ,left-array 0)))
	      (for ,right-pelt previous ,right-celt initially (funcall ,fun (aref ,right-array 0)))
              (if (< (funcall ,fun ,left-pelt) (funcall ,fun ,left-celt))
		  (setf ,left-could-be t)
		  (when ,left-could-be
		    (setf ,left-could-be nil)
		    (setf ,done t)
		    ,@body))
	      (if (< (funcall ,fun ,right-pelt) (funcall ,fun ,right-celt))
		  (setf ,right-could-be t)
		  (when (and ,right-could-be (not ,done))
		    (setf ,right-could-be nil)
		    ,@body))
	      (setf ,done nil))))