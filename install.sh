#!/bin/sh

root=$(pwd)
modules="cloud-bliss cloud-clicks cloud-grapher cloud-maths cloud-sp cloud-sound read-bytes soundfile"

for i in $modules
do
    ln -s $root/$i/$i.asd ~/.sbcl/systems/
done